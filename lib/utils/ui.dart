import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

Future<bool> toast(String msg) => Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 1,
      backgroundColor: ThemeColors.toast,
      textColor: ThemeColors.textGrey2,
      fontSize: 15.0,
    );

Widget header() => Container(
      height: 200,
      child: Stack(
        children: <Widget>[
          RotatedBox(
            quarterTurns: 2,
            child: WaveWidget(
              config: CustomConfig(
                gradients: [
                  [
                    ThemeColors.loginGradientStart,
                    ThemeColors.loginGradientEnd
                  ],
                  [
                    ThemeColors.loginGradientEnd,
                    ThemeColors.loginGradientStart
                  ],
                ],
                durations: [19440, 10800],
                heightPercentages: [0.20, 0.25],
                blur: MaskFilter.blur(BlurStyle.solid, 10),
                gradientBegin: Alignment.bottomLeft,
                gradientEnd: Alignment.topRight,
              ),
              waveAmplitude: 0,
              size: Size(
                double.infinity,
                double.infinity,
              ),
            ),
          ),
          Positioned(
            top: -105,
            left: -80,
            width: 211,
            height: 211,
            child: Container(
              decoration: BoxDecoration(
                  color: ThemeColors.whiteTransparent, shape: BoxShape.circle),
            ),
          ),
          Positioned(
            top: -18,
            right: -28,
            width: 93,
            height: 93,
            child: Container(
              decoration: BoxDecoration(
                  color: ThemeColors.whiteTransparent, shape: BoxShape.circle),
            ),
          ),
          Center(
            child: Container(
              margin: EdgeInsets.only(bottom: 40),
              child: Text("MarketLink",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white70,
                      fontWeight: FontWeight.w600,
                      fontSize: 40,
                      fontFamily: 'Rubik')),
            ),
          )
        ],
      ),
    );

Widget settingItem(
        {GestureTapCallback onTap, String title, bool border = true}) =>
    Material(
      color: Colors.white,
      child: InkWell(
          onTap: onTap,
          child: Container(
              margin: EdgeInsets.only(left: 18),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                color: ThemeColors.divider,
                width: border ? 1 : 0,
              ))),
              padding: EdgeInsets.only(right: 18, top: 15, bottom: 15),
              child: Row(children: <Widget>[
                Expanded(
                    flex: 8,
                    child: Row(children: <Widget>[
                      Container(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                            Text(
                              title,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontFamily: 'Rubik',
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w600,
                                  color: ThemeColors.textGrey3),
                            )
                          ]))
                    ])),
                Expanded(
                    flex: 2,
                    child: Align(
                        alignment: AlignmentDirectional.centerEnd,
                        child: Icon(Icons.arrow_forward_ios,
                            color: ThemeColors.textGrey2)))
              ]))),
    );

Widget divider([double _height = 6]) => SizedBox(height: _height);

Widget iconAttachment() => RotationTransition(
    turns: AlwaysStoppedAnimation(-30 / 360),
    child: Icon(
      Icons.attach_file,
      size: 18.0,
      color: ThemeColors.textGrey3,
    ));


Widget avatar({String url, double scale = 40, File image}) => ClipRRect(
  borderRadius: BorderRadius.circular(scale),
  child: (image == null) ? FadeInImage.assetNetwork(
      width: scale,
      height: scale,
      placeholder: 'assets/images/placeholder.png',
      image: url) :
  Image.file(image, width: scale, height: scale, fit: BoxFit.fitWidth,)
);

Widget members(List<String> members) {
  List<Widget> nodes = new List();

  double left = 0;

  for (var i = 0; i < 3; i++) {
    nodes.add(Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            border: Border.all(width: 2, color: Colors.white)
        ),
        margin: EdgeInsets.only(left: left),
        child: avatar(url: members[i], scale: 20)
    ));
    left+=10;
  }

  return Stack(
      children: nodes.reversed.toList()
  );
}