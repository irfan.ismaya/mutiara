import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

Future<bool> toast(String msg, Color) => Fluttertoast.showToast(
  msg: msg,
  toastLength: Toast.LENGTH_SHORT,
  gravity: ToastGravity.BOTTOM,
  timeInSecForIos: 2,
  backgroundColor: Color,
  textColor: ThemeColors.textGrey,
  fontSize: 15.0,
);

Widget header() => Container(
  height: 300,
  child: Stack(
    children: <Widget>[
      RotatedBox(
        quarterTurns: 2,
        child: WaveWidget(
          config: CustomConfig(
            gradients: [
              [
                ThemeColors.loginGradientStart,
                ThemeColors.loginGradientEnd
              ],
              [
                ThemeColors.loginGradientEnd,
                ThemeColors.loginGradientStart
              ],
            ],
            durations: [19440, 10800],
            heightPercentages: [0.20, 0.25],
            blur: MaskFilter.blur(BlurStyle.solid, 10),
            gradientBegin: Alignment.bottomLeft,
            gradientEnd: Alignment.topRight,
          ),
          waveAmplitude: 0,
          size: Size(
            double.infinity,
            double.infinity,
          ),
        ),
      ),
      Positioned(
        top: -105,
        left: -80,
        width: 211,
        height: 211,
        child: Container(
          decoration: BoxDecoration(
              color: ThemeColors.whiteTransparent, shape: BoxShape.circle),
        ),
      ),
      Positioned(
        top: 140,
        left: 50,
        width: 70,
        height: 70,
        child: Container(
          decoration: BoxDecoration(
              color: ThemeColors.whiteTransparent, shape: BoxShape.circle),
        ),
      ),
      Positioned(
        top: -18,
        right: -28,
        width: 93,
        height: 93,
        child: Container(
          decoration: BoxDecoration(
              color: ThemeColors.whiteTransparent, shape: BoxShape.circle),
        ),
      ),
      Positioned(
        top: 88,
        right: 50,
        width: 120,
        height: 120,
        child: Container(
          decoration: BoxDecoration(
              color: ThemeColors.whiteTransparent, shape: BoxShape.circle),
        ),
      ),
      Center(
        child: Container(
          margin: EdgeInsets.only(bottom: 80),
          child: Text("Mutiara",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white70,
                  fontWeight: FontWeight.w600,
                  fontSize: 40,
                  fontFamily: 'Rubik')),
        ),
      )
    ],
  ),
);