import 'package:flutter/material.dart';

Color strColor(String f) {
  switch(f) {
    case 'POLI GIGI':
      return Colors.blue;
    case 'POLI SPESIALIS KULIT & KELAMIN':
      return Colors.pinkAccent[100];
    case 'KHITAN':
      return Colors.lightGreen;
    case 'POLI KANDUNGAN (OBGYN)':
      return Colors.pinkAccent;
    case 'POLI ANAK':
      return Colors.orange;
    case 'POLI THT-KL':
      return Colors.deepPurpleAccent[100];
    case 'POLI PENYAKIT DALAM':
      return Colors.blue[900];
    default:
      return Colors.blue[100];
  }
}

String strIcon(String f) {
  switch(f) {
    case 'POLI GIGI':
      return "images/icon_poli/poli_gigi.png";
    case 'POLI SPESIALIS KULIT & KELAMIN':
      return "images/icon_poli/poli_kulit.png";
    case 'KHITAN':
      return "images/icon_poli/poli_khitanan.png";
    case 'POLI KANDUNGAN (OBGYN)':
      return "images/icon_poli/poli_kandungan.png";
    case 'POLI ANAK':
      return "images/icon_poli/poli_anak.png";
    case 'POLI THT-KL':
      return "images/icon_poli/poli_tht.png";
    default:
      return "images/icon_poli/poli_khitanan.png";
  }
}

Color strColorStatus(int f) {
  switch(f) {
    case -1:
      return Colors.red;
    case 1:
      return Colors.orange;
    case 2:
      return Colors.blue;
    case 3:
      return Colors.green;
    default:
      return Colors.blueGrey;
  }
}

String strPoliName(String poli_id) {
  switch(poli_id) {
    case '1':
      return "POLI GIGI";
    case '19':
      return "POLI SPESIALIS KULIT & KELAMIN";
    case '18':
      return "POLI KHITAN";
    case '13':
      return "POLI KANDUNGAN (OBGYN)";
    case '3':
      return "POLI ANAK";
    case '15':
      return "POLI THT-KL";
    default:
      return "Unknown";
  }
}

AssetImage strBannerPoli(int f) {
  switch(f) {
    case 3:
      return AssetImage('images/banner/banner_anak.png');
    case 1:
      return AssetImage('images/banner/banner_gigi.png');
    case 13:
      return AssetImage('images/banner/banner_kandungan.png');
    case 17:
      return AssetImage('images/banner/banner_penyakit_dalam.png');
    case 15:
      return AssetImage('images/banner/banner_tht.png');
    default:
      return AssetImage('images/banner/image_not_found.png');
  }
}
