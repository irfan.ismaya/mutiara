import 'package:meta/meta.dart';
import 'package:mvvm_mutiara/interfaces/interface_api.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/api_request.dart';

class DoctorViewModel extends Model {
  Future<ResponseListDoctor> _listdoctor;
  Future<ResponseListProfesi> _listprofesi;
  Future<ResponseScheduleDoctor> _scheduledoctor;

  Future<ResponseListDoctor> get listdoctor => _listdoctor;
  Future<ResponseListProfesi> get listprofesi => _listprofesi;
  Future<ResponseScheduleDoctor> get scheduledoctor => _scheduledoctor;

  set listdoctor(Future<ResponseListDoctor> value) {
    _listdoctor = value;
    notifyListeners();
  }

  set listprofesi(Future<ResponseListProfesi> value) {
    _listprofesi = value;
    notifyListeners();
  }

  set scheduledoctor(Future<ResponseScheduleDoctor> value) {
    _scheduledoctor = value;
    notifyListeners();
  }

  final InterfaceApi api;
  DoctorViewModel({@required this.api});

  Future<bool> listDoctor(int offset, String token) async {
    listdoctor = api?.listDoctor(offset, token);
    return listdoctor != null;
  }

  Future<bool> listProfesi(String token) async {
    listprofesi = api?.listProfesi(token);
    return listprofesi != null;
  }

  Future<bool> scheduleDoctor(int dokter_id, String token) async {
    scheduledoctor = api?.scheduleDoctor(dokter_id, token);
    return scheduledoctor != null;
  }
}