import 'package:meta/meta.dart';
import 'package:mvvm_mutiara/interfaces/interface_api.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/api_request.dart';

class FamilyViewModel extends Model {
  Future<ResponseFamily> _family;
  Future<ResponseMessage> _addfamily;
  Future<ResponseMessage> _updatefamily;
  Future<ResponseDetailFamily> _detailfamily;
  Future<ResponseMessage> _deletefamily;

  Future<ResponseFamily> get family => _family;
  Future<ResponseMessage> get addfamily => _addfamily;
  Future<ResponseMessage> get updatefamily => _updatefamily;
  Future<ResponseDetailFamily> get detailfamily => _detailfamily;
  Future<ResponseMessage> get deletefamily => _deletefamily;

  set family(Future<ResponseFamily> value) {
    _family = value;
    notifyListeners();
  }

  set addfamily(Future<ResponseMessage> value) {
    _addfamily = value;
    notifyListeners();
  }

  set updatefamily(Future<ResponseMessage> value) {
    _updatefamily = value;
    notifyListeners();
  }

  set detailfamily(Future<ResponseDetailFamily> value) {
    _detailfamily = value;
    notifyListeners();
  }

  set deletefamily(Future<ResponseMessage> value) {
    _deletefamily = value;
    notifyListeners();
  }

  final InterfaceApi api;
  FamilyViewModel({@required this.api});

  Future<bool> getFamily(String token) async {
    family = api?.getFamily(token);
    return family != null;
  }

  Future<bool> addFamily(AddFamilyRequest data, String token) async {
    addfamily = api?.addFamily(data, token);
    return addfamily != null;
  }

  Future<bool> updateFamily(UpdateFamilyRequest data, String token) async {
    updatefamily = api?.updateFamily(data, token);
    return updatefamily != null;
  }

  Future<bool> detailFamily(String kode_keluarga, String token) async {
    detailfamily = api?.detailFamily(kode_keluarga, token);
    return detailfamily != null;
  }

  Future<bool> deleteFamily(String kode_keluarga, String token) async {
    deletefamily = api?.deleteFamily(kode_keluarga, token);
    return deletefamily != null;
  }
}