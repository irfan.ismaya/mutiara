import 'package:meta/meta.dart';
import 'package:mvvm_mutiara/interfaces/interface_api.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/models/response_search_doctor.dart';

class PolyClinicViewModel extends Model {
  Future<ResponseListPoliklinik> _polyclinic;
  Future<ResponseQueue> _getqueue;
  Future<ResponsePolySchedule> _listpolyschedule;
  Future<ResponseBooking> _bookingpoly;
  Future<ResponseListConfirm> _bookingconfirmation;
  Future<ResponseMessage> _bookingconfirmationcome;
  Future<ResponseSearchDoctor> _listsearchdoctor;
  Future<ResponseConfirmQueue> _getqueueconfirm;

  Future<ResponseListPoliklinik> get polyclinic => _polyclinic;
  Future<ResponseQueue> get getqueue => _getqueue;
  Future<ResponsePolySchedule> get listpolyschedule => _listpolyschedule;
  Future<ResponseBooking> get bookingpoly => _bookingpoly;
  Future<ResponseListConfirm> get bookingconfirmation => _bookingconfirmation;
  Future<ResponseMessage> get bookingconfirmationcome => _bookingconfirmationcome;
  Future<ResponseSearchDoctor> get listsearchdoctor => _listsearchdoctor;
  Future<ResponseConfirmQueue> get getqueueconfirm => _getqueueconfirm;

  set polyclinic(Future<ResponseListPoliklinik> value) {
    _polyclinic = value;
    notifyListeners();
  }

  set getqueue(Future<ResponseQueue> value) {
    _getqueue = value;
    notifyListeners();
  }

  set listpolyschedule(Future<ResponsePolySchedule> value) {
    _listpolyschedule = value;
    notifyListeners();
  }

  set bookingpoly(Future<ResponseBooking> value) {
    _bookingpoly = value;
    notifyListeners();
  }

  set bookingconfirmation(Future<ResponseListConfirm> value) {
    _bookingconfirmation = value;
    notifyListeners();
  }

  set bookingconfirmationcome(Future<ResponseMessage> value) {
    _bookingconfirmationcome = value;
    notifyListeners();
  }

  set listsearchdoctor(Future<ResponseSearchDoctor> value) {
    _listsearchdoctor = value;
    notifyListeners();
  }

  set getqueueconfirm(Future<ResponseConfirmQueue> value) {
    _getqueueconfirm = value;
    notifyListeners();
  }

  final InterfaceApi api;
  PolyClinicViewModel({@required this.api});

  Future<bool> listPolyClinic(String token) async {
    polyclinic = api?.listPolyClinic(token);
    return polyclinic != null;
  }

  Future<bool> getQueue(String token) async {
    getqueue = api?.getQueue(token);
    return getqueue != null;
  }

  Future<bool> listPolySchedule(PolyClinicSheduleRequest data, String token) async {
    listpolyschedule = api?.listPolySchedule(data, token);
    return listpolyschedule != null;
  }

  Future<bool> bookingPoly(BookingRequest data, String token) async {
    bookingpoly = api?.bookingPoly(data, token);
    return bookingpoly != null;
  }

  Future<bool> bookingConfirmation(String token) async {
    bookingconfirmation = api?.bookingConfirmation(token);
    return bookingconfirmation != null;
  }

  Future<bool> bookingConfirmationCome(BookingConfirmationComeRequest data, String token) async {
    bookingconfirmationcome = api?.bookingConfirmationCome(data, token);
    return bookingconfirmationcome != null;
  }

  Future<bool> searchListDoctor(String token, int offset) async {
    listsearchdoctor = api?.listSearchDoctor(token, offset);
    return listsearchdoctor != null;
  }

  Future<bool> getQueueConfirm(String token, int kodebooking) async {
    getqueueconfirm = api?.getQueueConfirm(token, kodebooking);
    return getqueueconfirm != null;
  }
}