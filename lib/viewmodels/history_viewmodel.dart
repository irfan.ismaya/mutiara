import 'package:meta/meta.dart';
import 'package:mvvm_mutiara/interfaces/interface_api.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/api_request.dart';

class HistoryViewModel extends Model {
  Future<ResponseHistory> _history;

  Future<ResponseHistory> get history => _history;

  set history(Future<ResponseHistory> value) {
    _history = value;
    notifyListeners();
  }

  final InterfaceApi api;
  HistoryViewModel({@required this.api});

  Future<bool> setHistory(String token) async {
    history = api?.getHistory(token);
    return history != null;
  }
}