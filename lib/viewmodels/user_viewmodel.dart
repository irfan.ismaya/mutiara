import 'package:meta/meta.dart';
import 'package:mvvm_mutiara/interfaces/interface_api.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/api_request.dart';

class UserViewModel extends Model {
  Future<ResponseLogin>   _login;
  Future<ResponseGetProfile>   _getprofile;
  Future<ResponseMessage> _adduser;
  Future<ResponseMessage> _updateuser;
  Future<ResponseMessage> _logout;
  Future<ResponseMessage> _changepassword;
  Future<ResponseMessage> _forgotpassword;
  Future<ResponseMessage> _getotp;
  Future<ResponseDashboard> _getdashboard;
  Future<ResponseMessage> _checkotp;

  Future<ResponseLogin>   get login => _login;
  Future<ResponseGetProfile>  get getprofile => _getprofile;
  Future<ResponseMessage> get adduser => _adduser;
  Future<ResponseMessage> get updateuser => _updateuser;
  Future<ResponseMessage> get logout => _logout;
  Future<ResponseMessage> get changepassword => _changepassword;
  Future<ResponseMessage> get forgotpassword => _forgotpassword;
  Future<ResponseMessage> get getotp => _getotp;
  Future<ResponseDashboard> get getdashboard => _getdashboard;
  Future<ResponseMessage> get checkotp => _checkotp;

  set login(Future<ResponseLogin> value) {
    _login = value;
    notifyListeners();
  }

  set getprofile(Future<ResponseGetProfile> value) {
    _getprofile = value;
    notifyListeners();
  }

  set adduser(Future<ResponseMessage> value) {
    _adduser = value;
    notifyListeners();
  }

  set updateuser(Future<ResponseMessage> value) {
    _updateuser = value;
    notifyListeners();
  }

  set logout(Future<ResponseMessage> value) {
    _logout = value;
    notifyListeners();
  }

  set changepassword(Future<ResponseMessage> value) {
    _changepassword = value;
    notifyListeners();
  }

  set forgotpassword(Future<ResponseMessage> value) {
    _forgotpassword = value;
    notifyListeners();
  }

  set getotp(Future<ResponseMessage> value) {
    _getotp = value;
    notifyListeners();
  }

  set getdashboard(Future<ResponseDashboard> value) {
    _getdashboard = value;
    notifyListeners();
  }

  set checkotp(Future<ResponseMessage> value) {
    _checkotp = value;
    notifyListeners();
  }

  final InterfaceApi api;
  UserViewModel({@required this.api});

  Future<bool> doLogin(LoginRequest data) async {
    login = api?.doLogin(data);
    return login != null;
  }

  Future<bool> getProfile(String token) async {
    getprofile = api?.getProfile(token);
    return getprofile != null;
  }

  Future<bool> addUser(CreateUserRequest data) async {
    adduser = api?.addUser(data);
    return adduser != null;
  }

  Future<bool> updateUser(UpdateUserRequest data, String token) async {
    updateuser = api?.updateUser(data, token);
    return updateuser != null;
  }

  Future<bool> doLogout(String token) async {
    logout = api?.doLogout(token);
    return logout != null;
  }

  Future<bool> changePassword(ChangePasswordRequest data, String token) async {
    changepassword = api?.changePassword(data, token);
    return changepassword != null;
  }

  Future<bool> forgotPassword(String telephone) async {
    forgotpassword = api?.forgotPassword(telephone);
    return forgotpassword != null;
  }

  Future<bool> getOtp(String telephone) async {
    getotp = api?.getOtp(telephone);
    return getotp != null;
  }

  Future<bool> getDashboard(String token) async {
    getdashboard = api?.getDashboard(token);
    return getdashboard != null;
  }

  Future<bool> checkOtp(CheckOtpRequest data) async {
    checkotp = api?.checkOtp(data);
    return checkotp != null;
  }

}