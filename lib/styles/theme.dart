import 'dart:ui';

import 'package:flutter/material.dart';

class ThemeColors {

  const ThemeColors();

  static const Color primaryGradientColor = const Color(0xffd70044);

  static const Color loginGradientStart = const Color(0xFF3867D5);
  static const Color loginGradientEnd = const Color(0xFF81C7F5);

  static const Color primary          = const Color(0xFF5F87E7);
  static const Color secondary        = const Color(0xFF554E8F);
  static const Color background       = const Color(0xFFF9FCFF);

  static const Color textWhite        = const Color(0xFFF1F1F1);
  static const Color textGrey         = const Color(0xFFF1F1F1);
  static const Color textGrey2        = const Color(0xFFC6C6C8);
  static const Color textGrey3        = const Color(0xFF898989);

  static const Color divider          = const Color(0xFFEDEDED);

  static const Color statePlanned     = const Color(0xFFBEBEBE);
  static const Color stateOnGoing     = const Color(0xFF5F87E7);
  static const Color statePending     = const Color(0xFFFFB100);
  static const Color stateCanceled    = const Color(0xFFD10263);
  static const Color stateDone        = const Color(0xFF2AB116);
  static const Color stateApproval    = const Color(0xFF2AB116);

  static const Color shadowPlanned    = const Color(0x45C4C4C4);
  static const Color shadowOnGoing    = const Color(0x45023AD1);
  static const Color shadowPending    = const Color(0x26BAAE6E);
  static const Color shadowCanceled   = const Color(0x45D10202);
  static const Color shadowDone       = const Color(0x451ED102);
  static const Color shadowApproval   = const Color(0x452AB116);

  static const Color tabActive        = const Color(0xFF5F87E7);
  static const Color tabInactive      = const Color(0xFF554E8F);

  static const Color sectionTitle     = const Color(0xFF8B87B3);
  static const Color sectionSubTitle  = const Color(0xFFBEBEBE);

  static const Color whiteTransparent = const Color(0x1BFFFFFF);

  static const Color toast            = const Color(0xFF606060);

  static const Color shadow           = const Color(0x0D888888);
  static const Color shadow2           = const Color(0x2A888888);
  static const Color textField        = const Color(0xFF82A0B7);

  static const Color buttonGradientStart  = const Color(0xFF7EB6FF);
  static const Color buttonGradientEnd    = const Color(0xFF5F87E7);
  static const Color buttonShadow         = const Color(0xFF6894EE);

  static const Color fabGradientStart  = const Color(0xFFF857C3);
  static const Color fabGradientEnd    = const Color(0xFFE0139C);
  static const Color fabShadow         = const Color(0x78F456C3);

  static const Color redGradientStart  = const Color(0xFFE0139C);
  static const Color redGradientEnd    = const Color(0xFFD10263);
  static const Color redGradientShadow = const Color(0x45D10202);

  static const Color yellowGradientStart  = const Color(0xFFFFB100);
  static const Color yellowGradientEnd    = const Color(0xFFD89F20);
  static const Color yellowGradientShadow = const Color(0x26BAAE6E);

  static const primaryGradient = const LinearGradient(
    colors: const [Colors.blue, Colors.blue],
    stops: const [0.0, 1.0],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );

  static const buttonGradient = const LinearGradient(
    colors: const [Colors.blue, Colors.blue],
    stops: const [0.0, 1.0],
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
  );

  static const fabGradient = const LinearGradient(
    colors: const [fabGradientStart, fabGradientEnd],
    stops: const [0.0, 1.0],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );

  static const redGradient = const LinearGradient(
    colors: const [redGradientStart, redGradientEnd],
    stops: const [0.0, 1.0],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );

  static const yellowGradient = const LinearGradient(
    colors: const [yellowGradientStart, yellowGradientEnd],
    stops: const [0.0, 1.0],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );
}