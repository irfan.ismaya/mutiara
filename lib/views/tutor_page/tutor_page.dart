import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';

class TutorPage extends StatefulWidget {
  @override
  _TutorPageState createState() => _TutorPageState();
}

class _TutorPageState extends State<TutorPage> {
  final pages = [
    PageViewModel(
      pageColor: Color.fromRGBO(252, 252, 252, 1.0),
      bubbleBackgroundColor: Colors.blue,
      title: Container(),
      body: Column(
        children: <Widget>[
          Text(''),
          Text(
            '',
            style: TextStyle(
                color: Colors.black54,
                fontSize: 16.0
            ),
          ),
        ],
      ),
      mainImage: Image.asset(
        'images/tutor/tutor_one.png',
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.black),
    ),
    PageViewModel(
      pageColor: Color.fromRGBO(252, 252, 252, 1.0),
      iconColor: null,
      bubbleBackgroundColor: Colors.blue,
      title: Container(),
      body: Column(
        children: <Widget>[
          Text(''),
          Text('',
            style: TextStyle(
                color: Colors.black54,
                fontSize: 16.0
            ),
          ),
        ],
      ),
      mainImage: Image.asset(
        'images/tutor/tutor_two.png',
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.black),
    ),
    PageViewModel(
      pageColor: Color.fromRGBO(252, 252, 252, 1.0),
      iconColor: null,
      bubbleBackgroundColor: Colors.blue,
      title: Container(),
      body: Column(
        children: <Widget>[
          Text(''),
          Text('',
            style: TextStyle(
                color: Colors.black54,
                fontSize: 16.0
            ),
          ),
        ],
      ),
      mainImage: Image.asset(
        'images/tutor/tutor_three.png',
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.black),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: "Cara Penggunaan Aplikasi",
          isBack: true,
        ),
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              IntroViewsFlutter(
                pages,
                onTapDoneButton: (){
                  Navigator.pop(context);
                },
                showSkipButton: false,
                doneText: Text("Selesai",),
                pageButtonsColor: Colors.blue,
                pageButtonTextStyles: new TextStyle(
                  fontSize: 16.0,
                  fontFamily: "Regular",
                ),
              ),
            ],
          ),
        )
    );
  }
}
