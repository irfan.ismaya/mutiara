import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/viewmodels/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';

final UserViewModel viewModel = UserViewModel(api: ServiceApi());

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool checkValue = false;
  bool _isLoading = false;
  bool _obscureTextPass = true;
  bool _obscureTextConfirmPass = true;

  String _name;
  String _telephone;
  String _email;
  String _password;
  String _confirmpassword;
  String _address;

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _telephoneFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirmpasswordFocus = FocusNode();
  final FocusNode _addressFocus = FocusNode();

  ResponseMessage data;
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _telephoneController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _confirmpassController = new TextEditingController();
  TextEditingController _addressController = new TextEditingController();

  String _validatorTelephone(value) {
    String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_register.validation.field_phone_empty');
    }else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_register.validation.field_phone_regex');
    }
    return null;
  }

  String _validatorPass(value) {
    String patttern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_register.validation.field_pass_empty');
    }else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_register.validation.field_pass_regex');
    }
    return null;
  }

  String _validatorName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_register.validation.field_name_empty');
    } else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_register.validation.field_name_regex');
    }
    return null;
  }

  String _validatorEmail(String value) {
    String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_register.validation.field_email_empty');
    } else if(!regExp.hasMatch(value)){
      return AppLocalizations.of(context).tr('page_register.validation.field_email_regex');
    }else {
      return null;
    }
  }

  String _validatorAddress(value) {
    if (value.isEmpty) {
      return AppLocalizations.of(context).tr('page_register.validation.field_address_empty');
    }
    if (value.length < 10) {
      return AppLocalizations.of(context).tr('page_register.validation.field_address_regex');
    }
    return null;
  }

  String _validatorPassword(value){
    return _validatorPass(value);
  }

  void _togglePass() {
    setState(() {
      _obscureTextPass = !_obscureTextPass;
    });
  }

  void _toggleConfirmPass() {
    setState(() {
      _obscureTextConfirmPass = !_obscureTextConfirmPass;
    });
  }

  Future<String> _doRegister() async{
    setState(() {
      _isLoading = true;
    });

    try{

      CreateUserRequest adduser = CreateUserRequest(
          telephone: _telephone,
          name: _name,
          alamat: _address,
          email: _email,
          password: _password,
          password_confirmation: _confirmpassword);

      bool req = await viewModel.addUser(adduser);
      data = await viewModel.adduser;

      if(req){
        if(data.code == 200 || data.code == 201){
          HelpersView.toast(data.message, ThemeColors.stateApproval);
        }else if(data.code == 400 || data.code == 422){
          HelpersView.toast(data.message, Colors.orange);
        }
      }else{
        HelpersView.toast(data.message, ThemeColors.statePending);
      }
    }catch(ex){
      print(ex);
      HelpersView.toast(data.message, ThemeColors.statePending);
    }finally{
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarSmall(
        context: context,
        title: AppLocalizations.of(context).tr('page_register.title'),
        isBack: true,
      ),
      backgroundColor: ThemeColors.background,
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 25),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Visibility(
                visible: _isLoading,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: CircularProgressIndicator(strokeWidth: 1,),
                ),
              ),
              Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: ThemeColors.shadow,
                                blurRadius: 6,
                                spreadRadius: 4)
                          ]),
                      child: TextFormField(
                        enabled: !_isLoading,
                        validator: _validatorName,
                        onSaved: (value) => _name = value,
                        focusNode: _nameFocus,
                        controller: _nameController,
                        keyboardType: TextInputType.text,
                        style: TextStyle(color: ThemeColors.textField),
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.person),
                            hintText: AppLocalizations.of(context).tr('page_register.form.hint_name'),
                            hintStyle:
                            TextStyle(color: ThemeColors.textField),
                            filled: true,
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius:
                              BorderRadius.all(Radius.circular(5.0)),
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 16.0)),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: ThemeColors.shadow,
                                blurRadius: 6,
                                spreadRadius: 4)
                          ]),
                      child: TextFormField(
                        enabled: !_isLoading,
                        validator: _validatorTelephone,
                        onSaved: (value) => _telephone = value,
                        focusNode: _telephoneFocus,
                        controller: _telephoneController,
                        keyboardType: TextInputType.phone,
                        style: TextStyle(color: ThemeColors.textField),
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.phone_iphone),
                            hintText: AppLocalizations.of(context).tr('page_register.form.hint_phone'),
                            hintStyle:
                            TextStyle(color: ThemeColors.textField),
                            filled: true,
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius:
                              BorderRadius.all(Radius.circular(5.0)),
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 16.0)),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: ThemeColors.shadow,
                                blurRadius: 6,
                                spreadRadius: 4)
                          ]),
                      child: TextFormField(
                        enabled: !_isLoading,
                        validator: _validatorEmail,
                        onSaved: (value) => _email = value,
                        focusNode: _emailFocus,
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(color: ThemeColors.textField),
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.email),
                            hintText: AppLocalizations.of(context).tr('page_register.form.hint_email'),
                            hintStyle:
                            TextStyle(color: ThemeColors.textField),
                            filled: true,
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius:
                              BorderRadius.all(Radius.circular(5.0)),
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 16.0)),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: ThemeColors.shadow,
                                blurRadius: 6,
                                spreadRadius: 4)
                          ]),
                      child: TextFormField(
                        enabled: !_isLoading,
                        validator: _validatorPassword,
                        onSaved: (value) => _password = value,
                        focusNode: _passwordFocus,
                        controller: _passwordController,
                        obscureText: _obscureTextPass,
                        style: TextStyle(
                            color: ThemeColors.textField,
                            fontFamily: 'RobotoMono'),
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.lock),
                            suffixIcon: GestureDetector(
                              child: _obscureTextPass
                                  ? const Icon(Icons.visibility_off)
                                  : const Icon(Icons.visibility),
                              onTap: _togglePass,
                            ),
                            hintText: AppLocalizations.of(context).tr('page_register.form.hint_pass'),
                            hintStyle: TextStyle(
                                color: ThemeColors.textField,
                                fontFamily: 'RobotoMono'),
                            filled: true,
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius:
                              BorderRadius.all(Radius.circular(5.0)),
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 16.0)),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: ThemeColors.shadow,
                                blurRadius: 6,
                                spreadRadius: 4)
                          ]),
                      child: TextFormField(
                        enabled: !_isLoading,
                        validator: _validatorPassword,
                        onSaved: (value) => _confirmpassword = value,
                        focusNode: _confirmpasswordFocus,
                        controller: _confirmpassController,
                        obscureText: _obscureTextConfirmPass,
                        style: TextStyle(
                            color: ThemeColors.textField,
                            fontFamily: 'RobotoMono'),
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.lock),
                            suffixIcon: GestureDetector(
                              child: _obscureTextConfirmPass
                                  ? const Icon(Icons.visibility_off)
                                  : const Icon(Icons.visibility),
                              onTap: _toggleConfirmPass,
                            ),
                            hintText: AppLocalizations.of(context).tr('page_register.form.hint_confirmpass'),
                            hintStyle: TextStyle(
                                color: ThemeColors.textField,
                                fontFamily: 'RobotoMono'),
                            filled: true,
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius:
                              BorderRadius.all(Radius.circular(5.0)),
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 16.0)),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: ThemeColors.shadow,
                                blurRadius: 6,
                                spreadRadius: 4)
                          ]),
                      child: TextFormField(
                        enabled: !_isLoading,
                        validator: _validatorAddress,
                        onSaved: (value) => _address = value,
                        focusNode: _addressFocus,
                        controller: _addressController,
                        keyboardType: TextInputType.multiline,
                        maxLines: 3,
                        style: TextStyle(
                            color: ThemeColors.textField,
                            fontFamily: 'RobotoMono'),
                            decoration: InputDecoration(
                            hintText: AppLocalizations.of(context).tr('page_register.form.hint_address'),
                            hintStyle: TextStyle(
                                color: ThemeColors.textField,
                                fontFamily: 'RobotoMono'),
                      filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius:
                      BorderRadius.all(Radius.circular(5.0)),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 16.0)),
                      ),
                    ),
                    Button(
                      onTap: () {
                        if (!_isLoading) {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            _doRegister();
                          } else {
                            setState(() => _autoValidate = true);
                          }
                        }
                      },text: AppLocalizations.of(context).tr('page_register.form.register'),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 40, right: 40),
                        child: Text(AppLocalizations.of(context).tr('page_register.form.info'),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 15
                          ),
                        ),
                    ),
                    SizedBox(height: 5,),
                    FlatButton(
                      onPressed: (){
                        Navigator.pushNamed(context, '/pincode');
                      },
                      child: Text(AppLocalizations.of(context).tr('page_register.form.otp'),
                        style: TextStyle(
                            fontSize: 18,
                            color: ThemeColors.textGrey3
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ]
        ),
      )
    );
  }
}
