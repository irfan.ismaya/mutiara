import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/viewmodels/history_viewmodel.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/utils/helpers.dart' as helpers;
import 'package:mvvm_mutiara/views/commons/row_item_history.dart';

final HistoryViewModel viewModelHistory = HistoryViewModel(api: ServiceApi());
final ServiceApi _serviceApi = ServiceApi();

class HistoryPage extends StatefulWidget{
  final HistoryViewModel viewModel;
  HistoryPage({Key key, @required this.viewModel}) : super(key: key);

  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  String api_token = "";
  bool initData = false;
  SharedPreferences sharedPreferences;

  Future<Null> _onRefresh() async {
    await new Future.delayed(new Duration(seconds: 3));
    setState(() {
      initData = true;
    });
    initData ? _serviceApi.getHistory(api_token):_serviceApi.getHistory(api_token);

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: 'Rekaman',
          isBack: false,
        ),
        backgroundColor: ThemeColors.background,
        body: Container(
          child: FutureBuilder(
            future: initData ? _serviceApi.getHistory(api_token):_serviceApi.getHistory(api_token),
              // ignore: missing_return
            builder: (context, snapshot){
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.active:
                case ConnectionState.waiting:
                  return Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 1,
                      )
                  );
                  break;
                case ConnectionState.done:
                  if (snapshot.hasData) {
                    return RefreshIndicator(
                      child: ListView.builder(
                          itemCount: snapshot.data.data == null ? 0 : snapshot.data.data.length,
                          padding: EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
                          primary: false,
                          itemBuilder: (BuildContext context, int i) =>
                              RowItemHistory(
                                name: snapshot.data.data[i].dokterName,
                                poliname: snapshot.data.data[i].poliName,
                                date: snapshot.data.data[i].bookingDate,
                                status: snapshot.data.data[i].bookingStatusName,
                                icon: helpers.strIcon(snapshot.data.data[i].poliName),
                                image: helpers.strIcon(snapshot.data.data[i].poliName),
                                color: helpers.strColor(snapshot.data.data[i].poliName),
                                colorstatus: helpers.strColorStatus(snapshot.data.data[i].bookingStatus),
                              )
                      ),
                      onRefresh: _onRefresh,
                    );
                  }else if(snapshot.data == null) {
                    return Center(
                        child: Text("No Data")
                    );
                  }else if(snapshot.hasError) {
                    return Center(
                        child: Text("No Internet Connection")
                    );
                  }
                break;
              }
            }
          ),
        ),
    );
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      api_token = sharedPreferences.getString("api_token");
    });
  }

  @override
  void initState() {
    super.initState();
    getCredential();
  }
}
