import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/viewmodels/user_viewmodel.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/utils/ui.dart' as ui;
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;

final UserViewModel viewModelUser = UserViewModel(api: ServiceApi());
enum ConfirmAction { CANCEL, ACCEPT }

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool _isLoading = false;
  ResponseGetProfile responseGetProfile;
  SharedPreferences sharedPreferences;

  String name;
  String address;
  String telephone;
  String email;
  String image_fullpath;
  int role_id;
  String role_name;

  Future<String> _getProfile() async{
    setState(() {
      _isLoading = true;
    });

    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("api_token");
    bool req = await viewModelUser.getProfile(token);
    responseGetProfile = await viewModelUser.getprofile;
    if(req){
      if(responseGetProfile.code == 200){
        setState(() {
          _isLoading = false;
          name = responseGetProfile.data.name;
          address = responseGetProfile.data.address;
          telephone = responseGetProfile.data.telephone;
          email = responseGetProfile.data.email;
          image_fullpath = responseGetProfile.data.imageFullpath;
          role_id = responseGetProfile.data.roleId;
          role_name = responseGetProfile.data.roleName;
        });
      }else{
        print("MESSAGE");
        HelpersView.toast(responseGetProfile.message, Colors.orange);
        setState(() {
          _isLoading = false;
        });
      }
    }else{
      print("MESSAGE");
      print(responseGetProfile.message);
      HelpersView.toast("TESR", ThemeColors.statePending);
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  Future<ConfirmAction> _logoutConfirmDialog(BuildContext context) async {
    sharedPreferences = await SharedPreferences.getInstance();
    return showDialog<ConfirmAction>(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Keluar'),
          content: Text(
              'Apakah anda ingin keluar dari aplikasi?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Tidak'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.CANCEL);
              },
            ),
            FlatButton(
              child: Text('Ya'),
              onPressed: () {
//                Navigator.pushReplacementNamed(context, '/login');
                Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
                sharedPreferences.remove("api_token");
                sharedPreferences.remove("telephone");
                sharedPreferences.remove("password");
              },
            )
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _getProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: 'Profil',
          isBack: false,
        ),
        backgroundColor: ThemeColors.background,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                height: 200,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        stops: [0.5, 0.9],
                        colors: [
                          ThemeColors.background,
                          ThemeColors.background
                        ]
                    )
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                            Container(
                              width: 100,
                              height: 100,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: NetworkImage(image_fullpath != null ? image_fullpath : "https://flutter.io/images/catalog-widget-placeholder.png"),
                                      fit: BoxFit.cover
                                  )
                            )
                          ),
                        ],
                      ),
                      SizedBox(height: 10,),
                      Text(name != null ? name : "-", style: TextStyle(fontSize: 18.0, color: Colors.grey),),
                      SizedBox(height: 5,),
                      Text(role_name != null ? role_name : "-", style: TextStyle(fontSize: 14.0, color: Colors.grey),)
                    ],
                  )
                ),
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text("Nama"),
                  subtitle: Text(name != null ? name : "-"),
                ),
                ListTile(
                  leading: Icon(Icons.email),
                  title: Text("Surat Elektronik"),
                  subtitle: Text(email != null ? email : "-"),
                ),
                ListTile(
                  leading: Icon(Icons.phone),
                  title: Text("Telepon"),
                  subtitle: Text(telephone != null ? telephone : "-"),
                ),
                ListTile(
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                  leading: Icon(Icons.my_location),
                  title: Text("Alamat"),
                  subtitle: Text(address != null ? address : "-"),
                ),
                ListTile(
                  leading: Icon(Icons.group),
                  title: Text("Keluarga"),
                  subtitle: Text("Anggota Keluarga"),
                  onTap: () {
                    Navigator.pushNamed(context, '/listfamily');
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_album),
                  title: Text("Tutor"),
                  subtitle: Text("Cara Penggunaan Aplikasi"),
                  onTap: () {
                    Navigator.pushNamed(context, '/tutor');
                  },
                ),
                ListTile(
                  leading: Icon(Icons.lock),
                  title: Text("Perbaharui Kata Sandi"),
                  subtitle: Text("Edit Kata Sandi"),
                  onTap: () {
                    Navigator.pushNamed(context, '/changepassword');
                  },
                ),
                ListTile(
                  leading: Icon(Icons.exit_to_app),
                  title: Text("Keluar"),
                  subtitle: Text("Keluar Dari Aplikasi"),
                  onTap: () {
                    _logoutConfirmDialog(context);
                  },
                ),
            ],
          )
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            Navigator.pushNamed(context, '/editprofile');
          },
          label: Text('Ubah Profil'),
          icon: Icon(Icons.edit),
          backgroundColor: Colors.pink,
        )
    );
  }
}
