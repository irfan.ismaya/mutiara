import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/viewmodels/polyclinic_viewmodel.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/views/detail_confirm_page/detail_confirm_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/utils/helpers.dart' as helpers;
import 'package:mvvm_mutiara/views/commons/row_item_confirm.dart';

final PolyClinicViewModel viewModelPoli = PolyClinicViewModel(api: ServiceApi());
final ServiceApi _serviceApi = ServiceApi();

class MyOrderPage extends StatefulWidget {
  final MyOrderPage viewModelPolyClinic;
  MyOrderPage({Key key, @required this.viewModelPolyClinic}) : super(key: key);

  @override
  _MyOrderPageState createState() => _MyOrderPageState();
}

class _MyOrderPageState extends State<MyOrderPage> {
  String api_token = "";
  bool initData = false;
  SharedPreferences sharedPreferences;

  Future<Null> _onRefresh() async {
    await new Future.delayed(new Duration(seconds: 3));
    setState(() {
      initData = true;
    });
    initData ? _serviceApi.bookingConfirmation(api_token):_serviceApi.bookingConfirmation(api_token);

    return null;
  }

  void goToDetailConfirm(DataConfirm data){
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetailConfirmPage(
              bookingId: data.bookingId,
              kodebooking: data.kodebooking,
              hari: data.hari,
              jampraktekAwal : data.jampraktekAwal,
              jampraktekAkhir : data.jampraktekAkhir,
              poliklinikName: data.poliklinikName,
              bookingDate: data.bookingDate,
              dokterName: data.dokterName,
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarSmall(
        context: context,
        title: 'Pesanan',
        isBack: false,
      ),
      backgroundColor: ThemeColors.background,
      body: Container(
        child: FutureBuilder(
            future: initData ? _serviceApi.bookingConfirmation(api_token):_serviceApi.bookingConfirmation(api_token),
            // ignore: missing_return
            builder: (context, snapshot){
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  break;
                case ConnectionState.active:
                  break;
                case ConnectionState.waiting:
                  return Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 1,
                      )
                  );
                  break;
                case ConnectionState.done:
                  if (snapshot.hasData) {
                    return RefreshIndicator(
                      child: ListView.builder(
                          itemCount: snapshot.data.data == null ? 0 : snapshot.data.data.length,
                          padding: EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
                          primary: false,
                          itemBuilder: (BuildContext context, int i) =>
                              RowItemConfirm(
                                name: snapshot.data.data[i].dokterName,
                                poliname: snapshot.data.data[i].poliklinikName,
                                date: snapshot.data.data[i].bookingDate,
                                time: snapshot.data.data[i].hari+", "+snapshot.data.data[i].jampraktekAwal+"-"+snapshot.data.data[i].jampraktekAkhir,
                                icon: helpers.strIcon(snapshot.data.data[i].poliklinikName),
                                image: helpers.strIcon(snapshot.data.data[i].poliklinikName),
                                color: helpers.strColor(snapshot.data.data[i].poliklinikName),
                                onTap: (){
                                  DataConfirm data = DataConfirm(
                                    bookingId: snapshot.data.data[i].bookingId,
                                    kodebooking: snapshot.data.data[i].kodebooking,
                                    hari: snapshot.data.data[i].hari,
                                    jampraktekAwal : snapshot.data.data[i].jampraktekAwal,
                                    jampraktekAkhir : snapshot.data.data[i].jampraktekAkhir,
                                    poliklinikName: snapshot.data.data[i].poliklinikName,
                                    bookingDate: snapshot.data.data[i].bookingDate,
                                    dokterName: snapshot.data.data[i].dokterName,
                                  );
                                  goToDetailConfirm(data);
                                },
                              )
                      ),
                      onRefresh: _onRefresh,
                    );
                  }else if(snapshot.data == null) {
                    return Center(
                        child: Text("No Data")
                    );
                  }else if(snapshot.hasError) {
                    return Center(
                        child: Text("No Internet Connection")
                    );
                  }
                  break;
              }
            }
        ),
      ),
    );
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      api_token = sharedPreferences.getString("api_token");
    });
  }

  @override
  void initState() {
    super.initState();
    getCredential();
  }
}
