import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/views/commons/row_item_menu.dart';
import 'package:mvvm_mutiara/views/commons/line_separator_dotted.dart';
import 'package:mvvm_mutiara/views/commons/line_separator_dotted_ver.dart';
import 'package:mvvm_mutiara/views/list_doctorpoli_page/list_doctorpoli_page.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:url_launcher/url_launcher.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/viewmodels/user_viewmodel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/utils/ui.dart' as ui;
import 'package:intl/intl.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

final UserViewModel viewModelUser = UserViewModel(api: ServiceApi());
final ServiceApi _serviceApi = ServiceApi();
final List<String> imgList = [];
List dataImage = List();

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String api_token = "";
  ResponseDashboard responseDashboard = ResponseDashboard();
  SharedPreferences sharedPreferences;

  Future<String> _getDashboard() async{
    sharedPreferences = await SharedPreferences.getInstance();
    try{
      String token = sharedPreferences.getString("api_token");
      bool req = await viewModelUser.getDashboard(token);
      responseDashboard = await viewModelUser.getdashboard;
      if(req){
        if(responseDashboard.code == 200 || responseDashboard.code == 201){
          imgList.clear();
          for(int i = 0; i < responseDashboard.data.howtopicture.length; i++){
            print(responseDashboard.data.berita[i].gambar);
            imgList.add(responseDashboard.data.howtopicture[i].gambar);
          }
          setState(() {
            dataImage = responseDashboard.data.howtopicture;
          });

          print("LENGTH");
          print(imgList.length);
        }else{
          print("MESSAGE");
          HelpersView.toast(responseDashboard.message, Colors.orange);
        }
      }else{
        print("MESSAGE");
        print(responseDashboard.message);
        HelpersView.toast("TESR", ThemeColors.statePending);
      }
    }catch(ex){
      HelpersView.toast("wrong", ThemeColors.statePending);
    }finally{
    }
    return 'Success';
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      api_token = sharedPreferences.getString("api_token");
    });
  }

  @override
  void initState() {
    super.initState();
    getCredential();
  }

  void goToListDoctor(int poli_id, String title_poli){
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ListDocterPoliPage(poli_id: poli_id, date_time: new DateFormat("yyyy-MM-dd").format(DateTime.now()).toString(), title_poli: title_poli,),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarSmall(
        context: context,
        title: 'Beranda',
        isBack: false,
      ),
      backgroundColor: ThemeColors.background,
      body: ListView(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.symmetric(vertical: 15.0),
              child: Column(
                  children: [
                    Container(
                      height: 220,
                      child: FutureBuilder(
                          future: _serviceApi.getDashboard(api_token),
                          // ignore: missing_return
                          builder: (context, snapshot){
                            switch (snapshot.connectionState) {
                              case ConnectionState.none:
                              case ConnectionState.active:
                              case ConnectionState.waiting:
                                return Center(
                                    child: CircularProgressIndicator(
                                      strokeWidth: 1,
                                    )
                                );
                                break;
                              case ConnectionState.done:
                                if (snapshot.hasData) {
                                  return Swiper(
                                      itemBuilder: (BuildContext context, int i) {
                                        return Image.network(
                                          snapshot.data.data.berita[i].gambar,
                                          fit: BoxFit.fill,
                                          height: 220,
                                          width: double.infinity,
                                          alignment: Alignment.center,
                                        );
                                      },
                                      itemCount: snapshot.data.data.berita == null ? 0 : snapshot.data.data.berita.length,
                                      viewportFraction: 0.8,
                                      scale: 0.9,
                                      pagination: SwiperPagination(
                                        margin: EdgeInsets.only(right: 25.0,),
                                        builder: DotSwiperPaginationBuilder(
                                            color: Colors.grey
                                        ),
                                      )
                                  );
                                }else if(snapshot.data == null) {
                                  return Center(
                                      child: Text("No Data")
                                  );
                                }
                                break;
                            }
                          }
                      ),
                    )
              ]
            )
          ),
          Visibility(
            visible: false,
            child: Container(
              padding: EdgeInsets.only(left: 15, bottom: 20, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("Cari dokter yang tersedia hari ini?",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 15
                    ),
                  ),
                  SizedBox(
                      height: 45,
                      child: FlatButton.icon(
                        color: Colors.blue,
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                        icon: Icon(Icons.add_location, color: Colors.white,),
                        label: Text('Cari Dokter', style: TextStyle(
                            color: Colors.white,
                            fontSize: 13
                        )),
                        onPressed: () {
                          Navigator.pushNamed(context, '/searchdoctor');
                        },
                      )
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 20),
            child: LineSeparatorDotted(color: Colors.grey),
          ),
          Padding(
            padding: EdgeInsets.only(left: 15, bottom: 10),
            child: Text("Daftar Poli",
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 18,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          Container(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: RowItemMenu(
                        title: "Anak",
                        icon: "images/icon_poli/poli_anak.png",
                        colorBox: Colors.orange,
                        colorIcon: Colors.white,
                        onTap: () {
                          goToListDoctor(3, "Daftar Dokter Poli Anak");
                        }
                      ),
                    ),
                    Expanded(
                      child: RowItemMenu(
                        title: "Gigi",
                        icon: "images/icon_poli/poli_gigi.png",
                        colorBox: Colors.blue,
                        colorIcon: Colors.white,
                        onTap: () {
                          goToListDoctor(1, "Daftar Dokter Poli Gigi");
                        }
                      ),
                    ),
                    Expanded(
                      child: RowItemMenu(
                        title: "Kandungan",
                        icon: "images/icon_poli/poli_kandungan.png",
                        colorBox: Colors.pinkAccent,
                        colorIcon: Colors.white,
                        onTap: () {
                          goToListDoctor(13, "Daftar Dokter Poli Kandungan");
                        }
                      ),
                    ),
                    Expanded(
                      child: RowItemMenu(
                        title: "Umum",
                        icon: "images/icon_poli/poli_umum.png",
                        colorBox: Colors.green,
                        colorIcon: Colors.white,
                        onTap: () {
                          goToListDoctor(2, "Daftar Dokter Poli Umum");
                        }
                      ),
                    ),
                  ],
                ),
                ui.divider(15),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: RowItemMenu(
                        title: "THT",
                        icon: "images/icon_poli/poli_tht.png",
                        colorBox: Colors.deepPurpleAccent[100],
                        colorIcon: Colors.white,
                        onTap: () {
                          goToListDoctor(15, "Daftar Dokter Poli THT");
                        }
                      ),
                    ),
                    Expanded(
                      child: RowItemMenu(
                        title: "Penyakit Dalam",
                        icon: "images/icon_poli/poli_penyakit_dalam.png",
                        colorBox: Colors.blue[900],
                        colorIcon: Colors.white,
                        onTap: () {
                          goToListDoctor(17, "Daftar Dokter Poli Penyakit Dalam");
                        }
                      ),
                    ),
                    Expanded(
                      child: RowItemMenu(
                        title: "Kulit",
                        icon: "images/icon_poli/poli_kulit.png",
                        colorBox: Colors.pinkAccent[100],
                        colorIcon: Colors.white,
                        onTap: () {
                          goToListDoctor(19, "Daftar Dokter Poli Kulit");
                        }
                      ),
                    ),
                    Expanded(
                      child: RowItemMenu(
                        title: "Khitanan",
                        icon: "images/icon_poli/poli_khitanan.png",
                        colorBox: Colors.lightGreen,
                        colorIcon: Colors.white,
                        onTap: () {
                          goToListDoctor(18, "Daftar Dokter Poli Khitanan");
                        }
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          ui.divider(15),
          Padding(
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 20, top: 5),
            child: LineSeparatorDotted(color: Colors.grey),
          ),
          Padding(
            padding: EdgeInsets.only(left: 15, bottom: 15),
            child: Text("Bantuan Pelanggan",
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 18,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 20),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              child: Container(
                height: 160,
                color: Colors.blue[100].withOpacity(0.7),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      height: 105,
                      padding: EdgeInsets.all(10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Image.asset("images/icon_poli/poli_admin.png",
                            height: 80,
                            width: 80
                          ),
                          Expanded(
                              child:Container(
                                  height: 105,
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text("Ada yang bisa dibantu?",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color: Colors.grey[600],
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold
                                          ),
                                        ),
                                        Text("Kami siap membantu Anda 24 jam",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color: Colors.grey[600],
                                              fontSize: 16,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                              )
                          )
                        ],
                      ),
                    ),
                    LineSeparatorDotted(color: Colors.grey),
                    Container(
                      margin: EdgeInsets.only(top: 4),
                      height: 45,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          FlatButton.icon(
                            icon: Icon(Icons.phone, color: Colors.grey[600],),
                            label: Text('Phone', style: TextStyle(
                                color: Colors.grey[600],
                                fontSize: 14
                            )),
                            onPressed: () => launch("tel:0227216058"),
                          ),
                          RotatedBox(
                            quarterTurns: 1,
                            child: LineSeparatorDottedVer(color: Colors.grey),
                          ),
                          FlatButton.icon(
                            icon: Icon(Icons.email, color: Colors.grey[600],),
                            label: Text('Email', style: TextStyle(
                                color: Colors.grey[600],
                                fontSize: 14
                            )),
                            onPressed: () => launch("mailto:info@mutiaracikutra.com")
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
