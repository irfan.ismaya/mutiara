import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/main_menu_page/home_page/home_page.dart';
import 'package:mvvm_mutiara/views/main_menu_page/myorder_page/myorder_page.dart';
import 'package:mvvm_mutiara/views/main_menu_page/history_page/history_page.dart';
import 'package:mvvm_mutiara/views/main_menu_page/profile_page/profile_page.dart';

class MainMenuPage extends StatefulWidget {
  @override
  _MainMenuPageState createState() => _MainMenuPageState();
}

class _MainMenuPageState extends State<MainMenuPage> {
  int _currentIndex = 0;
  List<Widget> _children = [];

  @override
  void initState() {
    _children.add(HomePage());
    _children.add(MyOrderPage());
    _children.add(HistoryPage());
    _children.add(ProfilePage());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  BottomNavigationBar _buildBottomNavigationBar() {
    return BottomNavigationBar(
      backgroundColor: Colors.white,
      onTap: _onTabTapped,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Beranda")),
        BottomNavigationBarItem(
            icon: Icon(Icons.rate_review),
            title: Text("Pesanan")),
        BottomNavigationBarItem(
            icon: Icon(Icons.history),
            title: Text("Rekaman")),
        BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            title: Text("Profil")),
      ],
      currentIndex: _currentIndex,
      type: BottomNavigationBarType.fixed,
      unselectedLabelStyle: TextStyle(fontSize: 10, fontFamily: 'Rubik'),
      selectedLabelStyle: TextStyle(fontSize: 10, fontFamily: 'Rubik'),
    );
  }

  _onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

}

