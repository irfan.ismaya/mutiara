import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/views/commons/row_item_poli.dart';

List<RowItemPoli> menuUtamaItem = [
  RowItemPoli(
    title: "Umum",
    icon: "images/icon_poli/poli_umum.png",
    status: "Tersedia",
    colorone: Colors.white10,
    colortwo: Colors.white,
    iconcolor: Colors.green,
  ),
  RowItemPoli(
    title: "Anak",
    icon: "images/icon_poli/poli_anak.png",
    status: "Tersedia",
    colorone: Colors.white10,
    colortwo: Colors.white,
    iconcolor: Colors.orange,
  ),
  RowItemPoli(
    title: "Kandungan",
    icon: "images/icon_poli/poli_kandungan.png",
    status: "Tersedia",
    colorone: Colors.white10,
    colortwo: Colors.white,
    iconcolor: Colors.pinkAccent,
  ),
  RowItemPoli(
    title: "Gigi",
    icon: "images/icon_poli/poli_gigi.png",
    status: "Tersedia",
    colorone: Colors.white10,
    colortwo: Colors.white,
    iconcolor: Colors.blue,
  ),
  RowItemPoli(
    title: "THT",
    icon: "images/icon_poli/poli_tht.png",
    status: "Tersedia",
    colorone: Colors.white10,
    colortwo: Colors.white,
    iconcolor: Colors.deepPurpleAccent,
  ),
  RowItemPoli(
    title: "Penyakit Dalam",
    icon: "images/icon_poli/poli_penyakit_dalam.png",
    status: "Tersedia",
    colorone: Colors.white10,
    colortwo: Colors.white,
    iconcolor: Colors.lightBlue[800],
  ),
  RowItemPoli(
    title: "Kulit",
    icon: "images/icon_poli/poli_kulit.png",
    status: "Tersedia",
    colorone: Colors.white10,
    colortwo: Colors.white,
    iconcolor: Colors.pink[200],
  ),
  RowItemPoli(
    title: "Khitanan",
    icon: "images/icon_poli/poli_khitanan.png",
    status: "Tersedia",
    colorone: Colors.white10,
    colortwo: Colors.white,
    iconcolor: Colors.green[300],
  )

];
class ChoosePoliPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarSmall(
        context: context,
        title: 'Pilih Poli',
        isBack: true,
      ),
      backgroundColor: ThemeColors.background,
      body: SingleChildScrollView(
          child:  Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  color: Colors.blue,
                  height: 200,
                  width: double.infinity,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        height: 100,
                        width: double.infinity,
                        margin: EdgeInsets.fromLTRB(15.0, 150.0, 15.0, 25.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)
                        ),
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Lorem ipsum dolor sit amet", style: Theme.of(context).textTheme.title,),
                            SizedBox(height: 10.0),
                            Text("Oct 21, 2017 By DLohani"),
                          ],
                        )
                    )
                  ]
                )
              ]
            ),
            Container(
                padding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 25.0),
                child: GridView.count(
                  shrinkWrap: true,
                  crossAxisCount: 3,
                  children: menuUtamaItem,
                )
            )
          ]
        ),
      ),
    );
  }
}
