import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/message.dart';
import 'package:mvvm_mutiara/viewmodels/user_viewmodel.dart';
import 'package:mvvm_mutiara/viewmodels/history_viewmodel.dart';
import 'package:mvvm_mutiara/viewmodels/family_viewmodel.dart';
import 'package:mvvm_mutiara/viewmodels/doctor_viewmodel.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:easy_localization/easy_localization.dart';

final UserViewModel viewModelUser = UserViewModel(api: ServiceApi());
final HistoryViewModel viewModelHistory = HistoryViewModel(api: ServiceApi());
final FamilyViewModel viewModelFamily = FamilyViewModel(api: ServiceApi());
final DoctorViewModel viewModelDoctor = DoctorViewModel(api: ServiceApi());

class LoginPage extends StatefulWidget {
  LoginRequest loginRequest;
  LoginPage({this.loginRequest});

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with SingleTickerProviderStateMixin {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final List<Message> messages = [];

  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool checkValue = false;
  String _telephone;
  String _device_token;
  String _password;
  bool _isLoading = false;

  SharedPreferences sharedPreferences;
  ResponseLogin login;
  ResponseHistory history;
  ResponseFamily family;
  ResponseListDoctor listdoctor;

  final FocusNode _telephoneFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  TextEditingController _telephoneController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  bool _obscureText = true;

  String _validatorTelephone(value) {
    String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_login.validation.field_phone_empty');
    }else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_login.validation.field_phone_regex');
    }
    return null;
  }

  String _validatorPass(value) {
    if (value.isEmpty) {
      return AppLocalizations.of(context).tr('page_login.validation.field_pass_empty');
    }
    if (value.length < 5) {
      return AppLocalizations.of(context).tr('page_login.validation.field_pass_regex');
    }
    return null;
  }

  String _validatorPassword(value){
    return _validatorPass(value);
  }

  Future<String> _doLogin() async{
    setState(() {
      _isLoading = true;
    });

    try{
      LoginRequest profile = LoginRequest(telephone: _telephone, password: _password, versi:"1.6", device_token: _device_token);
      bool req = await viewModelUser.doLogin(profile);
      login = await viewModelUser.login;

      if(req){
        if(login.code == 200){
          Navigator.pushReplacementNamed(context, '/mainmenu');
          sharedPreferences = await SharedPreferences.getInstance();
          setState(() {
            sharedPreferences.setString("user_id", login.data.userId.toString());
            sharedPreferences.setString("role_id", login.data.roleId.toString());
            sharedPreferences.setString("name", login.data.name);
            sharedPreferences.setString("telephone", login.data.telephone);
            sharedPreferences.setString("password", _passwordController.text);
            sharedPreferences.setString("api_token", login.data.apiToken);
            sharedPreferences.commit();
          });
        }else{
          HelpersView.toast(login.message, Colors.orange);
        }
      }else{
        print(login.message);
        HelpersView.toast("TESR", ThemeColors.statePending);
      }
    }catch(ex){
      HelpersView.toast("wrong", ThemeColors.statePending);
    }finally{
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ThemeColors.background,
        body: SingleChildScrollView(
          child: Column(
              children: <Widget>[
                HelpersView.header(),
                Visibility(
                  visible: _isLoading,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: CircularProgressIndicator(strokeWidth: 1,),
                  ),
                ),
                Form(
                  key: _formKey,
                  autovalidate: _autoValidate,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ThemeColors.shadow,
                                  blurRadius: 6,
                                  spreadRadius: 4)
                            ]),
                        child: TextFormField(
                          enabled: !_isLoading,
                          keyboardType: TextInputType.phone,
                          validator: _validatorTelephone,
                          onSaved: (value) => _telephone = value,
                          focusNode: _telephoneFocus,
                          controller: _telephoneController,
                          style: TextStyle(color: ThemeColors.textField),
                          decoration: InputDecoration(
                              hintText: AppLocalizations.of(context).tr('page_login.form.hint_phone'),
                              prefixIcon: Icon(Icons.phone_iphone),
                              hintStyle:
                              TextStyle(color: ThemeColors.textField),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 16.0)),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 30, right: 30, top: 20),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ThemeColors.shadow,
                                  blurRadius: 6,
                                  spreadRadius: 4)
                            ]),
                        child: TextFormField(
                          enabled: !_isLoading,
                          validator: _validatorPassword,
                          onSaved: (value) => _password = value,
                          focusNode: _passwordFocus,
                          controller: _passwordController,
                          obscureText: _obscureText,
                          style: TextStyle(
                              color: ThemeColors.textField,
                              fontFamily: 'RobotoMono'),
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.lock),
                              suffixIcon: GestureDetector(
                                child: _obscureText
                                    ? const Icon(Icons.visibility_off)
                                    : const Icon(Icons.visibility),
                                onTap: _toggle,
                              ),
                              hintText: AppLocalizations.of(context).tr('page_login.form.hint_password'),
                              hintStyle: TextStyle(
                                  color: ThemeColors.textField,
                                  fontFamily: 'RobotoMono'),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 16.0)),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left:10, top: 20, right: 10),
                        child: CheckboxListTile(
                          value: checkValue,
                          onChanged: _onChanged,
                          title: Text(AppLocalizations.of(context).tr('page_login.form.save_account'),
                            style: TextStyle(
                              fontSize: 16,
                              color: ThemeColors.textGrey3
                            ),
                          ),
                          controlAffinity: ListTileControlAffinity.leading,
                        ),
                      ),
                      Button(
                        onTap: () {
                          if (!_isLoading) {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              _doLogin();
                            } else {
                              setState(() => _autoValidate = true);
                            }
                          }
                        },text: AppLocalizations.of(context).tr('page_login.form.login'),
                      ),
                      FlatButton(
                        onPressed: (){
                          Navigator.pushNamed(context, '/forgotpassword');
                        },
                        child: Text(
                          AppLocalizations.of(context).tr('page_login.form.forgotpass'),
                          style: TextStyle(
                            fontSize: 16,
                            color: ThemeColors.textGrey3
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: FlatButton(
                          onPressed: (){
                            Navigator.pushNamed(context, '/register');
                          },
                          child: Text(
                            AppLocalizations.of(context).tr('page_login.form.register'),
                            style: TextStyle(
                                fontSize: 16,
                                color: ThemeColors.textGrey3
                            ),
                          ),
                        )
                      ),
                    ],
                  ),
                )
            ]
          ),
        )
    );
  }

  _onChanged(bool value) async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = value;
      sharedPreferences.setBool("check", checkValue);
      sharedPreferences.commit();
      getCredential();
    });
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      if (checkValue != null) {
        if (checkValue) {
          _telephoneController.text = sharedPreferences.getString("telephone");
          _passwordController.text = sharedPreferences.getString("password");
        } else {
          _telephoneController.clear();
          _passwordController.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        final notification = message['notification'];
        setState(() {
          messages.add(Message(
              title: notification['title'], body: notification['body']));
        });
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");

        final notification = message['data'];
        setState(() {
          messages.add(Message(
            title: '${notification['title']}',
            body: '${notification['body']}',
          ));
        });
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));

    _firebaseMessaging.getToken().then((token) {
      _device_token = token;
      print("FCM");
      print(token);
    });
    _telephoneController.text = '085221511541';
    _passwordController.text = 'Sw@media1';
  }

}