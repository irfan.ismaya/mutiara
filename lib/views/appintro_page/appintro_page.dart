import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:mvvm_mutiara/views/login_page/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppIntroPage extends StatefulWidget {
  @override
  _AppIntroPageState createState() => new _AppIntroPageState();
}

class _AppIntroPageState extends State<AppIntroPage>{

  List<Slide> slides = new List();
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "Pesan",
        styleTitle:
        TextStyle(color: Color(0xffD02090), fontSize: 30.0, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),
        description: "Dengan teknologi internet dan sistem IT yang terinteragrasi terbaik, anda dapat melakukan booking antrian tanpa harus datang ke lokasi",
        styleDescription:
        TextStyle(color: Color(0xffD02090), fontSize: 20.0, fontStyle: FontStyle.italic, fontFamily: 'Raleway'),
        pathImage: "images/onboard/onboard_one.png",
        widthImage: 300,
        heightImage: 300,
        colorBegin: Color(0xffFFDAB9),
        colorEnd: Color(0xff40E0D0),
        directionColorBegin: Alignment.topLeft,
        directionColorEnd: Alignment.bottomRight,
      ),
    );
    slides.add(
      new Slide(
        title: "Konfirmasi",
        styleTitle:
        TextStyle(color: Color(0xffD02090), fontSize: 30.0, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),
        description: "Setelah kamu booking secara online, kamu tinggal datang untuk melakukan konfirmasi data-data yang sudah dimasukan.",
        styleDescription:
        TextStyle(color: Color(0xffD02090), fontSize: 20.0, fontStyle: FontStyle.italic, fontFamily: 'Raleway'),
        pathImage: "images/onboard/onboard_two.png",
        widthImage: 300,
        heightImage: 300,
        colorBegin: Color(0xffFFFACD),
        colorEnd: Color(0xffFF6347),
        directionColorBegin: Alignment.topRight,
        directionColorEnd: Alignment.bottomLeft,
      ),
    );
    slides.add(
      new Slide(
        title: "Dapat Nomor Antrian",
        styleTitle:
        TextStyle(color: Color(0xffD02090), fontSize: 30.0, fontWeight: FontWeight.bold, fontFamily: 'RobotoMono'),
        description:
        "Selamat anda sudah mendapatkan nomor andtrian tanpa harus menunggu lama",
        styleDescription:
        TextStyle(color: Color(0xffD02090), fontSize: 20.0, fontStyle: FontStyle.italic, fontFamily: 'Raleway'),
        pathImage: "images/onboard/onboard_three.png",
        widthImage: 300,
        heightImage: 300,
        colorBegin: Color(0xffFFA500),
        colorEnd: Color(0xff7FFFD4),
        directionColorBegin: Alignment.topCenter,
        directionColorEnd: Alignment.bottomCenter,
        maxLineTextDescription: 3,
      ),
    );
  }

  void onDonePress() async{
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      sharedPreferences.setBool("check", true);
    });
    Navigator.pushNamed(context, '/login');
  }

  void onSkipPress() async{
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      sharedPreferences.setBool("check", true);
    });
    Navigator.pushNamed(context, '/login');
  }

  Widget renderNextBtn() {
    return Icon(
      Icons.navigate_next,
      color: Color(0xffD02090),
      size: 35.0,
    );
  }

  Widget renderDoneBtn() {
    return Icon(
      Icons.done,
      color: Color(0xffD02090),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      // List slides
      slides: this.slides,

      // Next, Done button
      onDonePress: this.onDonePress,
      renderNextBtn: this.renderNextBtn(),
      renderDoneBtn: this.renderDoneBtn(),
      colorDoneBtn: Color(0x33000000),
      highlightColorDoneBtn: Color(0xff000000),

      // Dot indicator
      colorDot: Color(0x33D02090),
      colorActiveDot: Color(0xffD02090),
      sizeDot: 13.0,
    );
  }
}