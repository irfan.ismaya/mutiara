import 'package:flutter/material.dart';

class RowItemMenu extends StatelessWidget {
  RowItemMenu({this.title, this.icon, this.colorBox, this.colorIcon, this.onTap});
  final String title;
  final String icon;
  final GestureTapCallback onTap;
  final Color colorBox, colorIcon;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Material(
          child: InkWell(
            customBorder: CircleBorder(),
            onTap: this.onTap,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(25.0)),
              child: Stack(
                children: <Widget>[
                  Container(
                      width: 60.0,
                      height: 60.0,
                      decoration: BoxDecoration(
                        color: colorBox,
                      ),
                      child: Image.asset(icon, width: 30,height: 30,)
                  ),
                  Positioned(
                    top: -10,
                    left: -10,
                    width: 50,
                    height: 50,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color(0x1BFFFFFF),
                          shape: BoxShape.circle
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5.0),
          child: Text(
            title,
            style: TextStyle(
              fontSize: 13.0,
              color: Colors.grey[600]
            ),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }
}