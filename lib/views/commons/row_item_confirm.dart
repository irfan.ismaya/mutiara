import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/commons/circle_image_indicator.dart';

class RowItemConfirm extends StatelessWidget {
  final String name;
  final String poliname;
  final String status;
  final String date;
  final String time;
  final String image;
  final String icon;
  final Color color;
  final GestureTapCallback onTap;

  RowItemConfirm({this.name, this.poliname, this.date, this.time, this.status, this.image, this.icon, this.color, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.grey[50].withOpacity(0.9),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 90,
                  child: Center(
                      child: CircleImageIndicator(
                        personImagePath: 'images/icon_poli/poli_admin.png',
                        color: color,
                      )
                  ),
                ),
                Expanded(
                    child:Container(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(3),
                                child: Text(name,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Colors.grey[600],
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(3),
                                child: Text(poliname,
                                  textAlign: TextAlign.left,
                                  maxLines: 2,
                                  style: TextStyle(
                                    decoration: TextDecoration.none,
                                    color: color,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(3),
                                child: Text(date,
                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  style: TextStyle(
                                    decoration: TextDecoration.none,
                                    color: Colors.grey[600],
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(3),
                                child: Text(time,
                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  style: TextStyle(
                                    decoration: TextDecoration.none,
                                    color: Colors.grey[600],
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                    )
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 5),
                      height: 80,
                      width: 70,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(topRight: Radius.circular(10), bottomRight: Radius.circular(10)),
                          gradient:LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: [color, color]
                          )
                      ),
                      child: Center(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Align(
                                alignment: Alignment.center,
                                child: Image.asset(icon, width: 50,height: 50),
                              ),
                            ]
                        ),
                      ),
                    ),
                    Positioned(
                      top: -10,
                      left: -10,
                      width: 50,
                      height: 50,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color(0x1BFFFFFF),
                            shape: BoxShape.circle
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: -10,
                      right: -10,
                      width: 50,
                      height: 50,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color(0x1BFFFFFF),
                            shape: BoxShape.circle
                        ),
                      ),
                    ),
                  ],
                )
              ],
            )
        ),
      ),
      onTap: onTap,
    );
  }
}
