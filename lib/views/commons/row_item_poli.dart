import 'package:flutter/material.dart';

class RowItemPoli extends StatelessWidget {
  RowItemPoli({this.title, this.icon, this.colorone, this.colortwo, this.status, this.iconcolor});
  final String title;
  final String status;
  final String icon;
  final Color colorone, colortwo, iconcolor;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.white, width: 2.0),
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: InkWell(
        customBorder: CircleBorder(),
        onTap: () {
          Navigator.pushNamed(context, '/listdoctor');
        },
        child: Container(
          height: 130,
          width: 107,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              gradient:LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [colorone, colortwo])
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset(icon,
                  color: iconcolor,
                  width: 50,
                  height: 50
              ),
              SizedBox(height: 28),
              Padding(
                padding:EdgeInsets.only(left: 10),
                child: Text(
                  title,
                  style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.grey,
                      fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding:EdgeInsets.only(left: 10),
                child: Text(
                  status,
                  style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.grey
                  ),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
