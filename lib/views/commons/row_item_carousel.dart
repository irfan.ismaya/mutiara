import 'package:flutter/material.dart';

class RowItemCarousel extends StatelessWidget {
  final String img;

  RowItemCarousel({this.img});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(right: 50.0, bottom: 20.0),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: Image.asset(img, fit: BoxFit.cover))
    );
  }
}
