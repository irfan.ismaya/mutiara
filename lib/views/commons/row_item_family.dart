import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/commons/circle_image_indicator.dart';

class RowItemFamily extends StatelessWidget {
  final String image;
  final String name;
  final String status;
  final String phone;

  RowItemFamily({this.image, this.name, this.status, this.phone,});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: Container(
          margin: EdgeInsets.only(bottom: 10),
          color: Colors.grey[50].withOpacity(0.9),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 90,
                child: Center(
                    child: CircleImageIndicator(
                      personImagePath: 'images/icon_poli/poli_admin.png',
                      color: const Color(0xFF558AED),
                    )
                )
              ),
              Expanded(
                  child:Container(
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(3),
                              child: Text(name,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    decoration: TextDecoration.none,
                                    color: Colors.grey[600],
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(3),
                              child: Text(status,
                                textAlign: TextAlign.left,
                                maxLines: 2,
                                style: TextStyle(
                                  decoration: TextDecoration.none,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(3),
                              child: Text(phone,
                                textAlign: TextAlign.left,
                                maxLines: 1,
                                style: TextStyle(
                                  decoration: TextDecoration.none,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                  )
              ),
            ],
          )
      ),
    );
  }
}
