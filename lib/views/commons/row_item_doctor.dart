import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/commons/circle_image_indicator.dart';

class RowItemDoctor extends StatelessWidget {
  final String name;
  final String time;
  final String qouta;
  final String day;
  final String image;
  final String icon;
  final GestureTapCallback onTap;

  RowItemDoctor({this.name, this.time, this.qouta, this.day, this.image, this.icon, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onTap,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.grey[50].withOpacity(0.9),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 90,
                  child: Center(
                      child: CircleImageIndicator(
                        personImagePath: 'images/icon_poli/poli_admin.png',
                        color: Colors.blue,
                      )
                  ),
                ),
                Expanded(
                    child:Container(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                child: Text(name,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Colors.grey[600],
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(day,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Colors.grey[600],
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              SizedBox(height: 3,),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Icon(Icons.timer, color: Colors.grey[600], size: 15,),
                                  SizedBox(width: 5,),
                                  Text(time,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.w500,
                                      fontSize: 13,
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        )
                    )
                ),
                Container(
                  height: 80,
                  width: 70,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topRight: Radius.circular(10), bottomRight: Radius.circular(10)),
                      gradient:LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [Colors.blue, Colors.blue]
                      )
                  ),
                  child: Center(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Text(qouta,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  decoration: TextDecoration.none,
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text("sisa qouta",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  decoration: TextDecoration.none,
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500
                              ),
                            ),
                          ),
                        ]
                    ),
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }
}
