import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/commons/circle_image_indicator.dart';

class RowItemDoctorPoli extends StatelessWidget {
  final String name;
  final String poli;
  final String image;
  final String icon;
  final bool info;
  final GestureTapCallback onTap;

  RowItemDoctorPoli({this.name, this.poli, this.image, this.icon, this.info, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onTap,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
            margin: EdgeInsets.only(bottom: 10),
            color: Colors.grey[50],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 90,
                  child: Center(
                      child: CircleImageIndicator(
                        personImagePath: 'images/icon_poli/poli_admin.png',
                        color: Colors.blue,
                      )
                  ),
                ),
                Expanded(
                    child:Container(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                child: Text(name,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Colors.grey[600],
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(poli,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      color: Colors.grey[600],
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                    )
                ),
                Visibility(
                  visible: info,
                  child: Container(
                    height: 80,
                    width: 70,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(topRight: Radius.circular(8), bottomRight: Radius.circular(8), topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)),
                        gradient:LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [Colors.blue, Colors.blue]
                        )
                    ),
                    child: Center(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: Icon(Icons.calendar_today, color: Colors.white, size: 20,),
                            ),
                            SizedBox(height: 2),
                            Align(
                              alignment: Alignment.center,
                              child: Text("buat",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    decoration: TextDecoration.none,
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Text("Reservasi",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    decoration: TextDecoration.none,
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500
                                ),
                              ),
                            ),
                          ]
                      ),
                    ),
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }
}
