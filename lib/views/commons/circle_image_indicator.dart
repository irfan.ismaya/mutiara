import 'package:flutter/material.dart';

class CircleImageIndicator extends StatelessWidget {
  final String personImagePath;
  final Color color;
  const CircleImageIndicator({
    Key key,
    this.personImagePath,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Container(
            padding: const EdgeInsets.all(3.4),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50.0),
              border: Border.all(
                width: 2.0,
                color: color,
              ),
            ),
            child: Container(
              width: 54.0,
              height: 54.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50.0),
                image: DecorationImage(
                    image: AssetImage(personImagePath),
                    fit: BoxFit.cover
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
