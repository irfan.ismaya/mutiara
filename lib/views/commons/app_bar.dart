import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';

class AppBarSmall extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final bool isBack;
  final BuildContext context;

  AppBarSmall({this.context, this.title, this.isBack = false});

  @override
  _AppBarSmallState createState() => _AppBarSmallState();

  @override
  Size get preferredSize => Size(MediaQuery.of(context).size.width, 130.0);
}

class _AppBarSmallState extends State<AppBarSmall> {
  Widget _buildBackButton() =>
      Visibility(
        visible: widget.isBack,
        child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
            size: 24,
          ),
      );

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      child: Stack(
        alignment: AlignmentDirectional.topStart,
        children: <Widget>[

          Container(
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 10.0, top: 20.0, right: 10, bottom: 10.0),
              child: Container(
                height: 40,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Align(
                        alignment: Alignment.centerLeft,
                          child: _buildBackButton(),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            '${this.widget.title}',
                            maxLines: 1,
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontFamily: 'Rubik',
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      color: Colors.yellowAccent,
                    )
                  ],
                ),
              ),
            ),
            decoration: BoxDecoration( gradient: ThemeColors.primaryGradient, )
          ),
          Positioned(
            top: -105,
            left: -80,
            width: 211,
            height: 211,
            child: Container(
              decoration: BoxDecoration(
                color: ThemeColors.whiteTransparent,
                shape: BoxShape.circle
              ),
            ),
          ),
          Positioned(
            top: -18,
            right: -28,
            width: 93,
            height: 93,
            child: Container(
              decoration: BoxDecoration(
                  color: ThemeColors.whiteTransparent,
                  shape: BoxShape.circle
              ),
            ),
          ),
          Visibility(
            visible: widget.isBack,
            child: GestureDetector(
              onTap: () {
                Navigator.of(widget.context).pop();
              },
              child: Container(
                  width: 50,
                  height: 80,
                  color: Colors.transparent,),
            ),
          ),
        ],
      ),
    );
  }
}


class AppBarBig extends StatefulWidget implements PreferredSizeWidget {
  final String name;
  final String avatar;
  final BuildContext context;

  AppBarBig({this.context, this.name, this.avatar});

  @override
  _AppBarBigState createState() => _AppBarBigState();

  @override
  Size get preferredSize => Size.fromHeight(80.0);
}

class _AppBarBigState extends State<AppBarBig> {

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration( gradient: ThemeColors.primaryGradient, ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left:15),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        SizedBox(height: 25.0,),
                        Text("Hai, ${widget.name}!", style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Rubik'
                        ),),
                        SizedBox(height: 2.0,),
                        Text("Selamat datang kembali", style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0
                        ),)
                      ]
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 15, top: 25),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(40.0),
                    child: FadeInImage.assetNetwork(
                      width: 40,
                        height: 40,
                        placeholder: 'assets/images/placeholder.png',
                        image: widget.avatar),
                  ),
                )
              ],
            ),
          ),
          Positioned(
            top: -105,
            left: -80,
            width: 211,
            height: 211,
            child: Container(
              decoration: BoxDecoration(
                  color: ThemeColors.whiteTransparent,
                  shape: BoxShape.circle
              ),
            ),
          ),
          Positioned(
            top: -18,
            right: -28,
            width: 93,
            height: 93,
            child: Container(
              decoration: BoxDecoration(
                  color: ThemeColors.whiteTransparent,
                  shape: BoxShape.circle
              ),
            ),
          )
        ],
      ),
    );
  }
}

