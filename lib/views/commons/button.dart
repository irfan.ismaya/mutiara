import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';

class Button extends StatelessWidget {
  final String text;
  final GestureTapCallback onTap;
  final bool noPadding;

  Button({this.text, this.onTap, this.noPadding = false});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onTap,
      child: Container(
        alignment: AlignmentDirectional.center,
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: noPadding ? 0 : 30, vertical: 25),
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        decoration: BoxDecoration(
          borderRadius: new BorderRadius.all(Radius.circular(8)),
          gradient: ThemeColors.buttonGradient,
          boxShadow: [
            BoxShadow(
              offset: Offset(0.0, 3.0),
                color: ThemeColors.buttonShadow,
                blurRadius: 6,
                spreadRadius: 0)
          ]
        ),
        child: Text(text,
            style:
            TextStyle(color: Colors.white, fontSize: 20,
              fontWeight: FontWeight.w600,
              fontFamily: 'Rubik',)),
      ),
    );
  }
}

class ButtonConfirm extends StatelessWidget {
  final String text;
  final GestureTapCallback onTap;
  final bool noPadding;

  ButtonConfirm({this.text, this.onTap, this.noPadding = false});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onTap,
      child: Container(
        alignment: AlignmentDirectional.center,
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: noPadding ? 0 : 30, vertical: 10),
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        decoration: BoxDecoration(
            borderRadius: new BorderRadius.all(Radius.circular(8)),
            gradient: ThemeColors.redGradient,
            boxShadow: [
              BoxShadow(
                  offset: Offset(0.0, 3.0),
                  color: ThemeColors.buttonShadow,
                  blurRadius: 6,
                  spreadRadius: 0)
            ]
        ),
        child: Text(text,
            style:
            TextStyle(color: Colors.white, fontSize: 20,
              fontWeight: FontWeight.w600,
              fontFamily: 'Rubik',)),
      ),
    );
  }
}

class TextButton extends StatelessWidget {
  final String title;
  final GestureTapCallback onTap;

  TextButton({this.title = 'Selengkapnya', this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Align(
        alignment: AlignmentDirectional.centerEnd,
        child: Container(
          padding: EdgeInsets.only(top: 15, bottom: 15, left: 15),
          child: Text(title,
            textAlign: TextAlign.right,
            style: TextStyle(
                fontFamily: 'Rubik',
                fontSize: 13,
                fontWeight: FontWeight.w600,
                color: ThemeColors.sectionSubTitle
            ),
          ),
        ),
      ),
    );
  }
}
