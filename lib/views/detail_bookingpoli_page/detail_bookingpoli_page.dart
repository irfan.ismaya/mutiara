import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:mvvm_mutiara/viewmodels/family_viewmodel.dart';
import 'package:mvvm_mutiara/viewmodels/polyclinic_viewmodel.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/views/success_page/success_page.dart';

final FamilyViewModel viewModelFamily = FamilyViewModel(api: ServiceApi());
final PolyClinicViewModel viewModelPoli = PolyClinicViewModel(api: ServiceApi());
enum ConfirmAction { CANCEL, ACCEPT }

class DetailBookingPoliPage extends StatefulWidget {
  int jadwalId;
  String namaDokter;
  String jadwalStart;
  String jadwalEnd;
  String waktupraktek;
  String hari;
  String quota;
  int statusQuota;
  String datefilter;
  String poli_name;

  String kodeKeluarga;
  String namaKeluarga;

  DetailBookingPoliPage({Key key,
    @required this.jadwalId,
    @required this.namaDokter,
    @required this.jadwalStart,
    @required this.jadwalEnd,
    @required this.waktupraktek,
    @required this.kodeKeluarga,
    @required this.namaKeluarga,
    @required this.hari,
    @required this.quota,
    @required this.statusQuota,
    @required this.datefilter,
    @required this.poli_name}) : super(key: key);

  @override
  _DetailBookingPoliPageState createState() => _DetailBookingPoliPageState();
}

class _DetailBookingPoliPageState extends State<DetailBookingPoliPage> {
  int jadwalId;
  int kodebooking;
  String namaDokter;
  String jadwalStart;
  String jadwalEnd;
  String waktupraktek;
  String hari;
  String quota;
  int statusQuota;
  String datefilter;
  String poli_name;
  SharedPreferences sharedPreferences;
  ResponseFamily responseFamily;
  ResponseBooking responseBooking;
  ResponseConfirmQueue responseConfirmQueue;
  bool _isLoading = false;

  String kodeKeluarga;
  String namaKeluarga;

  String _kodeKeluarga;
  List data = List();

  Future<String> _getListFamily() async{
    sharedPreferences = await SharedPreferences.getInstance();
    try{
      String token = sharedPreferences.getString("api_token");
      bool req = await viewModelFamily.getFamily(token);
      responseFamily = await viewModelFamily.family;
      if(req){
        if(responseFamily.code == 200){
          setState(() {
            data = responseFamily.data;
          });
        }else{
          HelpersView.toast(responseFamily.message, Colors.orange);
        }
      }else{
        HelpersView.toast(responseFamily.message, ThemeColors.statePending);
      }
    }catch(ex){
      HelpersView.toast(responseFamily.message, ThemeColors.statePending);
    }finally{

    }
    return 'Success';
  }

  Future<ConfirmAction> _messageConfirmDialog(BuildContext context, String message, String queue, String timecoming) async {
    return showDialog<ConfirmAction>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Order Berhasil'),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Ya'),
              onPressed: () {
                Navigator.popUntil(context, ModalRoute.withName('/mainmenu'));
              },
            )
          ],
        );
      },
    );
  }

  void goToSuccessPage(BuildContext context, String message, String queue, String timecoming){
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SuccessPage(
            message: message,
            queue: queue,
            timecoming: timecoming,
          ),
        )
    );
  }

  Future<String> _bookingOrder(BuildContext context) async{
    setState(() {
      _isLoading = true;
    });
    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("api_token");
    BookingRequest bookingRequest = BookingRequest( jadwalId: jadwalId, bookingDate: datefilter, kodeKeluarga: int.parse(_kodeKeluarga));
    bool req = await viewModelPoli.bookingPoly(bookingRequest, token);
    responseBooking = await viewModelPoli.bookingpoly;
    if(req){
      if(responseBooking.code == 200 || responseBooking.code == 201){
        setState(() {
          _isLoading = false;
           kodebooking = responseBooking.data.kodebooking;
        });
        _queueConfirm(context, kodebooking);
      }else{
        HelpersView.toast(responseBooking.message, Colors.orange);
        setState(() {
          _isLoading = false;
        });
      }
    }else{
      HelpersView.toast(responseBooking.message, ThemeColors.statePending);
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  Future<String> _queueConfirm(BuildContext context, int kodebooking) async{
    setState(() {
      _isLoading = true;
    });
    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("api_token");
    bool req = await viewModelPoli.getQueueConfirm(token, kodebooking);
    responseConfirmQueue = await viewModelPoli.getqueueconfirm;
    if(req){
      if(responseConfirmQueue.code == 200 || responseConfirmQueue.code == 201){
        setState(() {
          _isLoading = false;
        });
        goToSuccessPage(context, responseConfirmQueue.message.toString(), responseConfirmQueue.data.noAntrian.toString(), responseConfirmQueue.data.jamKedatangan.toString());
      }else{
        HelpersView.toast(responseConfirmQueue.message, Colors.orange);
        setState(() {
          _isLoading = false;
        });
      }
    }else{
      HelpersView.toast(responseConfirmQueue.message, ThemeColors.statePending);
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: "Konfirmasi Pemesanan",
          isBack: true,
        ),
        backgroundColor: ThemeColors.background,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 15, top: 10, bottom: 10),
                child: Text("Detail Pesanan Anda",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Colors.grey[600],
                      fontSize: 18,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Text("Klinik"),
                    Expanded(child: Container()),
                    Text(poli_name, style: TextStyle(
                      fontWeight: FontWeight.w600
                    ),),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 25, right: 25),
                child: Divider(),
              ),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Text("Dokter"),
                    Expanded(child: Container()),
                    Text(widget.namaDokter, style: TextStyle(
                        fontWeight: FontWeight.w600
                    )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 25, right: 25),
                child: Divider(),
              ),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Text("Tanggal"),
                    Expanded(child: Container()),
                    Text(datefilter, style: TextStyle(
                        fontWeight: FontWeight.w600
                    )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 25, right: 25),
                child: Divider(),
              ),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Text("Jam"),
                    Expanded(child: Container()),
                    Text(widget.jadwalStart+"-"+widget.jadwalEnd, style: TextStyle(
                        fontWeight: FontWeight.w600
                    )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 25, right: 25),
                child: Divider(),
              ),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Text("Anggota Keluarga"),
                    Expanded(child: Container()),
                    DropdownButtonHideUnderline(
                      child: DropdownButton(
                        items: data.map((item) {
                          return DropdownMenuItem(
                            child: Text(item.namalengkap),
                            value: item.kodeKeluarga,
                          );
                        }).toList(),
                        onChanged: (newVal) {
                          setState(() {
                            _kodeKeluarga = newVal;
                          });
                        },
                        value: _kodeKeluarga,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 25, right: 25),
                child: Divider(),
              ),
              Button(
                onTap: () {
                  if(_kodeKeluarga != null){
                    _bookingOrder(context);
                  }else{
                    HelpersView.toast("Silahkan Pilih Anggota Keluarga", ThemeColors.statePending);
                  }
                },text: "Order",
              ),
              Visibility(
                visible: _isLoading,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: CircularProgressIndicator(strokeWidth: 1,),
                ),
              )
            ]
          )
        ),
    );
  }

  @override
  void initState() {
    super.initState();
    _getListFamily();
    kodeKeluarga = widget.kodeKeluarga;
    namaKeluarga = widget.namaKeluarga;
    jadwalId = widget.jadwalId;
    namaDokter = widget.namaDokter;
    jadwalStart = widget.jadwalStart;
    jadwalEnd = widget.jadwalEnd;
    waktupraktek = widget.waktupraktek;
    hari = widget.hari;
    quota = widget.quota;
    statusQuota = widget.statusQuota;
    datefilter = widget.datefilter;
    poli_name = widget.poli_name;
  }
}
