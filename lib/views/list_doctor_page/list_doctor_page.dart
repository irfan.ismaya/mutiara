import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/utils/helpers.dart' as helpers;
import 'package:mvvm_mutiara/views/commons/row_item_doctor.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:mvvm_mutiara/views/detail_bookingpoli_page/detail_bookingpoli_page.dart';

final ServiceApi _serviceApi = ServiceApi();

class ListDocterPage extends StatefulWidget {
  final int poli_id;
  final String date_time;
  final String title_poli;
  ListDocterPage({Key key, @required this.poli_id, @required this.date_time , @required this.title_poli}) : super(key: key);

  @override
  _ListDocterPageState createState() => _ListDocterPageState();
}

class _ListDocterPageState extends State<ListDocterPage> {
  String api_token = "";
  int poli_id;
  String date_time;
  String title_poli;
  String datefilter;
  bool initData = false;
  SharedPreferences sharedPreferences;

  void goToDetailBookingPoli(DataPolySchedule data, String datefilter, String poli_name){
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetailBookingPoliPage(
            jadwalId: data.jadwalId,
            namaDokter: data.namaDokter,
            jadwalStart: data.jadwalStart,
            jadwalEnd: data.jadwalEnd,
            hari: data.hari,
            quota: data.quota,
            statusQuota: data.statusQuota,
            datefilter: datefilter,
            poli_name: poli_name,
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    PolyClinicSheduleRequest poli = PolyClinicSheduleRequest(poliId: poli_id, bookingDate: initData ? datefilter: date_time);
    PolyClinicSheduleRequest poliFilter = PolyClinicSheduleRequest(poliId: poli_id, bookingDate: initData ? datefilter: date_time);
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: widget.title_poli,
          isBack: true,
        ),
        backgroundColor: ThemeColors.background,
        body: Container(
          child: FutureBuilder(
              future: initData ? _serviceApi.listPolySchedule(poliFilter, api_token) : _serviceApi.listPolySchedule(poli, api_token),
              // ignore: missing_return
              builder: (context, snapshot){
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.active:
                  case ConnectionState.waiting:
                    return Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 1,
                        )
                    );
                    break;
                  case ConnectionState.done:
                    if (snapshot.hasData) {
                      return ListView.builder(
                          itemCount: snapshot.data.data == null ? 0 : snapshot.data.data.length,
                          primary: false,
                          itemBuilder: (BuildContext context, int i){
                            if (i == 0) {
                              return  Container(
                                color: Colors.orange,
                                child: Column(
                                  children: <Widget>[
                                    Image(
                                        height: 200.0,
                                        fit: BoxFit.fill,
                                        image: AssetImage('images/banner/banner_kandungan.png')
                                    )
                                  ],
                                ),
                              );
                            }
                            return Container(
                              padding: EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
                              child: RowItemDoctor(
                                name: snapshot.data.data[i].namaDokter,
                                time: snapshot.data.data[i].jadwalStart+"-"+snapshot.data.data[i].jadwalEnd,
                                day: snapshot.data.data[i].hari+", "+snapshot.data.data[i].waktupraktek,
                                qouta: snapshot.data.data[i].quota,
                                onTap: (){
                                  DataPolySchedule data = DataPolySchedule(
                                    jadwalId: snapshot.data.data[i].jadwalId,
                                    namaDokter: snapshot.data.data[i].namaDokter,
                                    jadwalStart: snapshot.data.data[i].jadwalStart,
                                    jadwalEnd: snapshot.data.data[i].jadwalEnd,
                                    hari: snapshot.data.data[i].hari,
                                    quota: snapshot.data.data[i].quota,
                                    statusQuota: snapshot.data.data[i].statusQuota,
                                  );
                                  goToDetailBookingPoli(data, initData ? datefilter: widget.date_time, helpers.strPoliName(poli_id.toString()));
                                },
                              ),
                            );
                          }
                      );
                    }else if(snapshot.data == null) {
                      return Center(
                          child: Text("No Data")
                      );
                    }else if(snapshot.hasError) {
                      return Center(
                          child: Text("No Internet Connection")
                      );
                    }
                    break;
                }
              }
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            DatePicker.showDatePicker(context,
                showTitleActions: true,
                theme: DatePickerTheme(
                    itemStyle: TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.w100),
                    doneStyle:
                    TextStyle(color: Colors.grey, fontSize: 16)),
                onChanged: (date) {
                  print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
                }, onConfirm: (date) {
                  setState(() {
                    initData = true;
                    datefilter = date.toString().substring(0,10);
                  });

                  PolyClinicSheduleRequest poliFilter = PolyClinicSheduleRequest(poliId: poli_id, bookingDate: initData ? datefilter: date_time);
                  _serviceApi.listPolySchedule(poliFilter, api_token);
                }, currentTime: DateTime.now(), locale: LocaleType.en);

          },
          label: Text('Pilih Tanggal'),
          icon: Icon(Icons.calendar_today),
          backgroundColor: Colors.pink,
        )
    );
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      api_token = sharedPreferences.getString("api_token");
    });
  }

  @override
  void initState() {
    super.initState();
    getCredential();
    poli_id = widget.poli_id;
    date_time = widget.date_time;
  }

}

