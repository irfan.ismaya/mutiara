import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/viewmodels/user_viewmodel.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:easy_localization/easy_localization.dart';
import 'package:shared_preferences/shared_preferences.dart';

final UserViewModel viewModel = UserViewModel(api: ServiceApi());

class PinCodePage extends StatefulWidget {
  @override
  _PinCodePageState createState() => _PinCodePageState();
}

class _PinCodePageState extends State<PinCodePage> {
  static final int _pinLength = 4;
  bool _isLoading = false;

  static final TextStyle _textStyle = TextStyle(
    color: Colors.black,
    fontSize: 40,
  );

  final FocusNode _otpFocus = FocusNode();
  SharedPreferences sharedPreferences;
  ResponseMessage dataCheckOtp, dataGetOtp;
  PinEditingController _pinEditingController =
  PinEditingController(pinLength: _pinLength, autoDispose: false);

  PinDecoration _pinDecoration = UnderlineDecoration(
    textStyle: _textStyle,
    enteredColor: Colors.green,
  );

  Future<String> _checkOtp(BuildContext context) async{
    setState(() {
      _isLoading = true;
    });
    try{
      sharedPreferences = await SharedPreferences.getInstance();
      String telephone = sharedPreferences.getString("telephone");
      CheckOtpRequest otpRequest = CheckOtpRequest(code_otp: _pinEditingController.text, telephone: telephone);
      bool req = await viewModel.checkOtp(otpRequest);
      dataCheckOtp = await viewModel.checkotp;
      if(req){
        if(dataCheckOtp.code == 200){
          HelpersView.toast(dataCheckOtp.message, ThemeColors.stateApproval);
          _pinEditingController.clear();
        }else{
          HelpersView.toast(dataCheckOtp.message, Colors.orange);
        }
      }else{
        HelpersView.toast(dataCheckOtp.message, ThemeColors.statePending);
      }
    }catch(ex){
      HelpersView.toast(dataCheckOtp.message, ThemeColors.statePending);
    }finally{
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  Future<String> _getOtp(BuildContext context) async{
    setState(() {
      _isLoading = true;
    });
    try{
      bool req = await viewModel.getOtp("");
      dataGetOtp = await viewModel.getotp;
      if(req){
        if(dataGetOtp.code == 200){
          HelpersView.toast(dataGetOtp.message, ThemeColors.stateApproval);
        }else if(dataGetOtp.code == 422){
          HelpersView.toast(dataGetOtp.message, Colors.orange);
        }else{
          HelpersView.toast(dataGetOtp.message, Colors.orange);
        }
      }else{
        HelpersView.toast(dataGetOtp.message, ThemeColors.statePending);
      }
    }catch(ex){
      HelpersView.toast(dataGetOtp.message, ThemeColors.statePending);
    }finally{
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarSmall(
        context: context,
        title: AppLocalizations.of(context).tr('page_pincode.title'),
        isBack: true,
      ),
      backgroundColor: ThemeColors.background,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text(AppLocalizations.of(context).tr('page_pincode.label_title'),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.grey,
                fontSize: 22
              ),
            ),
            Padding(
              padding:EdgeInsets.all(30),
              child: PinInputTextField(
                pinLength: 4,
                enabled: !_isLoading,
                focusNode: _otpFocus,
                decoration: _pinDecoration,
                pinEditingController: _pinEditingController,
                autoFocus: true,
                textInputAction: TextInputAction.go,
                onSubmit: (pin) {
                  debugPrint('submit pin:$pin');
                },
              ),
            ),
            Button(
              onTap: () {
                if(_pinEditingController.text.isEmpty){
                  HelpersView.toast("tidak boleh kosong", Colors.orange);
                }else{
                  _checkOtp(context);
                }
              },text: AppLocalizations.of(context).tr('page_pincode.form.send_otp'),
            ),
            FlatButton(
              onPressed: (){
                _getOtp(context);
              },
              child: Text(
                AppLocalizations.of(context).tr('page_pincode.form.resend'),
                style: TextStyle(
                    fontSize: 20,
                    color: ThemeColors.textGrey3
                ),
              ),
            ),
            Visibility(
              visible: _isLoading,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: CircularProgressIndicator(strokeWidth: 1,),
              ),
            )
          ],
        )
      ),
    );
  }
}
