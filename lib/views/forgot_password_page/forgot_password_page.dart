import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/viewmodels/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';

final UserViewModel viewModel = UserViewModel(api: ServiceApi());

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool checkValue = false;
  String _telephone;
  bool _isLoading = false;

  final FocusNode _telephoneFocus = FocusNode();
  ResponseMessage data;
  TextEditingController _telephoneController = new TextEditingController();

  String _validatorTelephone(value) {
    String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_forgotpass.validation.field_phone_empty');
    }else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_forgotpass.validation.field_phone_regex');
    }
    return null;
  }

  Future<String> _forgotPassword(BuildContext context) async{
    setState(() {
      _isLoading = true;
    });
    try{
      bool req = await viewModel.forgotPassword(_telephone);
      data = await viewModel.forgotpassword;
      if(req){
        if(data.code == 200){
          HelpersView.toast(data.message, ThemeColors.stateApproval);
          _telephoneController.clear();
          Navigator.of(context).pop();
        }else{
          HelpersView.toast(data.message, Colors.orange);
        }
      }else{
        HelpersView.toast(data.message, ThemeColors.statePending);
      }
    }catch(ex){
      HelpersView.toast(data.message, ThemeColors.statePending);
    }finally{
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarSmall(
        context: context,
        title: AppLocalizations.of(context).tr('page_forgotpass.title'),
        isBack: true,
      ),
      backgroundColor: ThemeColors.background,
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 25),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
                Visibility(
                  visible: _isLoading,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: CircularProgressIndicator(strokeWidth: 1,),
                  ),
              ),
              Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: ThemeColors.shadow,
                                blurRadius: 6,
                                spreadRadius: 4)
                          ]),
                      child: TextFormField(
                        enabled: !_isLoading,
                        validator: _validatorTelephone,
                        onSaved: (value) => _telephone = value,
                        focusNode: _telephoneFocus,
                        controller: _telephoneController,
                        keyboardType: TextInputType.phone,
                        style: TextStyle(color: ThemeColors.textField),
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.phone_iphone),
                            hintText: AppLocalizations.of(context).tr('page_forgotpass.form.hint_phone'),
                            hintStyle:
                            TextStyle(color: ThemeColors.textField),
                            filled: true,
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius:
                              BorderRadius.all(Radius.circular(5.0)),
                            ),
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 16.0)),
                      ),
                    ),
                    Button(
                      onTap: () {
                        if (!_isLoading) {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            _forgotPassword(context);
                          } else {
                            setState(() => _autoValidate = true);
                          }
                        }
                      },text: AppLocalizations.of(context).tr('page_forgotpass.form.submit'),
                    ),
                  ],
                ),
              )
            ]
        ),
      )
    );
  }
}
