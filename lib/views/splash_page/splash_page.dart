import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => new _SplashPageState();
}

class _SplashPageState extends State<SplashPage>{
  bool checkvalue = false;
  String telephone;
  String password;
  String token;
  SharedPreferences sharedPreferences;

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      telephone = sharedPreferences.getString("telephone");
      password = sharedPreferences.getString("password");
      token = sharedPreferences.getString("api_token");
      checkvalue = sharedPreferences.getBool("check");
      if (telephone != null && password != null) {
        if (token != null) {
          Navigator.pushReplacementNamed(context, '/mainmenu');
        }else{
          Navigator.pushReplacementNamed(context, '/login');
        }
      }else{
        if (checkvalue != null) {
          Navigator.pushReplacementNamed(context, '/login');
        }else{
          Navigator.pushReplacementNamed(context, '/appintro');
        }
      }
    });
  }

  void initState(){
    super.initState();
    Timer(Duration(seconds: 3),  () => getCredential());
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.blue.shade900, Colors.blue.shade600, Colors.blue.shade100])),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image(
                            width: 180.0,
                            height: 180.0,
                            fit: BoxFit.fill,
                            image: new AssetImage('images/logomutiara.png')
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}