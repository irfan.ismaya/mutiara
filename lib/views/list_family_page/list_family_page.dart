import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/viewmodels/family_viewmodel.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/views/commons/row_item_family.dart';

final FamilyViewModel viewModelFamily = FamilyViewModel(api: ServiceApi());
final ServiceApi _serviceApi = ServiceApi();

class ListFamilyPage extends StatefulWidget {
  final FamilyViewModel viewModel;
  ListFamilyPage({Key key, @required this.viewModel}) : super(key: key);

  @override
  _ListFamilyPageState createState() => _ListFamilyPageState();
}

class _ListFamilyPageState extends State<ListFamilyPage> {
  bool initData = false;
  String api_token = "";
  SharedPreferences sharedPreferences;

  Future<Null> _onRefresh() async {
    await new Future.delayed(new Duration(seconds: 3));
    setState(() {
      initData = true;
    });
    initData ? _serviceApi.getFamily(api_token):_serviceApi.getFamily(api_token);

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: 'Anggota Keluarga',
          isBack: true,
        ),
        backgroundColor: ThemeColors.background,
        body: Container(
            child: FutureBuilder(
                future: initData ? _serviceApi.getFamily(api_token):_serviceApi.getFamily(api_token),
                // ignore: missing_return
                builder: (context, snapshot){
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.active:
                    case ConnectionState.waiting:
                      return Center(
                          child: CircularProgressIndicator(
                            strokeWidth: 1,
                          )
                      );
                      break;
                    case ConnectionState.done:
                      if (snapshot.hasData) {
                        return RefreshIndicator(
                          child: ListView.builder(
                              itemCount: snapshot.data.data == null ? 0 : snapshot.data.data.length,
                              padding: EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
                              primary: false,
                              itemBuilder: (BuildContext context, int i){
                                return RowItemFamily(
                                  image: "images/icon_poli/poli_anak.png",
                                  name: snapshot.data.data[i].namalengkap,
                                  status: snapshot.data.data[i].statusDalamKeluarga,
                                  phone: snapshot.data.data[i].notelpon,
                                );
                              }
                          ),
                          onRefresh: _onRefresh,
                        );
                      }else if(snapshot.data == null) {
                        return Center(
                            child: Text("No Data")
                        );
                      }else if(snapshot.hasError) {
                        return Center(
                            child: Text("No Internet Connection")
                        );
                      }
                      break;
                  }
                }
            )
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            Navigator.pushNamed(context, '/addfamily');
          },
          label: Text('Anggota Keluarga'),
          icon: Icon(Icons.add),
          backgroundColor: Colors.pink,
        ),
    );
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      api_token = sharedPreferences.getString("api_token");
    });
  }

  @override
  void initState() {
    super.initState();
    getCredential();
  }
}
