import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/viewmodels/polyclinic_viewmodel.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/models/response_search_doctor.dart';
import 'package:mvvm_mutiara/views/schedule_doctor/schedule_doctor_page.dart';
import 'package:mvvm_mutiara/utils/helpers.dart' as helpers;
import 'package:intl/intl.dart';

final PolyClinicViewModel viewModelPoli = PolyClinicViewModel(api: ServiceApi());
final ServiceApi _serviceApi = ServiceApi();

class SearchDoctorPage extends StatefulWidget {
  final SearchDoctorPage viewModelPolyClinic;
  SearchDoctorPage({Key key, @required this.viewModelPolyClinic}) : super(key: key);

  @override
  _SearchDoctorPageState createState() => _SearchDoctorPageState();
}

class _SearchDoctorPageState extends State<SearchDoctorPage> {
  String api_token = "";
  SharedPreferences sharedPreferences;
  bool initData = false;

  void goToDetailBooking(DokterJadwal data, String datefilter, String poli_name){
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ScheduleDoctorPage(
            jadwalId: data.jadwalId,
            namaDokter: data.dokterName,
            jadwalStart: data.jadwalStart,
            jadwalEnd: data.jadwalEnd,
            hari: data.hari,
            waktupraktek: data.waktupraktek,
            datefilter: datefilter,
            poli_name: poli_name,
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarSmall(
        context: context,
        title: 'Cari Dokter',
        isBack: true,
      ),
      backgroundColor: ThemeColors.background,
      body: Container(
        child: FutureBuilder(
            future: _serviceApi.listSearchDoctor(api_token, 1),
            // ignore: missing_return
            builder: (context, snapshot){
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  break;
                case ConnectionState.active:
                  break;
                case ConnectionState.waiting:
                  return Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 1,
                      )
                  );
                  break;
                case ConnectionState.done:
                  if (snapshot.hasData) {
                    return ListView.builder(
                        itemCount: snapshot.data.data.dokter == null ? 0 : snapshot.data.data.dokter.length,
                        padding: EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
                        primary: false,
                        itemBuilder: (BuildContext context, int i) =>
                          ExpansionTile(
                            title: Container(
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.all(3),
                                        child: Text(snapshot.data.data.dokter[i].dokterName.toString(),
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              decoration: TextDecoration.none,
                                              color: Colors.grey[600],
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(3),
                                        child: Text(snapshot.data.data.dokter[i].dokterProfession.toString(),
                                          textAlign: TextAlign.left,
                                          maxLines: 2,
                                          style: TextStyle(
                                            decoration: TextDecoration.none,
                                            color: Colors.grey[600],
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                            ),
                            children: <Widget>[
                              Container(
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: snapshot.data.data.dokter[i].dokterJadwal == null ? 0 : snapshot.data.data.dokter[i].dokterJadwal.length,
                                    padding: EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
                                    primary: false,
                                    itemBuilder: (BuildContext context, int y) =>
                                        Container(
                                          margin: EdgeInsets.all(10),
                                          child: ListTile(
                                            leading: Stack(
                                              children: <Widget>[
                                                Container(
                                                  margin: EdgeInsets.only(left: 5),
                                                  height: 55,
                                                  width: 55,
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.only(topRight: Radius.circular(30), bottomRight: Radius.circular(30), topLeft: Radius.circular(30), bottomLeft: Radius.circular(30)),
                                                      gradient:LinearGradient(
                                                          begin: Alignment.topRight,
                                                          end: Alignment.bottomLeft,
                                                          colors: [helpers.strColor(snapshot.data.data.dokter[i].dokterJadwal[y].poliName.toString()), helpers.strColor(snapshot.data.data.dokter[i].dokterJadwal[y].poliName.toString())]
                                                      )
                                                  ),
                                                  child: Center(
                                                    child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: <Widget>[
                                                          Align(
                                                            alignment: Alignment.center,
                                                            child: Image.asset(helpers.strIcon(snapshot.data.data.dokter[i].dokterJadwal[y].poliName.toString()), width: 50,height: 50),
                                                          ),
                                                        ]
                                                    ),
                                                  ),
                                                ),
                                                Positioned(
                                                  top: -10,
                                                  left: -10,
                                                  width: 50,
                                                  height: 50,
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        color: Color(0x1BFFFFFF),
                                                        shape: BoxShape.circle
                                                    ),
                                                  ),
                                                ),
                                                Positioned(
                                                  bottom: -10,
                                                  right: -10,
                                                  width: 50,
                                                  height: 50,
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        color: Color(0x1BFFFFFF),
                                                        shape: BoxShape.circle
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            title: Text(snapshot.data.data.dokter[i].dokterJadwal[y].hari.toString()+"-"+snapshot.data.data.dokter[i].dokterJadwal[y].waktupraktek.toString()+", "+snapshot.data.data.dokter[i].dokterJadwal[y].poliName.toString()),
                                            subtitle: Text(snapshot.data.data.dokter[i].dokterJadwal[y].jadwalStart.toString()+"-"+snapshot.data.data.dokter[i].dokterJadwal[y].jadwalEnd.toString()),
                                            onTap: () {
                                              DokterJadwal data = DokterJadwal(
                                                jadwalId: snapshot.data.data.dokter[i].dokterJadwal[y].jadwalId,
                                                dokterName: snapshot.data.data.dokter[i].dokterJadwal[y].dokterName.toString(),
                                                jadwalStart: snapshot.data.data.dokter[i].dokterJadwal[y].jadwalStart.toString(),
                                                jadwalEnd: snapshot.data.data.dokter[i].dokterJadwal[y].jadwalEnd.toString(),
                                                hari: snapshot.data.data.dokter[i].dokterJadwal[y].hari.toString(),
                                                waktupraktek: snapshot.data.data.dokter[i].dokterJadwal[y].waktupraktek.toString(),
                                              );
                                              goToDetailBooking(data, new DateFormat("yyyy-MM-dd").format(DateTime.now()).toString(), snapshot.data.data.dokter[i].dokterJadwal[y].poliName.toString());
                                            },
                                          ),
                                        )
                                ),
                              )
                            ],
                          ),

                    );
                  }else if(snapshot.data == null) {
                    return Center(
                        child: Text("No Data")
                    );
                  }else if(snapshot.hasError) {
                    return Center(
                        child: Text("No Internet Connection")
                    );
                  }
                  break;
              }
            }
        ),
      ),
    );
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      api_token = sharedPreferences.getString("api_token");
    });
  }

  @override
  void initState() {
    super.initState();
    getCredential();
  }
}
