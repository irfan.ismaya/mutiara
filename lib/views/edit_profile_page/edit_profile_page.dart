import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/viewmodels/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:convert';

final UserViewModel viewModelUser = UserViewModel(api: ServiceApi());

class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool checkValue = false;
  bool _isLoading = false;
  bool _boolFoto = false;
  SharedPreferences sharedPreferences;
  ResponseMessage responseMessage;

  String _name;
  String _telephone;
  String _email;
  String _address;

  ResponseGetProfile responseGetProfile;

  String name;
  String address;
  String telephone;
  String email;
  String image_fullpath;
  int role_id;
  String role_name;

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _telephoneFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _addressFocus = FocusNode();

  ResponseMessage data;
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _telephoneController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _addressController = new TextEditingController();

  File _image;
  String imgBase64;
  String fileImg = "data:image/png;base64,";

  Future getImageCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera, maxHeight:480, maxWidth: 640);
    setState(() {
      _image = image;
      _boolFoto = true;
    });
    File imageFile = new File(_image.path);
    List<int> imageBytes = imageFile.readAsBytesSync();
    setState(() {
      imgBase64 = fileImg + base64.encode(imageBytes);
    });
  }

  Future getImageGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery, maxHeight:480, maxWidth: 640);
    setState(() {
      _image = image;
      _boolFoto = true;
    });
    File imageFile = new File(_image.path);
    List<int> imageBytes = imageFile.readAsBytesSync();
    setState(() {
      imgBase64 = fileImg + base64.encode(imageBytes);
    });
  }

  String _validatorTelephone(value) {
    String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_editprofile.validation.field_phone_empty');
    }else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_editprofile.validation.field_phone_regex');
    }
    return null;
  }

  String _validatorName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_editprofile.validation.field_name_empty');
    } else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_editprofile.validation.field_name_regex');
    }
    return null;
  }

  String _validatorEmail(String value) {
    String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_editprofile.validation.field_email_empty');
    } else if(!regExp.hasMatch(value)){
      return AppLocalizations.of(context).tr('page_editprofile.validation.field_email_regex');
    }else {
      return null;
    }
  }

  String _validatorAddress(value) {
    if (value.isEmpty) {
      return AppLocalizations.of(context).tr('page_editprofile.validation.field_address_empty');
    }
    if (value.length < 10) {
      return AppLocalizations.of(context).tr('page_editprofile.validation.field_address_regex');
    }
    return null;
  }

  void LogPrint(Object object) async {
    int defaultPrintLength = 1020;
    if (object == null || object.toString().length <= defaultPrintLength) {
      print(object);
    } else {
      String log = object.toString();
      int start = 0;
      int endIndex = defaultPrintLength;
      int logLength = log.length;
      int tmpLogLength = log.length;
      while (endIndex < logLength) {
        print(log.substring(start, endIndex));
        endIndex += defaultPrintLength;
        start += defaultPrintLength;
        tmpLogLength -= defaultPrintLength;
      }
      if (tmpLogLength > 0) {
        print(log.substring(start, logLength));
      }
    }
  }

  Future<void> _messageConfirmDialog(BuildContext context, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Order Berhasil'),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Ya'),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  Future<String> _editProfile(BuildContext context) async{
    setState(() {
      _isLoading = true;
    });
    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("api_token");
    UpdateUserRequest bookingRequest = UpdateUserRequest(
      email: _emailController.text,
      telephone: _telephoneController.text,
      name: _nameController.text,
      address: _addressController.text,
      image: imgBase64
    );
    bool req = await viewModelUser.updateUser(bookingRequest, token);
    responseMessage = await viewModelUser.updateuser;
    if(req){
      if(responseMessage.code == 200 || responseMessage.code == 201){
        setState(() {
          _isLoading = false;
        });
        _messageConfirmDialog(context, responseMessage.message);
      }else{
        HelpersView.toast(responseMessage.message, Colors.orange);
        setState(() {
          _isLoading = false;
        });
      }
    }else{
      HelpersView.toast(responseMessage.message, ThemeColors.statePending);
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  Future<String> _getProfile() async{
    setState(() {
      _isLoading = true;
    });

    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("api_token");
    bool req = await viewModelUser.getProfile(token);
    responseGetProfile = await viewModelUser.getprofile;
    if(req){
      if(responseGetProfile.code == 200){
        setState(() {
          _isLoading = false;
          name = responseGetProfile.data.name;
          address = responseGetProfile.data.address;
          telephone = responseGetProfile.data.telephone;
          email = responseGetProfile.data.email;
          image_fullpath = responseGetProfile.data.imageFullpath;
          role_id = responseGetProfile.data.roleId;
          role_name = responseGetProfile.data.roleName;
        });

        _nameController.text = name != null ? name : "-";
        _emailController.text = email != null ? email : "-";
        _telephoneController.text = telephone != null ? telephone : "-";
        _addressController.text = address != null ? address : "-";
      }else{
        print("MESSAGE");
        HelpersView.toast(responseGetProfile.message, Colors.orange);
        setState(() {
          _isLoading = false;
        });
      }
    }else{
      print("MESSAGE");
      print(responseGetProfile.message);
      HelpersView.toast("TESR", ThemeColors.statePending);
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  @override
  void initState() {
    super.initState();
    _getProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: AppLocalizations.of(context).tr('page_editprofile.title'),
          isBack: true,
        ),
        backgroundColor: ThemeColors.background,
        body: SingleChildScrollView(
          padding: const EdgeInsets.only(top: 25),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Visibility(
                  visible: _isLoading,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: CircularProgressIndicator(strokeWidth: 1,),
                  ),
                ),
                Form(
                  key: _formKey,
                  autovalidate: _autoValidate,
                  child: Column(
                    children: <Widget>[
                      Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              GestureDetector(
                                child: _boolFoto ? Container(
                                  width: 100,
                                  height: 100,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: _image == null
                                            ? AssetImage('images/icon_poli/poli_admin.png')
                                            : FileImage(_image, scale: 100)
                                    )
                                  )
                                ):Container(
                                    width: 100,
                                    height: 100,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: NetworkImage(image_fullpath != null ? image_fullpath : "https://flutter.io/images/catalog-widget-placeholder.png"),
                                            fit: BoxFit.cover
                                        )
                                    )
                                ),
                                onTap: (){
                                  showModalBottomSheet<void>(context: context, builder: (BuildContext context) {
                                    return Container(
                                      child: Wrap(
                                        children: <Widget>[
                                          GestureDetector(
                                            behavior: HitTestBehavior.opaque,
                                            child: ListTile(
                                                leading: Icon(Icons.camera),
                                                title: Text('Camera'),
                                            ),
                                            onTap: () => getImageCamera()
                                          ),
                                          GestureDetector(
                                            behavior: HitTestBehavior.opaque,
                                            child: ListTile(
                                              leading: Icon(Icons.image),
                                              title: Text('Gallery'),
                                            ),
                                            onTap: () => getImageGallery(),
                                          ),
                                        ],
                                      ),
                                    );
                                  });
                                },
                              ),
                            ],
                          ),
                          SizedBox(height: 15),
                          Text("Ganti Foto", style: TextStyle(fontSize: 18.0, color: Colors.grey),),
                          SizedBox(height: 15),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ThemeColors.shadow,
                                  blurRadius: 6,
                                  spreadRadius: 4)
                            ]),
                        child: TextFormField(
                          enabled: !_isLoading,
                          validator: _validatorName,
                          onSaved: (value) => _name = value,
                          focusNode: _nameFocus,
                          controller: _nameController,
                          style: TextStyle(color: ThemeColors.textField),
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person),
                              hintText: AppLocalizations.of(context).tr('page_editprofile.form.hint_name'),
                              hintStyle:
                              TextStyle(color: ThemeColors.textField),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 16.0)),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ThemeColors.shadow,
                                  blurRadius: 6,
                                  spreadRadius: 4)
                            ]),
                        child: TextFormField(
                          enabled: !_isLoading,
                          validator: _validatorTelephone,
                          onSaved: (value) => _telephone = value,
                          focusNode: _telephoneFocus,
                          controller: _telephoneController,
                          style: TextStyle(color: ThemeColors.textField),
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.phone_iphone),
                              hintText: AppLocalizations.of(context).tr('page_editprofile.form.hint_phone'),
                              hintStyle:
                              TextStyle(color: ThemeColors.textField),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 16.0)),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ThemeColors.shadow,
                                  blurRadius: 6,
                                  spreadRadius: 4)
                            ]),
                        child: TextFormField(
                          enabled: !_isLoading,
                          validator: _validatorEmail,
                          onSaved: (value) => _email = value,
                          focusNode: _emailFocus,
                          controller: _emailController,
                          style: TextStyle(color: ThemeColors.textField),
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.email),
                              hintText: AppLocalizations.of(context).tr('page_editprofile.form.hint_email'),
                              hintStyle:
                              TextStyle(color: ThemeColors.textField),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 16.0)),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ThemeColors.shadow,
                                  blurRadius: 6,
                                  spreadRadius: 4)
                            ]),
                        child: TextFormField(
                          enabled: !_isLoading,
                          validator: _validatorAddress,
                          onSaved: (value) => _address = value,
                          focusNode: _addressFocus,
                          controller: _addressController,
                          keyboardType: TextInputType.multiline,
                          maxLines: 3,
                          style: TextStyle(
                              color: ThemeColors.textField,
                              fontFamily: 'RobotoMono'),
                          decoration: InputDecoration(
                              hintText: AppLocalizations.of(context).tr('page_editprofile.form.hint_address'),
                              hintStyle: TextStyle(
                                  color: ThemeColors.textField,
                                  fontFamily: 'RobotoMono'),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 16.0)),
                        ),
                      ),
                      Button(
                        onTap: () {
                          if (!_isLoading) {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              _editProfile(context);
                            } else {
                              setState(() => _autoValidate = true);
                            }
                          }
                        },text: AppLocalizations.of(context).tr('page_editprofile.form.update'),
                      ),
                    ],
                  ),
                ),
              ]
          ),
        )
    );
  }
}
