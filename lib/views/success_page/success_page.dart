import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';

class SuccessPage extends StatefulWidget {
  String message;
  String queue;
  String timecoming;

  SuccessPage({Key key,
    @required this.message,
    @required this.queue,
    @required this.timecoming}) : super(key: key);

  @override
  _SuccessPageState createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  String message;
  String queue;
  String timecoming;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: "Sukses Order",
          isBack: false,
        ),
        body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: Icon(Icons.message),
                  title: Text("Pesan"),
                  subtitle: Text(message != null ? message : "-"),
                ),
                ListTile(
                  leading: Icon(Icons.confirmation_number),
                  title: Text("Nomor Antrian"),
                  subtitle: Text(queue != null ? queue : "-"),
                ),
                ListTile(
                  leading: Icon(Icons.timer),
                  title: Text("Jam Kedatangan"),
                  subtitle: Text(timecoming != null ? timecoming : "-"),
                ),
                Button(
                  onTap: () {
                    Navigator.popUntil(context, ModalRoute.withName('/mainmenu'));
                  },text: "Kembali Ke Menu",
                ),
              ],
            )
        )
    );
  }

  @override
  void initState() {
    super.initState();
    message = widget.message;
    queue = widget.queue;
    timecoming = widget.timecoming;
  }
}
