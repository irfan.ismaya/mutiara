import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/views/commons/row_item_doctor_poli.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/utils/helpers.dart' as helpers;
import 'package:mvvm_mutiara/views/detail_bookingpoli_page/detail_bookingpoli_page.dart';
import 'package:mvvm_mutiara/views/choose_schedule_page/choose_schedule_page.dart';

final ServiceApi _serviceApi = ServiceApi();

class ListDocterPoliPage extends StatefulWidget {
  final int poli_id;
  final String date_time;
  final String title_poli;
  ListDocterPoliPage({Key key, @required this.poli_id, @required this.date_time , @required this.title_poli}) : super(key: key);

  @override
  _ListDocterPoliPageState createState() => _ListDocterPoliPageState();
}

class _ListDocterPoliPageState extends State<ListDocterPoliPage> {
  String api_token = "";
  int poli_id;
  String date_time;
  String title_poli;
  String datefilter;
  bool initData = false;
  SharedPreferences sharedPreferences;

  void goToChooseSchedule(DataDokter data, String current_date){
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ChooseSchedulePage(
            dokter_id: data.dokterId,
            dokter_name: data.dokterName,
            dokter_poli: data.dokterPoli,
            dokter_image: data.dokterImage,
            current_date: current_date,
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: "Pilih Dokter",
          isBack: true,
        ),
        backgroundColor: ThemeColors.background,
        body: Container(
          child: FutureBuilder(
              future: _serviceApi.listDoctorByPoli(poli_id, api_token),
              // ignore: missing_return
              builder: (context, snapshot){
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.active:
                  case ConnectionState.waiting:
                    return Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 1,
                        )
                    );
                    break;
                  case ConnectionState.done:
                    if (snapshot.hasData) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Image(
                                  height: 180.0,
                                  fit: BoxFit.fill,
                                  image: helpers.strBannerPoli(poli_id)
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 30, top: 30),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text((snapshot.data.data.length).toString()+" "+'Dokter yang tersedia', style: TextStyle(fontSize: 16, fontWeight:FontWeight.bold),),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 30, top: 5, right: 30),
                                child: Align(
                                  alignment: Alignment.topLeft,
                                  child: Text('Hai, Dokter yang kamu cari bersedia membantu ada hari ini.', style: TextStyle(fontSize: 14)),
                                ),
                              )
                            ],
                          ),
                          Expanded(
                            child: ListView.builder(
                                itemCount: snapshot.data.data == null ? 0 : snapshot.data.data.length,
                                primary: false,
                                itemBuilder: (BuildContext context, int i){
                                  return Container(
                                    padding: EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
                                    child: RowItemDoctorPoli(
                                      name: snapshot.data.data[i].dokterName,
                                      poli: snapshot.data.data[i].dokterPoli,
                                      info: true,
                                      onTap: (){
                                        DataDokter data = DataDokter(
                                          dokterId: snapshot.data.data[i].dokterId,
                                          dokterName: snapshot.data.data[i].dokterName,
                                          dokterPoli: snapshot.data.data[i].dokterPoli,
                                          dokterImage: snapshot.data.data[i].dokterImage,
                                        );
                                        goToChooseSchedule(data, date_time);
                                      },
                                    ),
                                  );
                                }
                            ),
                          )
                        ],
                      );
                    }else if(snapshot.data == null) {
                      return Center(
                          child: Text("No Data")
                      );
                    }else if(snapshot.hasError) {
                      return Center(
                          child: Text("No Internet Connection")
                      );
                    }
                    break;
                }
              }
          ),
        ),
    );
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      api_token = sharedPreferences.getString("api_token");
    });
  }

  @override
  void initState() {
    super.initState();
    getCredential();
    poli_id = widget.poli_id;
    date_time = widget.date_time;
  }

}

