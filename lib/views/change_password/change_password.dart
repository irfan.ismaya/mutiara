import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/viewmodels/user_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:shared_preferences/shared_preferences.dart';

final UserViewModel viewModelUser = UserViewModel(api: ServiceApi());
enum ConfirmAction { CANCEL, ACCEPT }

class ChangePasswordPage extends StatefulWidget {
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool checkValue = false;
  bool _isLoading = false;
  bool _obscureTextOldPass = true;
  bool _obscureTextPass = true;
  bool _obscureTextConfirmPass = true;
  SharedPreferences sharedPreferences;
  ResponseMessage responseMessage;

  String _oldpassword;
  String _password;
  String _confirmpassword;

  final FocusNode _oldpasswordFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirmpasswordFocus = FocusNode();

  ResponseMessage data;
  TextEditingController _oldpasswordController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _confirmpassController = new TextEditingController();


  String _validatorPass(value) {
    String patttern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_register.validation.field_pass_empty');
    }else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_register.validation.field_pass_regex');
    }
    return null;
  }

  String _validatorPassword(value){
    return _validatorPass(value);
  }

  void _toggleOldPass() {
    setState(() {
      _obscureTextOldPass = !_obscureTextOldPass;
    });
  }

  void _togglePass() {
    setState(() {
      _obscureTextPass = !_obscureTextPass;
    });
  }

  void _toggleConfirmPass() {
    setState(() {
      _obscureTextConfirmPass = !_obscureTextConfirmPass;
    });
  }

  Future<String> _doChangePassword(BuildContext context) async{
    setState(() {
      _isLoading = true;
    });
    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("api_token");
    ChangePasswordRequest changePasswordRequest = ChangePasswordRequest(
        old_password: _oldpasswordController.text,
        password: _passwordController.text,
        password_confirmation: _confirmpassController.text);
    bool req = await viewModelUser.changePassword(changePasswordRequest, token);
    responseMessage = await viewModelUser.changepassword;
    if(req){
      if(responseMessage.code == 200 || responseMessage.code == 201){
        setState(() {
          _isLoading = false;
        });
        HelpersView.toast(responseMessage.message, Colors.green);
        Navigator.pop(context);
      }else{
        HelpersView.toast(responseMessage.message, Colors.orange);
        setState(() {
          _isLoading = false;
        });
      }
    }else{
      HelpersView.toast(responseMessage.message, ThemeColors.statePending);
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: AppLocalizations.of(context).tr('page_change_pass.title'),
          isBack: true,
        ),
        backgroundColor: ThemeColors.background,
        body: SingleChildScrollView(
          padding: const EdgeInsets.only(top: 25),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Visibility(
                  visible: _isLoading,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: CircularProgressIndicator(strokeWidth: 1,),
                  ),
                ),
                Form(
                  key: _formKey,
                  autovalidate: _autoValidate,
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ThemeColors.shadow,
                                  blurRadius: 6,
                                  spreadRadius: 4)
                            ]),
                        child: TextFormField(
                          enabled: !_isLoading,
                          onSaved: (value) => _password = value,
                          focusNode: _oldpasswordFocus,
                          controller: _oldpasswordController,
                          obscureText: _obscureTextOldPass,
                          style: TextStyle(
                              color: ThemeColors.textField,
                              fontFamily: 'RobotoMono'),
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.lock),
                              suffixIcon: GestureDetector(
                                child: _obscureTextOldPass
                                    ? const Icon(Icons.visibility_off)
                                    : const Icon(Icons.visibility),
                                onTap: _toggleOldPass,
                              ),
                              hintText: AppLocalizations.of(context).tr('page_change_pass.form.hint_oldpass'),
                              hintStyle: TextStyle(
                                  color: ThemeColors.textField,
                                  fontFamily: 'RobotoMono'),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 16.0)),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ThemeColors.shadow,
                                  blurRadius: 6,
                                  spreadRadius: 4)
                            ]),
                        child: TextFormField(
                          enabled: !_isLoading,
                          validator: _validatorPassword,
                          onSaved: (value) => _password = value,
                          focusNode: _passwordFocus,
                          controller: _passwordController,
                          obscureText: _obscureTextPass,
                          style: TextStyle(
                              color: ThemeColors.textField,
                              fontFamily: 'RobotoMono'),
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.lock),
                              suffixIcon: GestureDetector(
                                child: _obscureTextPass
                                    ? const Icon(Icons.visibility_off)
                                    : const Icon(Icons.visibility),
                                onTap: _togglePass,
                              ),
                              hintText: AppLocalizations.of(context).tr('page_change_pass.form.hint_pass'),
                              hintStyle: TextStyle(
                                  color: ThemeColors.textField,
                                  fontFamily: 'RobotoMono'),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 16.0)),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ThemeColors.shadow,
                                  blurRadius: 6,
                                  spreadRadius: 4)
                            ]),
                        child: TextFormField(
                          enabled: !_isLoading,
                          validator: _validatorPassword,
                          onSaved: (value) => _confirmpassword = value,
                          focusNode: _confirmpasswordFocus,
                          controller: _confirmpassController,
                          obscureText: _obscureTextConfirmPass,
                          style: TextStyle(
                              color: ThemeColors.textField,
                              fontFamily: 'RobotoMono'),
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.lock),
                              suffixIcon: GestureDetector(
                                child: _obscureTextConfirmPass
                                    ? const Icon(Icons.visibility_off)
                                    : const Icon(Icons.visibility),
                                onTap: _toggleConfirmPass,
                              ),
                              hintText: AppLocalizations.of(context).tr('page_change_pass.form.hint_confirmpass'),
                              hintStyle: TextStyle(
                                  color: ThemeColors.textField,
                                  fontFamily: 'RobotoMono'),
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 16.0)),
                        ),
                      ),
                      Button(
                        onTap: () {
                          if (!_isLoading) {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              _doChangePassword(context);
                            } else {
                              setState(() => _autoValidate = true);
                            }
                          }
                        },text: AppLocalizations.of(context).tr('page_change_pass.form.changepass'),
                      ),
                    ],
                  ),
                ),
              ]
          ),
        )
    );
  }
}
