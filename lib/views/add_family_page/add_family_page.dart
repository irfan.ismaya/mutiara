import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/viewmodels/family_viewmodel.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:easy_localization/easy_localization.dart';

final FamilyViewModel viewModelFamily = FamilyViewModel(api: ServiceApi());
enum ConfirmAction { CANCEL, ACCEPT }

class AddFamilyPage extends StatefulWidget {
  @override
  _AddFamilyPageState createState() => _AddFamilyPageState();
}

class _AddFamilyPageState extends State<AddFamilyPage> {
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool checkValue = false;
  bool _isLoading = false;
  SharedPreferences sharedPreferences;
  ResponseMessage responseMessage;

  String _statuskeluarga;
  String _name;
  String _tanggallahir;
  String _telephone;
  String _email;
  String _address;

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _telephoneFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _statusFocus = FocusNode();
  final FocusNode _birthFocus = FocusNode();
  final FocusNode _addressFocus = FocusNode();

  TextEditingController _nameController = new TextEditingController();
  TextEditingController _telephoneController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _birthController = new TextEditingController();
  TextEditingController _addressController = new TextEditingController();

  String _validatorTelephone(value) {
    String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_add_family.validation.field_phone_empty');
    }else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_add_family.validation.field_phone_regex');
    }
    return null;
  }

  String _validatorPass(value) {
    String patttern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_add_family.validation.field_birthday_empty');
    }else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_add_family.validation.field_birthday_regex');
    }
    return null;
  }

  String _validatorName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_add_family.validation.field_name_empty');
    } else if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).tr('page_add_family.validation.field_name_regex');
    }
    return null;
  }

  String _validatorEmail(String value) {
    String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0 || value.isEmpty) {
      return AppLocalizations.of(context).tr('page_add_family.validation.field_email_empty');
    } else if(!regExp.hasMatch(value)){
      return AppLocalizations.of(context).tr('page_add_family.validation.field_email_regex');
    }else {
      return null;
    }
  }

  String _validatorAddress(value) {
    if (value.isEmpty) {
      return AppLocalizations.of(context).tr('page_add_family.validation.field_address_empty');
    }
    if (value.length < 10) {
      return AppLocalizations.of(context).tr('page_add_family.validation.field_address_regex');
    }
    return null;
  }

  String _validatorPassword(value){
    return _validatorPass(value);
  }

  List<String> _fams = <String>['', 'SUAMI', 'ISTERI', 'ANAK', 'SINGLE'];
  String _fam = '';

  Future<String> _addFamily(BuildContext context) async{
    setState(() {
      _isLoading = true;
    });
    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("api_token");
    AddFamilyRequest bookingRequest = AddFamilyRequest(
      status_dalam_keluarga: _fam,
      namalengkap: _nameController.text,
      email: _emailController.text,
      tanggal_lahir: _birthController.text,
      notelpon: _telephoneController.text,
      alamat: _addressController.text
    );
    bool req = await viewModelFamily.addFamily(bookingRequest, token);
    responseMessage = await viewModelFamily.addfamily;
    if(req){
      if(responseMessage.code == 200 || responseMessage.code == 201){
        setState(() {
          _isLoading = false;
        });
        HelpersView.toast(responseMessage.message, Colors.green);
        Navigator.pop(context);
      }else{
        HelpersView.toast(responseMessage.message, Colors.orange);
        setState(() {
          _isLoading = false;
        });
      }
    }else{
      HelpersView.toast(responseMessage.message, ThemeColors.statePending);
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: AppLocalizations.of(context).tr('page_add_family.title'),
          isBack: true,
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.only(top: 25),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Visibility(
                    visible: _isLoading,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: CircularProgressIndicator(strokeWidth: 1,),
                    ),
                  ),
                  Form(
                    key: _formKey,
                    autovalidate: _autoValidate,
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 30),
                          child: InputDecorator(
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.assignment_ind),
                                hintText: AppLocalizations.of(context).tr('page_add_family.form.hint_status'),
                                hintStyle: TextStyle(
                                    color: ThemeColors.textField,
                                    fontFamily: 'RobotoMono'),
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 16.0)),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                value: _fam,
                                isDense: true,
                                onChanged: (String newValue) {
                                  setState(() {
                                    _fam = newValue;
                                  });
                                },
                                items: _fams.map((String value) {
                                  return DropdownMenuItem(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 30),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: ThemeColors.shadow,
                                    blurRadius: 6,
                                    spreadRadius: 4)
                              ]),
                          child: TextFormField(
                            enabled: !_isLoading,
                            validator: _validatorName,
                            onSaved: (value) => _name = value,
                            focusNode: _nameFocus,
                            controller: _nameController,
                            keyboardType: TextInputType.text,
                            style: TextStyle(color: ThemeColors.textField),
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person),
                                hintText: AppLocalizations.of(context).tr('page_add_family.form.hint_name'),
                                hintStyle:
                                TextStyle(color: ThemeColors.textField),
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 16.0)),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 30),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: ThemeColors.shadow,
                                    blurRadius: 6,
                                    spreadRadius: 4)
                              ]),
                          child: TextFormField(
                            enabled: !_isLoading,
                            validator: _validatorEmail,
                            onSaved: (value) => _email = value,
                            focusNode: _emailFocus,
                            controller: _emailController,
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(color: ThemeColors.textField),
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.email),
                                hintText: AppLocalizations.of(context).tr('page_add_family.form.hint_email'),
                                hintStyle:
                                TextStyle(color: ThemeColors.textField),
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 16.0)),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 30),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: ThemeColors.shadow,
                                    blurRadius: 6,
                                    spreadRadius: 4)
                              ]),
                          child: TextFormField(
                            enabled: !_isLoading,
                            validator: _validatorTelephone,
                            onSaved: (value) => _telephone = value,
                            focusNode: _telephoneFocus,
                            controller: _telephoneController,
                            keyboardType: TextInputType.phone,
                            style: TextStyle(color: ThemeColors.textField),
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.phone_iphone),
                                hintText: AppLocalizations.of(context).tr('page_add_family.form.hint_phone'),
                                hintStyle:
                                TextStyle(color: ThemeColors.textField),
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 16.0)),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 30),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: ThemeColors.shadow,
                                    blurRadius: 6,
                                    spreadRadius: 4)
                              ]),
                          child: TextFormField(
                            enabled: !_isLoading,
                            validator: _validatorPassword,
                            onSaved: (value) => _statuskeluarga = value,
                            focusNode: _birthFocus,
                            controller: _birthController,
                            keyboardType: TextInputType.datetime,
                            style: TextStyle(
                                color: ThemeColors.textField,
                                fontFamily: 'RobotoMono'),
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.calendar_today),
                                hintText: AppLocalizations.of(context).tr('page_add_family.form.hint_birth'),
                                hintStyle: TextStyle(
                                    color: ThemeColors.textField,
                                    fontFamily: 'RobotoMono'),
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 16.0)),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 30),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: ThemeColors.shadow,
                                    blurRadius: 6,
                                    spreadRadius: 4)
                              ]),
                          child: TextFormField(
                            enabled: !_isLoading,
                            validator: _validatorAddress,
                            onSaved: (value) => _address = value,
                            focusNode: _addressFocus,
                            controller: _addressController,
                            keyboardType: TextInputType.multiline,
                            maxLines: 3,
                            style: TextStyle(
                                color: ThemeColors.textField,
                                fontFamily: 'RobotoMono'),
                            decoration: InputDecoration(
                                hintText: AppLocalizations.of(context).tr('page_add_family.form.hint_address'),
                                hintStyle: TextStyle(
                                    color: ThemeColors.textField,
                                    fontFamily: 'RobotoMono'),
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5.0)),
                                ),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 16.0)),
                          ),
                        ),
                        Button(
                          onTap: () {
                            if(_fam != null){
                              _addFamily(context);
                            }else{
                              HelpersView.toast("Silahkan Pilih Status Anggota Keluarga", ThemeColors.statePending);
                            }
                          },text: AppLocalizations.of(context).tr('page_add_family.form.register'),
                        ),
                      ],
                    ),
                  ),
                ]
            )
        )
    );
  }
}
