import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart' show CalendarCarousel;
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:mvvm_mutiara/views/commons/row_item_doctor_qouta.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:mvvm_mutiara/viewmodels/family_viewmodel.dart';
import 'package:mvvm_mutiara/viewmodels/polyclinic_viewmodel.dart';
import 'package:mvvm_mutiara/views/success_page/success_page.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:shared_preferences/shared_preferences.dart';

final FamilyViewModel viewModelFamily = FamilyViewModel(api: ServiceApi());
final PolyClinicViewModel viewModelPoli = PolyClinicViewModel(api: ServiceApi());
enum ConfirmAction { CANCEL, ACCEPT }

class ScheduleDoctorPage extends StatefulWidget {
  int jadwalId;
  String namaDokter;
  String jadwalStart;
  String jadwalEnd;
  String waktupraktek;
  String hari;
  String datefilter;
  String poli_name;

  ScheduleDoctorPage({Key key,
    @required this.jadwalId,
    @required this.namaDokter,
    @required this.jadwalStart,
    @required this.jadwalEnd,
    @required this.waktupraktek,
    @required this.hari,
    @required this.datefilter,
    @required this.poli_name}) : super(key: key);

  @override
  _ScheduleDoctorState createState() => _ScheduleDoctorState();
}

class _ScheduleDoctorState extends State<ScheduleDoctorPage> {
  DateTime _currentDate;
  int jadwalId;
  int kodebooking;
  String namaDokter;
  String jadwalStart;
  String jadwalEnd;
  String waktupraktek;
  String hari;
  String datefilter;
  String poli_name;
  bool initDate = false;

  SharedPreferences sharedPreferences;
  ResponseFamily responseFamily;
  ResponseBooking responseBooking;
  ResponseConfirmQueue responseConfirmQueue;
  bool _isLoading = false;
  String _kodeKeluarga;
  List data = List();

  Future<String> _getListFamily() async{
    sharedPreferences = await SharedPreferences.getInstance();
    try{
      String token = sharedPreferences.getString("api_token");
      bool req = await viewModelFamily.getFamily(token);
      responseFamily = await viewModelFamily.family;
      if(req){
        if(responseFamily.code == 200){
          setState(() {
            data = responseFamily.data;
          });
        }else{
          HelpersView.toast(responseFamily.message, Colors.orange);
        }
      }else{
        HelpersView.toast(responseFamily.message, ThemeColors.statePending);
      }
    }catch(ex){
      HelpersView.toast(responseFamily.message, ThemeColors.statePending);
    }finally{

    }
    return 'Success';
  }

  Future<String> _bookingOrder(BuildContext context) async{
    setState(() {
      _isLoading = true;
    });
    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("api_token");
    BookingRequest bookingRequest = BookingRequest( jadwalId: jadwalId, bookingDate: initDate ? _currentDate.toString().substring(0,10) : datefilter, kodeKeluarga: int.parse(_kodeKeluarga));
    bool req = await viewModelPoli.bookingPoly(bookingRequest, token);
    responseBooking = await viewModelPoli.bookingpoly;
    if(req){
      if(responseBooking.code == 200 || responseBooking.code == 201){
        setState(() {
          _isLoading = false;
          kodebooking = responseBooking.data.kodebooking;
        });

        _queueConfirm(context, kodebooking);
      }else{
        HelpersView.toast(responseBooking.message, Colors.orange);
        setState(() {
          _isLoading = false;
        });
      }
    }else{
      HelpersView.toast(responseBooking.message, ThemeColors.statePending);
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  Future<String> _queueConfirm(BuildContext context, int kodebooking) async{
    setState(() {
      _isLoading = true;
    });
    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("api_token");
    bool req = await viewModelPoli.getQueueConfirm(token, kodebooking);
    responseConfirmQueue = await viewModelPoli.getqueueconfirm;
    if(req){
      if(responseConfirmQueue.code == 200 || responseConfirmQueue.code == 201){
        setState(() {
          _isLoading = false;
        });
        goToSuccessPage(context, responseConfirmQueue.message.toString(), responseConfirmQueue.data.noAntrian.toString(), responseConfirmQueue.data.jamKedatangan.toString());
      }else{
        HelpersView.toast(responseConfirmQueue.message, Colors.orange);
        setState(() {
          _isLoading = false;
        });
      }
    }else{
      HelpersView.toast(responseConfirmQueue.message, ThemeColors.statePending);
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  Future<ConfirmAction> _messageConfirmDialog(BuildContext context, String message) async {
    return showDialog<ConfirmAction>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Order Berhasil'),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Ya'),
              onPressed: () {
                Navigator.popUntil(context, ModalRoute.withName('/mainmenu'));
              },
            )
          ],
        );
      },
    );
  }

  void goToSuccessPage(BuildContext context, String message, String queue, String timecoming){
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SuccessPage(
            message: message,
            queue: queue,
            timecoming: timecoming,
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarSmall(
        context: context,
        title: 'Jadwal Dokter',
        isBack: true,
      ),
      backgroundColor: Colors.blue[50].withOpacity(0.9),
      body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                    margin: EdgeInsets.only(left: 15, right: 15, top: 15),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        gradient:LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [Colors.white, Colors.white]
                        )
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin:EdgeInsets.only(left: 15, top: 15, bottom: 10),
                          child: Text("Detail Pesanan Anda",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Colors.grey[600],
                                fontSize: 20,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                        Container(
                          margin:EdgeInsets.only(left: 15, top: 5, right: 15),
                          child: Divider(),
                        ),
                        SizedBox(height: 20),
                        Container(
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Row(
                            children: <Widget>[
                              Text("Klinik"),
                              Expanded(child: Container()),
                              Text(poli_name, style: TextStyle(
                                  fontWeight: FontWeight.w600
                              ),),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 25, right: 25),
                          child: Divider(),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Row(
                            children: <Widget>[
                              Text("Dokter"),
                              Expanded(child: Container()),
                              Text(widget.namaDokter, style: TextStyle(
                                  fontWeight: FontWeight.w600
                              )),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 25, right: 25),
                          child: Divider(),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Row(
                            children: <Widget>[
                              Text("Tanggal"),
                              Expanded(child: Container()),
                              Text(datefilter, style: TextStyle(
                                  fontWeight: FontWeight.w600
                              )),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 25, right: 25),
                          child: Divider(),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Row(
                            children: <Widget>[
                              Text("Jam"),
                              Expanded(child: Container()),
                              Text(widget.jadwalStart+"-"+widget.jadwalEnd, style: TextStyle(
                                  fontWeight: FontWeight.w600
                              )),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 25, right: 25),
                          child: Divider(),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Row(
                            children: <Widget>[
                              Text("Anggota Keluarga"),
                              Expanded(child: Container()),
                              DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  items: data.map((item) {
                                    return DropdownMenuItem(
                                      child: Text(item.namalengkap),
                                      value: item.kodeKeluarga,
                                    );
                                  }).toList(),
                                  onChanged: (newVal) {
                                    setState(() {
                                      _kodeKeluarga = newVal;
                                    });
                                  },
                                  value: _kodeKeluarga,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 25),
                      ],
                    )
              ),
              Container(
                margin: EdgeInsets.only(top:15, left: 15, right: 15),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    gradient:LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [Colors.white, Colors.white]
                    )
                ),
                child: CalendarCarousel<Event>(
                  onDayPressed: (DateTime date, List<Event> events) {
                    this.setState(() => _currentDate = date);
                    setState(() {
                      initDate = true;
                      _currentDate = date;
                    });
                    print(_currentDate.toString().substring(0,10));
                  },
                  weekendTextStyle: TextStyle(
                    color: Colors.red,
                  ),
                  weekFormat: false,
                  height: 380.0,
                  selectedDateTime: _currentDate,
                  daysHaveCircularBorder: true,
                ),
              ),
              Button(
                onTap: () {
                  if(_kodeKeluarga != null){
                    _bookingOrder(context);
                  }else{
                    HelpersView.toast("Silahkan Pilih Anggota Keluarga", ThemeColors.statePending);
                  }
                },text: "Order",
              ),
              Visibility(
                visible: _isLoading,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: CircularProgressIndicator(strokeWidth: 1,),
                ),
              )
            ],
          ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _getListFamily();
    jadwalId = widget.jadwalId;
    namaDokter = widget.namaDokter;
    jadwalStart = widget.jadwalStart;
    jadwalEnd = widget.jadwalEnd;
    waktupraktek = widget.waktupraktek;
    hari = widget.hari;
    datefilter = widget.datefilter;
    poli_name = widget.poli_name;
  }
}

