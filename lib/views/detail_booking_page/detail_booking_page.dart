import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/views/commons/row_item_doctor_qouta.dart';

class DetailBookingPage extends StatefulWidget {
  @override
  _DetailBookingPageState createState() => _DetailBookingPageState();
}

class _DetailBookingPageState extends State<DetailBookingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarSmall(
        context: context,
        title: 'Informasi Booking',
        isBack: true,
      ),
      backgroundColor: Colors.blue[50].withOpacity(0.9),
      body: SingleChildScrollView(
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(15),
              child: RowItemDoctorQouta(
                name: "Anak",
                icon: "images/icon_poli/poli_anak.png",
                image: "images/icon_poli/poli_anak.png",
              ),
            ),
            SizedBox(height: 10),
            Container(
              margin:EdgeInsets.only(left: 15, top: 15),
              child: Text("Detail pesanan anda",
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 20,
                    fontWeight: FontWeight.w500
                ),
              ),
            ),
            Container(
              margin:EdgeInsets.only(left: 15, top: 5),
              child: Text("Hai, Dokter yang anda booking bersedia pada:",
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 15,
                ),
              ),
            ),
            SizedBox(height: 5),
            Container(
              margin:EdgeInsets.only(left: 15, top: 10, right: 15),
              child: Divider(),
            ),
          ]
        )
      )
    );
  }
}
