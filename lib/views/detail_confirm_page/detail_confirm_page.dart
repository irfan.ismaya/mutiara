import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:mvvm_mutiara/viewmodels/family_viewmodel.dart';
import 'package:mvvm_mutiara/viewmodels/polyclinic_viewmodel.dart';
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/views/commons/button.dart';
import 'package:shared_preferences/shared_preferences.dart';

final FamilyViewModel viewModelFamily = FamilyViewModel(api: ServiceApi());
final PolyClinicViewModel viewModelPoli = PolyClinicViewModel(api: ServiceApi());
enum ConfirmAction { CANCEL, ACCEPT }

class DetailConfirmPage extends StatefulWidget {
  int bookingId;
  int kodebooking;
  String bookingDate;
  String poliklinikName;
  String dokterName;
  String jampraktekAwal;
  String jampraktekAkhir;
  String hari;

  DetailConfirmPage({Key key,
    @required this.bookingId,
    @required this.kodebooking,
    @required this.bookingDate,
    @required this.dokterName,
    @required this.poliklinikName,
    @required this.jampraktekAwal,
    @required this.hari,
    @required this.jampraktekAkhir}) : super(key: key);

  @override
  _DetailConfirmPageState createState() => _DetailConfirmPageState();
}

class _DetailConfirmPageState extends State<DetailConfirmPage> {
  int bookingId;
  int kodebooking;
  String bookingDate;
  String poliklinikName;
  String dokterName;
  String jampraktekAwal;
  String jampraktekAkhir;
  String hari;

  SharedPreferences sharedPreferences;
  ResponseFamily responseFamily;
  ResponseMessage responseBooking;
  bool _isLoading = false;

  Future<String> _bookingOrder(BuildContext context, int status) async{
    setState(() {
      _isLoading = true;
    });
    sharedPreferences = await SharedPreferences.getInstance();
    String token = sharedPreferences.getString("api_token");
    BookingConfirmationComeRequest bookingRequest = BookingConfirmationComeRequest( bookingId: bookingId, bookingStatus: status);
    bool req = await viewModelPoli.bookingConfirmationCome(bookingRequest, token);
    responseBooking = await viewModelPoli.bookingconfirmationcome;
    if(req){
      if(responseBooking.code == 200 || responseBooking.code == 201){
        setState(() {
          _isLoading = false;
        });
        HelpersView.toast(responseBooking.message, Colors.green);
        Navigator.pop(context);
      }else{
        HelpersView.toast(responseBooking.message, Colors.orange);
        setState(() {
          _isLoading = false;
        });
      }
    }else{
      HelpersView.toast(responseBooking.message, ThemeColors.statePending);
      setState(() {
        _isLoading = false;
      });
    }
    return 'Success';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: "Konfirmasi Kedatangan",
          isBack: true,
        ),
        backgroundColor: ThemeColors.background,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 15, top: 10, bottom: 10),
                child: Text("Detail Antrian Anda",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Colors.grey[600],
                      fontSize: 18,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Text("Klinik"),
                    Expanded(child: Container()),
                    Text(poliklinikName, style: TextStyle(
                      fontWeight: FontWeight.w600
                    ),),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 25, right: 25),
                child: Divider(),
              ),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Text("Dokter"),
                    Expanded(child: Container()),
                    Text(widget.dokterName, style: TextStyle(
                        fontWeight: FontWeight.w600
                    )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 25, right: 25),
                child: Divider(),
              ),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Text("Tanggal"),
                    Expanded(child: Container()),
                    Text(bookingDate, style: TextStyle(
                        fontWeight: FontWeight.w600
                    )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 25, right: 25),
                child: Divider(),
              ),
              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Text("Jam"),
                    Expanded(child: Container()),
                    Text(widget.jampraktekAwal+"-"+widget.jampraktekAkhir, style: TextStyle(
                        fontWeight: FontWeight.w600
                    )),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 25, right: 25),
                child: Divider(),
              ),

              Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Center(
                  child: Text("Apakah anda akan hadir pada jam tersebut?", textAlign: TextAlign.center, style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                  )),
                ),
              ),
              Button(
                onTap: () {
                  _bookingOrder(context, 2);
                },text: "Ya",
              ),
              ButtonConfirm(
                onTap: () {
                  _bookingOrder(context, -1);
                },text: "Batal",
              ),
              Visibility(
                visible: _isLoading,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: CircularProgressIndicator(strokeWidth: 1,),
                ),
              )
            ]
          )
        ),
    );
  }

  @override
  void initState() {
    super.initState();
    bookingId = widget.bookingId;
    kodebooking = widget.bookingId;
    bookingDate = widget.bookingDate;
    poliklinikName = widget.poliklinikName;
    dokterName = widget.dokterName;
    jampraktekAwal = widget.jampraktekAwal;
    jampraktekAkhir = widget.jampraktekAkhir;
    hari = widget.hari;
  }
}
