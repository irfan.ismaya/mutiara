import 'package:flutter/material.dart';
import 'package:mvvm_mutiara/styles/theme.dart';
import 'package:mvvm_mutiara/views/commons/app_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mvvm_mutiara/views/commons/row_item_doctor_poli.dart';
import 'package:mvvm_mutiara/viewmodels/family_viewmodel.dart';
import 'package:mvvm_mutiara/utils/helpers_view.dart' as HelpersView;
import 'package:mvvm_mutiara/services/service_api.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart' show CalendarCarousel;
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/views/detail_bookingpoli_page/detail_bookingpoli_page.dart';

final FamilyViewModel viewModelFamily = FamilyViewModel(api: ServiceApi());
final ServiceApi _serviceApi = ServiceApi();

class ChooseSchedulePage extends StatefulWidget {
  int dokter_id;
  String dokter_image;
  String dokter_name;
  String dokter_poli;
  String current_date;

  ChooseSchedulePage({Key key,
    @required this.dokter_id,
    @required this.dokter_image,
    @required this.dokter_name,
    @required this.dokter_poli,
    @required this.current_date}) : super(key: key);
  @override
  _ChooseSchedulePageState createState() => _ChooseSchedulePageState();
}

class _ChooseSchedulePageState extends State<ChooseSchedulePage> {
  DateTime _currentDate;
  String api_token = "";
  int dokter_id;
  int status_quota;
  String dokter_image;
  String dokter_name;
  String dokter_poli;
  String current_date;
  String datefilter;
  ResponseFamily responseFamily;

  bool initData = false;
  bool initButton = false;
  SharedPreferences sharedPreferences;

  String _kodeKeluarga;
  String _namaKeluarga;
  List data = List();

  Future<String> _getListFamily() async{
    sharedPreferences = await SharedPreferences.getInstance();
    try{
      String token = sharedPreferences.getString("api_token");
      bool req = await viewModelFamily.getFamily(token);
      responseFamily = await viewModelFamily.family;
      if(req){
        if(responseFamily.code == 200){
          setState(() {
            data = responseFamily.data;
          });
        }else{
          HelpersView.toast(responseFamily.message, Colors.orange);
        }
      }else{
        HelpersView.toast(responseFamily.message, ThemeColors.statePending);
      }
    }catch(ex){
      HelpersView.toast(responseFamily.message, ThemeColors.statePending);
    }finally{

    }
    return 'Success';
  }

  void goToDetailBookingPoli(DataPolySchedule data, String datefilter, String poli_name, String namaKeluarga, String kodeKeluarga){
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetailBookingPoliPage(
            jadwalId: data.jadwalId,
            namaDokter: data.namaDokter,
            jadwalStart: data.jadwalStart,
            jadwalEnd: data.jadwalEnd,
            hari: data.hari,
            quota: data.quota,
            statusQuota: data.statusQuota,
            datefilter: datefilter,
            poli_name: poli_name,
            kodeKeluarga: kodeKeluarga,
            namaKeluarga: namaKeluarga,
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    DoctorSheduleRequest doctor = DoctorSheduleRequest(dokterId: dokter_id, bookingDate: initData ? _currentDate.toString().substring(0,10): current_date);
    DoctorSheduleRequest doctorFilter = DoctorSheduleRequest(dokterId: dokter_id, bookingDate: initData ? _currentDate.toString().substring(0,10): current_date);
    return Scaffold(
        appBar: AppBarSmall(
          context: context,
          title: "Pilih Jadwal",
          isBack: true,
        ),
      body:SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 15, top: 15, right: 15),
                child: RowItemDoctorPoli(
                    name: dokter_name,
                    poli: dokter_poli,
                    info: false,
                    onTap: null
                )
            ),
            Container(
              margin: EdgeInsets.only(left: 25, bottom: 5),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text("Hari yang tersedia", style: TextStyle(fontSize: 16, fontWeight:FontWeight.w500, color: Colors.black45),),
              ),
            ),
            Container(
                child: FutureBuilder(
                    future : _serviceApi.scheduleDoctor(dokter_id, api_token),
                    // ignore: missing_return
                    builder: (context, snapshot){
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          break;
                        case ConnectionState.active:
                          break;
                        case ConnectionState.waiting:
                          return Center(
                              child: CircularProgressIndicator(
                                strokeWidth: 1,
                              )
                          );
                          break;
                        case ConnectionState.done:
                          if (snapshot.hasData) {
                            return GridView.builder(
                                itemCount: snapshot.data.data == null ? 0 : snapshot.data.data.length,
                                primary: false,
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                padding: EdgeInsets.symmetric(horizontal: 12),
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  childAspectRatio: MediaQuery.of(context).size.width /
                                      (MediaQuery.of(context).size.height / 5),
                                ),
                                itemBuilder: (BuildContext context, int i){
                                  return Container(
                                    margin: EdgeInsets.all(10),
                                    decoration: new BoxDecoration(
                                      color: Colors.purple,
                                      borderRadius: new BorderRadius.circular(10.0),
                                      gradient: new LinearGradient(
                                        colors: [Colors.blue, Colors.cyan],
                                      ),
                                    ),
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text(snapshot.data.data[i].hari.toString(),
                                        style: TextStyle(
                                            color: Colors.white
                                        ),
                                      ),
                                    ),
                                  );
                                }
                            );
                          }else if(snapshot.data == null) {
                            print(snapshot.data);
                            return Center(
                                child: Text("No Data")
                            );
                          }
                          break;
                      }
                    }
                )
            ),
            Visibility(
              visible: false,
              child: Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Text("Anggota Keluarga"),
                    Expanded(child: Container()),
                    DropdownButtonHideUnderline(
                      child: DropdownButton(
                        items: data.map((item) {
                          return DropdownMenuItem(
                            child: Text(item.namalengkap),
                            value: item.kodeKeluarga+"-"+item.namalengkap,
                          );
                        }).toList(),
                        onChanged: (newVal) {
                          setState(() {
                            _kodeKeluarga = newVal;
                            _namaKeluarga = newVal;
                          });
                        },
                        value: _kodeKeluarga,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top:15, left: 15, right: 15),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  gradient:LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [Colors.white, Colors.white]
                  )
              ),
              child: CalendarCarousel<Event>(
                onDayPressed: (DateTime date, List<Event> events) {
                  this.setState(() => _currentDate = date);
                  setState(() {
                    initData = true;
                    _currentDate = date;
                    datefilter = date.toString().substring(0,10);
                  });

                  DoctorSheduleRequest doctorFilter = DoctorSheduleRequest(dokterId: dokter_id, bookingDate: initData ? _currentDate.toString().substring(0,10): current_date);
                  _serviceApi.listDoctorSchedule(doctorFilter, api_token);

                  print(_currentDate.toString().substring(0,10));
                },
                weekendTextStyle: TextStyle(
                  color: Colors.red,
                ),
                weekFormat: false,
                height: 380.0,
                selectedDateTime: _currentDate,
                daysHaveCircularBorder: true,
              ),
            ),
            Container(
              child: FutureBuilder(
                  future: initData ? _serviceApi.listDoctorSchedule(doctorFilter, api_token) : _serviceApi.listDoctorSchedule(doctor, api_token),
                  // ignore: missing_return
                  builder: (context, snapshot){
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                      case ConnectionState.active:
                      case ConnectionState.waiting:
                        return Center(
                            child: CircularProgressIndicator(
                              strokeWidth: 1,
                            )
                        );
                        break;
                      case ConnectionState.done:
                        if (snapshot.hasData) {
                          status_quota = snapshot.data.data[0].statusQuota;
                          if(snapshot.data.data.length != 0){
                            initButton = true;
                          }
                          return ListView.builder(
                              itemCount: snapshot.data.data == null ? 0 : snapshot.data.data.length,
                              primary: false,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (BuildContext context, int i){
                                return Container(
                                    margin: EdgeInsets.only(top:15, left: 15, right: 15),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        gradient:LinearGradient(
                                            begin: Alignment.topRight,
                                            end: Alignment.bottomLeft,
                                            colors: [Colors.white, Colors.white]
                                        )
                                    ),
                                  child: Column(
                                    children: <Widget>[
                                      ListTile(
                                        leading: Icon(Icons.access_time),
                                        title: Text("Jam "+snapshot.data.data[i].jadwalStart.toString()+"-"+snapshot.data.data[i].jadwalEnd.toString()),
                                        subtitle: Text("Kouta "+snapshot.data.data[i].quota.toString()),
                                      ),
                                      Visibility(
                                        visible: snapshot.data.data.length != 0,
                                        child: GestureDetector(
                                          onTap: (){
                                            if(status_quota == 0){
                                              HelpersView.toast("Qouta pesanan tidak tersedia", ThemeColors.statePending);
                                            }else{
                                              DataPolySchedule data = DataPolySchedule(
                                                jadwalId: snapshot.data.data[i].jadwalId,
                                                namaDokter: snapshot.data.data[i].namaDokter,
                                                jadwalStart: snapshot.data.data[i].jadwalStart,
                                                jadwalEnd: snapshot.data.data[i].jadwalEnd,
                                                hari: snapshot.data.data[i].hari,
                                                quota: snapshot.data.data[i].quota,
                                                statusQuota: snapshot.data.data[i].statusQuota,
                                              );
                                              goToDetailBookingPoli(data, initData ? datefilter: widget.current_date, dokter_poli, _namaKeluarga, _kodeKeluarga);
                                            }
                                          },
                                          child: Container(
                                            alignment: AlignmentDirectional.center,
                                            margin: EdgeInsets.all(15),
                                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                                            decoration: BoxDecoration(
                                                borderRadius: new BorderRadius.all(Radius.circular(8)),
                                                gradient: ThemeColors.buttonGradient,
                                                boxShadow: [
                                                  BoxShadow(
                                                      offset: Offset(0.0, 3.0),
                                                      color: ThemeColors.buttonShadow,
                                                      blurRadius: 6,
                                                      spreadRadius: 0)
                                                ]
                                            ),
                                            child: Text("Selanjutnya",
                                                style: TextStyle(color: Colors.white, fontSize: 20,
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: 'Rubik',)),
                                          ),
                                        ),
                                      )
                                    ],
                                  )
                                );
                              }
                          );
                        }else if(snapshot.data == null) {
                          return Container(
                            margin: EdgeInsets.all(15),
                            child: Center(
                                child: Text("Jadwal dokter belum tersedia")
                            ),
                          );
                        }else if(snapshot.hasError) {
                          return Center(
                              child: Text("No Internet Connection")
                          );
                        }
                        break;
                    }
                  }
              ),
            ),
          ],
        ),
      ),
    );
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      api_token = sharedPreferences.getString("api_token");
    });
  }

  @override
  void initState() {
    super.initState();
    getCredential();
    _getListFamily();
    dokter_id = widget.dokter_id;
    dokter_name = widget.dokter_name;
    dokter_image = widget.dokter_image;
    dokter_poli = widget.dokter_poli;
    current_date = widget.current_date;
  }
}
