import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/response_search_doctor.dart';

abstract class InterfaceApi {
  //USER
  Future<ResponseLogin> doLogin(LoginRequest data);
  Future<ResponseGetProfile> getProfile(String token);
  Future<ResponseMessage> addUser(CreateUserRequest data);
  Future<ResponseMessage> updateUser(UpdateUserRequest data, String token);
  Future<ResponseMessage> doLogout(String token);
  Future<ResponseDashboard> getDashboard(String token);
  Future<ResponseMessage> getOtp(String telephone);
  Future<ResponseMessage> checkOtp(CheckOtpRequest data);
  Future<ResponseMessage> changePassword(ChangePasswordRequest data, String token);
  Future<ResponseMessage> forgotPassword(String telephone);

  //HISTORY
  Future<ResponseHistory> getHistory(String token);

  //POLYCLINIC
  Future<ResponseListPoliklinik> listPolyClinic(String token);
  Future<ResponseQueue> getQueue(String token);
  Future<ResponsePolySchedule> listPolySchedule(PolyClinicSheduleRequest data, String token);
  Future<ResponsePolySchedule> listDoctorSchedule(DoctorSheduleRequest data, String token);
  Future<ResponseBooking> bookingPoly(BookingRequest data, String token);
  Future<ResponseListConfirm> bookingConfirmation(String token);
  Future<ResponseMessage> bookingConfirmationCome(BookingConfirmationComeRequest data, String token);
  Future<ResponseSearchDoctor> listSearchDoctor(String token, int offset);
  Future<ResponseConfirmQueue> getQueueConfirm(String token, int kodebooking);

  //FAMILY
  Future<ResponseMessage> addFamily(AddFamilyRequest data, String token);
  Future<ResponseFamily> getFamily(String token);
  Future<ResponseDetailFamily> detailFamily(String kode_keluarga, String token);
  Future<ResponseMessage> deleteFamily(String kode_keluarga, String token);
  Future<ResponseMessage> updateFamily(UpdateFamilyRequest data, String token);

  //DOCTOR
  Future<ResponseListDoctor> listDoctor(int offset, String token);
  Future<ResponseListDoctorPoli> listDoctorByPoli(int poliklinik_id, String token);
  Future<ResponseListProfesi> listProfesi(String token);
  Future<ResponseScheduleDoctor> scheduleDoctor(int dokter_id, String token);

}