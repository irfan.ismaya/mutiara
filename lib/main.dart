import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mvvm_mutiara/views/splash_page/splash_page.dart';
import 'package:mvvm_mutiara/views/login_page/login_page.dart';
import 'package:mvvm_mutiara/views/register_page/register_page.dart';
import 'package:mvvm_mutiara/views/forgot_password_page/forgot_password_page.dart';
import 'package:mvvm_mutiara/views/main_menu_page/main_menu_page.dart';
import 'package:mvvm_mutiara/views/choose_poli_page/choose_poli_page.dart';
import 'package:mvvm_mutiara/views/pincode_page/pincode_page.dart';
import 'package:mvvm_mutiara/views/list_family_page/list_family_page.dart';
import 'package:mvvm_mutiara/views/list_doctor_page/list_doctor_page.dart';
import 'package:mvvm_mutiara/views/list_doctorpoli_page/list_doctorpoli_page.dart';
import 'package:mvvm_mutiara/views/change_password/change_password.dart';
import 'package:mvvm_mutiara/views/success_page/success_page.dart';
import 'package:mvvm_mutiara/views/choose_schedule_page/choose_schedule_page.dart';
import 'package:mvvm_mutiara/views/search_doctor_page/search_doctor_page.dart';
import 'package:mvvm_mutiara/views/add_family_page/add_family_page.dart';
import 'package:mvvm_mutiara/views/tutor_page/tutor_page.dart';
import 'package:mvvm_mutiara/views/edit_profile_page/edit_profile_page.dart';
import 'package:mvvm_mutiara/views/schedule_doctor/schedule_doctor_page.dart';
import 'package:mvvm_mutiara/views/detail_booking_page/detail_booking_page.dart';
import 'package:mvvm_mutiara/views/detail_confirm_page/detail_confirm_page.dart';
import 'package:mvvm_mutiara/views/appintro_page/appintro_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(EasyLocalization(child: MyApp()));
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark
    ));

    return EasyLocalizationProvider(
      data: data,
      child: MaterialApp(
        title: 'Flutter Demo',
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          EasylocaLizationDelegate(
              locale: data.locale,
              path: 'langs'),
        ],
        supportedLocales: [Locale('id', 'ID'), Locale('en', 'US')],
        locale: data.savedLocale,
        theme: ThemeData(
            primarySwatch: Colors.blue,
            appBarTheme: AppBarTheme(
                brightness: Brightness.dark
            )
        ),
        home: SplashPage(),
        routes: {
          '/splash': (context) => SplashPage(),
          '/appintro': (context) => AppIntroPage(),
          '/login': (context) => LoginPage(),
          '/forgotpassword': (context) => ForgotPasswordPage(),
          '/register': (context) => RegisterPage(),
          '/mainmenu': (context) => MainMenuPage(),
          '/pincode': (context) => PinCodePage(),
          '/choosepoli': (context) => ChoosePoliPage(),
          '/listdoctor': (context) => ListDocterPage(),
          '/listdoctorpoli': (context) => ListDocterPoliPage(),
          '/listfamily': (context) => ListFamilyPage(),
          '/changepassword': (context) => ChangePasswordPage(),
          '/addfamily': (context) => AddFamilyPage(),
          '/editprofile': (context) => EditProfilePage(),
          '/scheduledoctor': (context) => ScheduleDoctorPage(),
          '/chooseschedule': (context) => ChooseSchedulePage(),
          '/detailbooking': (context) => DetailBookingPage(),
          '/detailconfirm': (context) => DetailConfirmPage(),
          '/searchdoctor': (context) => SearchDoctorPage(),
          '/tutor': (context) => TutorPage(),
          '/success': (context) => SuccessPage()
        },
      ),
    );
  }
}