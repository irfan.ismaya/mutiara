import 'package:mvvm_mutiara/models/api_request.dart';
import 'package:mvvm_mutiara/models/api_response.dart';
import 'package:mvvm_mutiara/models/response_search_doctor.dart';
import 'package:mvvm_mutiara/interfaces/interface_api.dart';
import 'package:mvvm_mutiara/utils/network_util.dart';
import 'package:http/http.dart' show Client;
import 'dart:io';
import 'dart:convert';

class ServiceApi implements InterfaceApi{
  NetworkUtil _netUtil = new NetworkUtil();
  //final String baseUrl = "https://api.morfem.id/booking-online/v1";
  final String baseUrl = "http://172.17.0.125/booking-online/v1";
  Client client = Client();
//=======================================================USER======================================================================//
  Future<ResponseLogin> doLogin(LoginRequest data) {
    return _netUtil.post("$baseUrl/user/login", body: {
      "telephone": data.telephone,
      "password": data.password,
      "versi": data.versi,
      "device_app_type": "1",
      "device_token": data.device_token,
    }).then((dynamic res) {
      if(res["code"] == 200) {
        return ResponseLogin.map(res);
      }else if(res["code"] == 400){
        return ResponseLogin.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseGetProfile> getProfile(String token) {
    return _netUtil.get("$baseUrl/user", headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseGetProfile.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseGetProfile.fromJson(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> addUser(CreateUserRequest data) {
    return _netUtil.post("$baseUrl/user", body: {
      "telephone": data.telephone,
      "name": data.name,
      "alamat": data.alamat,
      "email": data.email,
      "password": data.password,
      "password_confirmation": data.password_confirmation
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.map(res);
      }else if(res["code"] == 400 || res["code"] == 422){
        return ResponseMessage.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> updateUser(UpdateUserRequest data, String token) {
    var body = json.encode({
      "name": data.name,
      "address": data.address,
      "telephone": data.telephone,
      "email": data.email,
      "image": data.image
    });

    return _netUtil.post("$baseUrl/user/update", body: body, headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.map(res);
      }else if(res["code"] == 400 || res["code"] == 422){
        return ResponseMessage.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> doLogout(String token) {
    return _netUtil.post("$baseUrl/user/logout", headers:{
      HttpHeaders.authorizationHeader: "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.map(res);
      }else if(res["code"] == 400){
        return ResponseMessage.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> changePassword(ChangePasswordRequest data, String token) {
    var body = json.encode({
      "old_password": data.old_password,
      "password": data.password,
      "password_confirmation": data.password_confirmation,
    });

    return _netUtil.post("$baseUrl/user/changepassword", body:body, headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.map(res);
      }else if(res["code"] == 400){
        return ResponseMessage.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> forgotPassword(String telephone) {
    return _netUtil.post("$baseUrl/password/forget", body: {
      "telephone": telephone,
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.map(res);
      }else if(res["code"] == 400){
        return ResponseMessage.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> getOtp(String telephone) {
    return _netUtil.post("$baseUrl/user/getotp", body: {
      "telephone": telephone
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.map(res);
      }else if(res["code"] == 400 || res["code"] == 422){
        try{
          return ResponseMessage.map(res);
        }catch(ex){
          throw new Exception(res);
        }
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> checkOtp(CheckOtpRequest data) {
    return _netUtil.post("$baseUrl/user/checkotp", body: {
      "telephone": data.telephone,
      "code_otp": data.code_otp
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.map(res);
      }else if(res["code"] == 400){
        return ResponseMessage.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseDashboard> getDashboard(String token) {
    return _netUtil.get("$baseUrl/user/dashboard", headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseDashboard.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseDashboard.fromJson(res);
      }else{
        throw new Exception(res);
      }
    });
  }

//=======================================================HISTORY===================================================================//
  Future<ResponseHistory> getHistory(String token) {
    return _netUtil.get("$baseUrl/history/booking", headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseHistory.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseHistory.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

//=======================================================FAMILY===================================================================//
  Future<ResponseFamily> getFamily(String token) {
    return _netUtil.get("$baseUrl/keluarga", headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseFamily.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseFamily.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> addFamily(AddFamilyRequest data, String token) {
    final body = jsonEncode({
      "status_dalam_keluarga": data.status_dalam_keluarga,
      "namalengkap": data.namalengkap,
      "tanggal_lahir": data.tanggal_lahir,
      "email": data.email,
      "notelpon": data.notelpon,
      "alamat": data.alamat,
    });

    return _netUtil.post("$baseUrl/keluarga", body: body, headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.map(res);
      }else if(res["code"] == 400 || res["code"] == 422){
        return ResponseMessage.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseDetailFamily> detailFamily(String kode_keluarga, String token) {
    return _netUtil.get("$baseUrl/keluarga/$kode_keluarga", headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseDetailFamily.map(res);
      }else if(res["code"] == 400){
        return ResponseDetailFamily.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> updateFamily(UpdateFamilyRequest data, String token) {
    return _netUtil.post("$baseUrl/keluarga/"+data.kode_keluarga, body: {
      "kode_keluarga": data.kode_keluarga,
      "status_dalam_keluarga": data.status_dalam_keluarga,
      "namalengkap": data.namalengkap,
      "tanggal_lahir": data.tanggal_lahir,
      "email": data.email,
      "notelpon": data.notelpon,
      "alamat": data.alamat
    },headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.map(res);
      }else if(res["code"] == 400){
        return ResponseMessage.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> deleteFamily(String kode_keluarga, String token) {
    return _netUtil.delete("$baseUrl/keluarga/$kode_keluarga", headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.map(res);
      }else if(res["code"] == 400){
        return ResponseMessage.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

//=======================================================DOCTOR===================================================================//
  Future<ResponseListDoctor> listDoctor(int offset, String token) {
    final body = jsonEncode({"offset":offset});
    return _netUtil.post("$baseUrl/dokter", body: body, headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseListDoctor.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseListDoctor.fromJson(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseListDoctorPoli> listDoctorByPoli(int poliklinik_id, String token) {
    return _netUtil.get("$baseUrl/dokter/poliklinik/$poliklinik_id", headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseListDoctorPoli.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseListDoctorPoli.fromJson(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseListProfesi> listProfesi(String token) {
    return _netUtil.get("$baseUrl/dokter",
    headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseListProfesi.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseListProfesi.fromJson(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseScheduleDoctor> scheduleDoctor(int dokter_id, String token) {
    return _netUtil.get("$baseUrl/dokter/jadwal/$dokter_id",
    headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseScheduleDoctor.fromJson(res);
      }else if(res["code"] == 400){
        print(res.toString());
        return ResponseScheduleDoctor.fromJson(res);
      }else{
        throw new Exception(res);
      }
    });
  }

//=======================================================POLYCLINIC===================================================================//
  Future<ResponseListPoliklinik> listPolyClinic(String token) {
    return _netUtil.get("$baseUrl/poliklinik", headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseListPoliklinik.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseListPoliklinik.fromJson(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponsePolySchedule> listPolySchedule(PolyClinicSheduleRequest data, String token) {
    var body = json.encode({
      "poli_id": data.poliId,
      "booking_date" : data.bookingDate
    });

    return _netUtil.post("$baseUrl/poliklinik/jadwal", body: body, headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponsePolySchedule.fromJson(res);
      }else if(res["code"] == 400){
        return ResponsePolySchedule.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponsePolySchedule> listDoctorSchedule(DoctorSheduleRequest data, String token) {
    var body = json.encode({
      "dokter_id": data.dokterId,
      "booking_date" : data.bookingDate
    });
    return _netUtil.post("$baseUrl/dokter/poliklinik/jadwal", body: body, headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponsePolySchedule.fromJson(res);
      }else if(res["code"] == 400){
        return ResponsePolySchedule.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseQueue> getQueue(String token) {
    return _netUtil.post("$baseUrl/poliklinik/antrian", headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseQueue.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseQueue.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseBooking> bookingPoly(BookingRequest data, String token) {
    var body = json.encode({
      "jadwal_id": data.jadwalId,
      "booking_date": data.bookingDate,
      "kode_keluarga": data.kodeKeluarga,
    });

    return _netUtil.post("$baseUrl/poliklinik/booking", body: body, headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseBooking.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseBooking.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseListConfirm> bookingConfirmation(String token) {
    return _netUtil.get("$baseUrl/poliklinik/confirmation", headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseListConfirm.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseListConfirm.fromJson(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseMessage> bookingConfirmationCome(BookingConfirmationComeRequest data, String token) {
    var body = json.encode({
      "booking_id": data.bookingId,
      "booking_status": data.bookingStatus
    });

    return _netUtil.post("$baseUrl/poliklinik/confirmation", body: body, headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseMessage.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseMessage.fromJson(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseSearchDoctor> listSearchDoctor(String token, int offset) {
    var body = json.encode({
      "offset": offset
    });

    return _netUtil.post("$baseUrl/dokter", body: body, headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseSearchDoctor.fromJson(res);
      }else if(res["code"] == 400){
        return ResponseSearchDoctor.fromJson(res);
      }else{
        throw new Exception(res);
      }
    });
  }

  Future<ResponseConfirmQueue> getQueueConfirm(String token, int kodebooking) {
    var body = json.encode({
      "kodebooking": kodebooking
    });

    return _netUtil.post("$baseUrl/poliklinik/antrian", body: body, headers:{
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    }).then((dynamic res) {
      if(res["code"] == 200 || res["code"] == 201) {
        return ResponseConfirmQueue.fromJson(res);
      }else if(res["code"] == 400 || res["code"] == 404){
        return ResponseConfirmQueue.map(res);
      }else{
        throw new Exception(res);
      }
    });
  }
}