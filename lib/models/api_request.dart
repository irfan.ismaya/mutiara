import 'dart:convert';

//REQUEST LOGIN
class LoginRequest{
  final String telephone;
  final String password;
  final String versi;
  final int device_app_type;
  final String device_token;

  LoginRequest({this.telephone, this.password, this.versi, this.device_app_type, this.device_token});

  Map<String, dynamic> toJsonLogin() {
    return {"telephone": telephone,
            "password": password,
            "versi": versi,
            "device_app_type": device_app_type ,
            "device_token": device_token};
  }

  factory LoginRequest.fromJsonLogin(Map<String, dynamic> map) {
    return LoginRequest(
        telephone: map["telephone"],
        password: map["password"],
        versi: map["versi"],
        device_app_type: map["device_app_type"],
        device_token: map["device_token"]
    );
  }
}

String loginRequestToJson(LoginRequest data) {
  final jsonData = data.toJsonLogin();
  return json.encode(jsonData);
}

//REQUEST CHECKOTP
class CheckOtpRequest{
  final String telephone;
  final String code_otp;


  CheckOtpRequest({this.telephone, this.code_otp, });

  Map<String, dynamic> toJsonCheckOtp() {
    return {"telephone": telephone,
            "code_otp": code_otp};
  }

  factory CheckOtpRequest.fromJsonCheckOtp(Map<String, dynamic> map) {
    return CheckOtpRequest(
        telephone: map["telephone"],
        code_otp: map["code_otp"]
    );
  }
}

String checkotpRequestToJson(CheckOtpRequest data) {
  final jsonData = data.toJsonCheckOtp();
  return json.encode(jsonData);
}

//REQUEST CREATE USER
class CreateUserRequest{
  final String telephone;
  final String name;
  final String alamat;
  final String email;
  final String password;
  final String password_confirmation;

  CreateUserRequest({this.telephone, this.name, this.alamat, this.email, this.password, this.password_confirmation});

  Map<String, dynamic> toJsonCreateUser() {
    return {"telephone": telephone,
            "name": name,
            "alamat": alamat,
            "email": email,
            "password": password,
            "password_confirmation": password_confirmation};
  }

  factory CreateUserRequest.fromJsonCreateUser(Map<String, dynamic> map) {
    return CreateUserRequest(
        telephone: map["telephone"],
        name: map["name"],
        alamat: map["alamat"],
        email: map["email"],
        password: map["password"],
        password_confirmation: map["password_confirmation"]
    );
  }
}

String createuserRequestToJson(CreateUserRequest data) {
  final jsonData = data.toJsonCreateUser();
  return json.encode(jsonData);
}

//REQUEST CHANGE PASSWORD
class ChangePasswordRequest{
  final String old_password;
  final String password;
  final String password_confirmation;


  ChangePasswordRequest({this.old_password, this.password, this.password_confirmation});

  Map<String, dynamic> toJsonChangePassword() {
    return {"old_password": old_password,
            "password": password,
            "password_confirmation": password_confirmation};
  }

  factory ChangePasswordRequest.fromJsonChangePassword(Map<String, dynamic> map) {
    return ChangePasswordRequest(
        old_password: map["old_password"],
        password: map["password"],
        password_confirmation: map["password_confirmation"]
    );
  }
}

String changepasswordRequestToJson(ChangePasswordRequest data) {
  final jsonData = data.toJsonChangePassword();
  return json.encode(jsonData);
}

//REQUEST UPDATE USER
class UpdateUserRequest{
  final String name;
  final String address;
  final String telephone;
  final String image;
  final String email;

  UpdateUserRequest({this.telephone, this.name, this.image, this.email, this.address});

  Map<String, dynamic> toJsonUpdateUser() {
    return {"telephone": telephone,
      "name": name,
      "address": address,
      "email": email,
      "telephone": telephone,
      "image": image};
  }

  factory UpdateUserRequest.fromJsonCreateUser(Map<String, dynamic> map) {
    return UpdateUserRequest(
        name: map["name"],
        address: map["address"],
        telephone: map["telephone"],
        email: map["email"],
        image: map["image"]
    );
  }
}

String updateuserRequestToJson(UpdateUserRequest data) {
  final jsonData = data.toJsonUpdateUser();
  return json.encode(jsonData);
}

//REQUEST ADD FAMILY
class AddFamilyRequest{
  final String status_dalam_keluarga;
  final String namalengkap;
  final String tanggal_lahir;
  final String email;
  final String notelpon;
  final String alamat;

  AddFamilyRequest({this.status_dalam_keluarga, this.namalengkap, this.tanggal_lahir, this.email, this.notelpon, this.alamat});

  Map<String, dynamic> toJsonAddFamily() {
    return {"status_dalam_keluarga": status_dalam_keluarga,
      "namalengkap": namalengkap,
      "alamat": alamat,
      "email": email,
      "tanggal_lahir": tanggal_lahir,
      "notelpon": notelpon};
  }

  factory AddFamilyRequest.fromJsonAddFamily(Map<String, dynamic> map) {
    return AddFamilyRequest(
        status_dalam_keluarga: map["status_dalam_keluarga"],
        namalengkap: map["namalengkap"],
        alamat: map["alamat"],
        email: map["email"],
        tanggal_lahir: map["tanggal_lahir"],
        notelpon: map["notelpon"]
    );
  }
}

String addfamilyRequestToJson(AddFamilyRequest data) {
  final jsonData = data.toJsonAddFamily();
  return json.encode(jsonData);
}


//REQUEST UPDATE FAMILY
class UpdateFamilyRequest{
  final String kode_keluarga;
  final String status_dalam_keluarga;
  final String namalengkap;
  final String tanggal_lahir;
  final String email;
  final String notelpon;
  final String alamat;

  UpdateFamilyRequest({this.kode_keluarga, this.status_dalam_keluarga, this.namalengkap, this.tanggal_lahir, this.email, this.notelpon, this.alamat});

  Map<String, dynamic> toJsonUpdateFamily() {
    return {"kode_keluarga": kode_keluarga,
      "status_dalam_keluarga": status_dalam_keluarga,
      "namalengkap": namalengkap,
      "alamat": alamat,
      "email": email,
      "tanggal_lahir": tanggal_lahir,
      "notelpon": notelpon};
  }

  factory UpdateFamilyRequest.fromJsonUpdateFamily(Map<String, dynamic> map) {
    return UpdateFamilyRequest(
        kode_keluarga: map["kode_keluarga"],
        status_dalam_keluarga: map["status_dalam_keluarga"],
        namalengkap: map["namalengkap"],
        alamat: map["alamat"],
        email: map["email"],
        tanggal_lahir: map["tanggal_lahir"],
        notelpon: map["notelpon"]
    );
  }
}

String updatefamilyRequestToJson(UpdateFamilyRequest data) {
  final jsonData = data.toJsonUpdateFamily();
  return json.encode(jsonData);
}

//REQUEST POLY CLINIC SHEDULE
PolyClinicSheduleRequest polyClinicSheduleRequestFromJson(String str) => PolyClinicSheduleRequest.fromJson(json.decode(str));

String polyClinicSheduleRequestToJson(PolyClinicSheduleRequest data) => json.encode(data.toJson());

class PolyClinicSheduleRequest {
  String bookingDate;
  int poliId;

  PolyClinicSheduleRequest({
    this.bookingDate,
    this.poliId,
  });

  factory PolyClinicSheduleRequest.fromJson(Map<String, dynamic> json) => new PolyClinicSheduleRequest(
    bookingDate: json["booking_date"],
    poliId: json["poli_id"],
  );

  Map<String, dynamic> toJson() => {
    "booking_date": bookingDate,
    "poli_id": poliId,
  };
}

//REQUEST BOOKING
BookingRequest bookingRequestFromJson(String str) => BookingRequest.fromJson(json.decode(str));

String bookingRequestToJson(BookingRequest data) => json.encode(data.toJson());

class BookingRequest {
  String bookingDate;
  int kodeKeluarga;
  int jadwalId;

  BookingRequest({
    this.bookingDate,
    this.kodeKeluarga,
    this.jadwalId,
  });

  factory BookingRequest.fromJson(Map<String, dynamic> json) => new BookingRequest(
    bookingDate: json["booking_date"],
    kodeKeluarga: json["kode_keluarga"],
    jadwalId: json["jadwal_id"],
  );

  Map<String, dynamic> toJson() => {
    "booking_date": bookingDate,
    "kode_keluarga": kodeKeluarga,
    "jadwal_id": jadwalId,
  };
}


//REQUEST BOOKING COME
BookingConfirmationComeRequest bookingConfirmationComeRequestFromJson(String str) => BookingConfirmationComeRequest.fromJson(json.decode(str));

String bookingConfirmationComeRequestToJson(BookingConfirmationComeRequest data) => json.encode(data.toJson());

class BookingConfirmationComeRequest {
  int bookingId;
  int bookingStatus;

  BookingConfirmationComeRequest({
    this.bookingId,
    this.bookingStatus,
  });

  factory BookingConfirmationComeRequest.fromJson(Map<String, dynamic> json) => new BookingConfirmationComeRequest(
    bookingId: json["booking_id"],
    bookingStatus: json["booking_status"],
  );

  Map<String, dynamic> toJson() => {
    "booking_id": bookingId,
    "booking_status": bookingStatus,
  };
}

//
DoctorSheduleRequest doctorSheduleRequestFromJson(String str) => DoctorSheduleRequest.fromJson(json.decode(str));

String doctorSheduleRequestToJson(DoctorSheduleRequest data) => json.encode(data.toJson());

class DoctorSheduleRequest {
  int dokterId;
  String bookingDate;

  DoctorSheduleRequest({
    this.dokterId,
    this.bookingDate,
  });

  factory DoctorSheduleRequest.fromJson(Map<String, dynamic> json) => new DoctorSheduleRequest(
    dokterId: json["dokter_id"],
    bookingDate: json["booking_date"],
  );

  Map<String, dynamic> toJson() => {
    "dokter_id": dokterId,
    "booking_date": bookingDate,
  };
}

