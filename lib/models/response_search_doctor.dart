import 'dart:convert';

ResponseSearchDoctor responseSearchDoctorFromJson(String str) => ResponseSearchDoctor.fromJson(json.decode(str));

String responseSearchDoctorToJson(ResponseSearchDoctor data) => json.encode(data.toJson());

class ResponseSearchDoctor {
  String message;
  Data data;
  int code;

  ResponseSearchDoctor({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseSearchDoctor.fromJson(Map<String, dynamic> json) => new ResponseSearchDoctor(
    message: json["message"],
    data: Data.fromJson(json["data"]),
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": data.toJson(),
    "code": code,
  };
}

class Data {
  List<Dokter> dokter;
  int totalPage;

  Data({
    this.dokter,
    this.totalPage,
  });

  factory Data.fromJson(Map<String, dynamic> json) => new Data(
    dokter: new List<Dokter>.from(json["dokter"].map((x) => Dokter.fromJson(x))),
    totalPage: json["total_page"],
  );

  Map<String, dynamic> toJson() => {
    "dokter": new List<dynamic>.from(dokter.map((x) => x.toJson())),
    "total_page": totalPage,
  };
}

class Dokter {
  int dokterId;
  String dokterCode;
  String dokterName;
  String dokterProfession;
  List<DokterJadwal> dokterJadwal;

  Dokter({
    this.dokterId,
    this.dokterCode,
    this.dokterName,
    this.dokterProfession,
    this.dokterJadwal,
  });

  factory Dokter.fromJson(Map<String, dynamic> json) => new Dokter(
    dokterId: json["dokter_id"],
    dokterCode: json["dokter_code"],
    dokterName: json["dokter_name"],
    dokterProfession: json["dokter_profession"],
    dokterJadwal: new List<DokterJadwal>.from(json["dokter_jadwal"].map((x) => DokterJadwal.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "dokter_id": dokterId,
    "dokter_code": dokterCode,
    "dokter_name": dokterName,
    "dokter_profession": dokterProfessionValues.reverse[dokterProfession],
    "dokter_jadwal": new List<dynamic>.from(dokterJadwal.map((x) => x.toJson())),
  };
}

class DokterJadwal {
  int jadwalId;
  String dokterName;
  String poliName;
  String jadwalStart;
  String jadwalEnd;
  String waktupraktek;
  String hari;

  DokterJadwal({
    this.jadwalId,
    this.dokterName,
    this.poliName,
    this.jadwalStart,
    this.jadwalEnd,
    this.waktupraktek,
    this.hari,
  });

  factory DokterJadwal.fromJson(Map<String, dynamic> json) => new DokterJadwal(
    jadwalId: json["jadwal_id"],
    dokterName: json["dokter_name"],
    poliName: json["poli_name"],
    jadwalStart: json["jadwal_start"],
    jadwalEnd: json["jadwal_end"],
    waktupraktek:json["waktupraktek"],
    hari: json["hari"],
  );

  Map<String, dynamic> toJson() => {
    "jadwal_id": jadwalId,
    "dokter_name": dokterName,
    "poli_name": poliNameValues.reverse[poliName],
    "jadwal_start": jadwalStart,
    "jadwal_end": jadwalEnd,
    "waktupraktek": waktupraktekValues.reverse[waktupraktek],
    "hari": hari,
  };
}

enum PoliName { POLI_GIGI, KHITAN, POLI_KANDUNGAN_OBGYN }

final poliNameValues = new EnumValues({
  "KHITAN": PoliName.KHITAN,
  "POLI GIGI": PoliName.POLI_GIGI,
  "POLI KANDUNGAN (OBGYN)": PoliName.POLI_KANDUNGAN_OBGYN
});

enum Waktupraktek { PAGI, MALAM, SIANG }

final waktupraktekValues = new EnumValues({
  "MALAM": Waktupraktek.MALAM,
  "PAGI": Waktupraktek.PAGI,
  "SIANG": Waktupraktek.SIANG
});

enum DokterProfession { DOKTER_GIGI, DOKTER_KHITAN, DOKTER_KANDUNGAN }

final dokterProfessionValues = new EnumValues({
  "Dokter GIGI": DokterProfession.DOKTER_GIGI,
  "Dokter Kandungan": DokterProfession.DOKTER_KANDUNGAN,
  "Dokter Khitan": DokterProfession.DOKTER_KHITAN
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
