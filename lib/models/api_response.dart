import 'dart:convert';

//RESPONSE LOGIN
class ResponseLogin {
  int code;
  String message;
  DataLogin data;

  ResponseLogin({
    this.code,
    this.message,
    this.data,
  });

  factory ResponseLogin.fromJson(Map<String, dynamic> json) => new ResponseLogin(
    code: json["code"],
    message: json["message"],
    data: DataLogin.fromJson(json["data"]),
  );

  ResponseLogin.map(dynamic obj) {
    this.code = obj["code"];
    this.message = obj["message"];
    this.data = obj["data"] == null ? new DataLogin() : DataLogin.fromJson(obj["data"]);
  }

  Map<String, dynamic> toJson() => {
    "code": code,
    "message": message,
    "data": data.toJson(),
  };
}

ResponseLogin loginFromJson(String str) => ResponseLogin.fromJson(json.decode(str));
String loginToJson(ResponseLogin data) => json.encode(data.toJson());


class DataLogin {
  int userId;
  int roleId;
  String name;
  String image;
  String telephone;
  String apiToken;

  DataLogin({
    this.userId,
    this.roleId,
    this.name,
    this.image,
    this.telephone,
    this.apiToken,
  });

  factory DataLogin.fromJson(Map<String, dynamic> json) => DataLogin(
    userId: json["user_id"],
    roleId: json["role_id"],
    name: json["name"],
    image: json["image"],
    telephone: json["telephone"],
    apiToken: json["api_token"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "role_id": roleId,
    "name": name,
    "image": image,
    "telephone": telephone,
    "api_token": apiToken,
  };
}

//RESPONSE MESSAGE
class ResponseMessage{
  int code;
  String message;

  ResponseMessage({
    this.code,
    this.message,
  });

  factory ResponseMessage.fromJson(Map<String, dynamic> json) => new ResponseMessage(
    code: json["code"],
    message: json["message"]
  );

  ResponseMessage.map(dynamic obj) {
    this.code = obj["code"];
    this.message = obj["message"];
  }

  Map<String, dynamic> toJson() => {
    "code": code,
    "message": message,
  };
}

//RESPONSE HISTORY
ResponseHistory responseHistoryFromJson(String str) => ResponseHistory.fromJson(json.decode(str));

String responseHistoryToJson(ResponseHistory data) => json.encode(data.toJson());

class ResponseHistory {
  String message;
  int code;
  List<DataHistory> data;

  ResponseHistory({
    this.message,
    this.code,
    this.data,
  });

  factory ResponseHistory.fromJson(Map<String, dynamic> json) => new ResponseHistory(
    message: json["message"],
    code: json["code"],
    data: new List<DataHistory>.from(json["data"].map((x) => DataHistory.fromJson(x))),
  );

  ResponseHistory.map(dynamic obj) {
    this.code = obj["code"];
    this.message = obj["message"];
    this.data = obj["data"] == null ? new DataHistory() : ResponseHistory.fromJson(obj["data"]);
  }

  Map<String, dynamic> toJson() => {
    "message": message,
    "code": code,
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class DataHistory {
  String poliName;
  String dokterName;
  String bookingDate;
  int bookingCode;
  int bookingStatus;
  String bookingStatusName;
  String note;

  DataHistory({
    this.poliName,
    this.dokterName,
    this.bookingDate,
    this.bookingCode,
    this.bookingStatus,
    this.bookingStatusName,
    this.note,
  });

  factory DataHistory.fromJson(Map<String, dynamic> json) => new DataHistory(
    poliName: json["poli_name"],
    dokterName: json["dokter_name"],
    bookingDate: json["booking_date"],
    bookingCode: json["booking_code"],
    bookingStatus: json["booking_status"],
    bookingStatusName: json["booking_status_name"],
    note: json["note"],
  );

  Map<String, dynamic> toJson() => {
    "poli_name": poliName,
    "dokter_name": dokterName,
    "booking_date": bookingDate,
    "booking_code": bookingCode,
    "booking_status": bookingStatus,
    "booking_status_name": bookingStatusName,
    "note": note,
  };
}

//RESPONSE FAMILY
ResponseFamily responseFamilyFromJson(String str) => ResponseFamily.fromJson(json.decode(str));

String responseFamilyToJson(ResponseFamily data) => json.encode(data.toJson());

class ResponseFamily {
  String message;
  int code;
  List<DataFamily> data;

  ResponseFamily({
    this.message,
    this.code,
    this.data,
  });

  factory ResponseFamily.fromJson(Map<String, dynamic> json) => new ResponseFamily(
    message: json["message"],
    code: json["code"],
    data: new List<DataFamily>.from(json["data"].map((x) => DataFamily.fromJson(x))),
  );

  ResponseFamily.map(dynamic obj) {
    this.code = obj["code"];
    this.message = obj["message"];
    this.data = obj["data"] == null ? new DataFamily() : ResponseFamily.fromJson(obj["data"]);
  }

  Map<String, dynamic> toJson() => {
    "message": message,
    "code": code,
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class DataFamily {
  String kodeKeluarga;
  String namalengkap;
  String statusDalamKeluarga;
  String notelpon;

  DataFamily({
    this.kodeKeluarga,
    this.namalengkap,
    this.statusDalamKeluarga,
    this.notelpon,
  });

  factory DataFamily.fromJson(Map<String, dynamic> json) => new DataFamily(
    kodeKeluarga: json["kode_keluarga"],
    namalengkap: json["namalengkap"],
    statusDalamKeluarga: json["status_dalam_keluarga"],
    notelpon: json["notelpon"],
  );

  Map<String, dynamic> toJson() => {
    "kode_keluarga": kodeKeluarga,
    "namalengkap": namalengkap,
    "status_dalam_keluarga": statusDalamKeluarga,
    "notelpon": notelpon,
  };
}

//RESPONSE DETAIL FAMILY
class ResponseDetailFamily {
  int code;
  String message;
  DetailFamily data;

  ResponseDetailFamily({
    this.code,
    this.message,
    this.data,
  });

  factory ResponseDetailFamily.fromJson(Map<String, dynamic> json) => new ResponseDetailFamily(
    code: json["code"],
    message: json["message"],
    data: DetailFamily.fromJson(json["data"]),
  );

  ResponseDetailFamily.map(dynamic obj) {
    this.code = obj["code"];
    this.message = obj["message"];
    this.data = obj["data"] == null ? new DetailFamily() : DetailFamily.fromJson(obj["data"]);
  }

  Map<String, dynamic> toJson() => {
    "code": code,
    "message": message,
    "data": data.toJson(),
  };
}

ResponseDetailFamily detailfamilyFromJson(String str) => ResponseDetailFamily.fromJson(json.decode(str));
String detailfamilyToJson(ResponseDetailFamily data) => json.encode(data.toJson());


class DetailFamily {
  int userId;
  int roleId;
  String name;
  String image;
  String telephone;
  String apiToken;

  DetailFamily({
    this.userId,
    this.roleId,
    this.name,
    this.image,
    this.telephone,
    this.apiToken,
  });

  factory DetailFamily.fromJson(Map<String, dynamic> json) => DetailFamily(
    userId: json["user_id"],
    roleId: json["role_id"],
    name: json["name"],
    image: json["image"],
    telephone: json["telephone"],
    apiToken: json["api_token"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "role_id": roleId,
    "name": name,
    "image": image,
    "telephone": telephone,
    "api_token": apiToken,
  };
}

//RESPONSE LIST DOCTOR
ResponseListDoctor responseListDoctorFromJson(String str) => ResponseListDoctor.fromJson(json.decode(str));

String responseListDoctorToJson(ResponseListDoctor data) => json.encode(data.toJson());

class ResponseListDoctor {
  String message;
  DataDoctor data;
  int code;

  ResponseListDoctor({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseListDoctor.fromJson(Map<String, dynamic> json) => new ResponseListDoctor(
    message: json["message"],
    data: DataDoctor.fromJson(json["data"]),
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": data.toJson(),
    "code": code,
  };
}

class DataDoctor {
  List<Dokter> dokter;
  int totalPage;

  DataDoctor({
    this.dokter,
    this.totalPage,
  });

  factory DataDoctor.fromJson(Map<String, dynamic> json) => new DataDoctor(
    dokter: new List<Dokter>.from(json["dokter"].map((x) => Dokter.fromJson(x))),
    totalPage: json["total_page"],
  );

  Map<String, dynamic> toJson() => {
    "dokter": new List<dynamic>.from(dokter.map((x) => x.toJson())),
    "total_page": totalPage,
  };
}

class Dokter {
  int dokterId;
  String dokterCode;
  String dokterName;
  String dokterProfession;
  List<DoctorSchedule> dokterJadwal;

  Dokter({
    this.dokterId,
    this.dokterCode,
    this.dokterName,
    this.dokterProfession,
    this.dokterJadwal,
  });

  factory Dokter.fromJson(Map<String, dynamic> json) => new Dokter(
    dokterId: json["dokter_id"],
    dokterCode: json["dokter_code"],
    dokterName: json["dokter_name"],
    dokterProfession: json["dokter_profession"],
    dokterJadwal: new List<DoctorSchedule>.from(json["dokter_jadwal"].map((x) => DoctorSchedule.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "dokter_id": dokterId,
    "dokter_code": dokterCode,
    "dokter_name": dokterName,
    "dokter_profession": dokterProfession,
    "dokter_jadwal": new List<dynamic>.from(dokterJadwal.map((x) => x.toJson())),
  };
}

class DoctorSchedule {
  int jadwalId;
  String dokterName;
  String poliName;
  String jadwalStart;
  String jadwalEnd;
  String waktupraktek;
  String hari;

  DoctorSchedule({
    this.jadwalId,
    this.dokterName,
    this.poliName,
    this.jadwalStart,
    this.jadwalEnd,
    this.waktupraktek,
    this.hari,
  });

  factory DoctorSchedule.fromJson(Map<String, dynamic> json) => new DoctorSchedule(
    jadwalId: json["jadwal_id"],
    dokterName: json["dokter_name"],
    poliName: json["poli_name"],
    jadwalStart: json["jadwal_start"],
    jadwalEnd: json["jadwal_end"],
    waktupraktek: json["waktupraktek"],
    hari: json["hari"],
  );

  Map<String, dynamic> toJson() => {
    "jadwal_id": jadwalId,
    "dokter_name": dokterName,
    "poli_name": poliName,
    "jadwal_start": jadwalStart,
    "jadwal_end": jadwalEnd,
    "waktupraktek": waktupraktek,
    "hari": hari,
  };
}

//RESPONSE LIST PROFESI
ResponseListProfesi responseListProfesiFromJson(String str) => ResponseListProfesi.fromJson(json.decode(str));

String responseListProfesiToJson(ResponseListProfesi data) => json.encode(data.toJson());

class ResponseListProfesi {
  String message;
  List<DataProfesi> data;
  int code;

  ResponseListProfesi({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseListProfesi.fromJson(Map<String, dynamic> json) => new ResponseListProfesi(
    message: json["message"],
    data: new List<DataProfesi>.from(json["data"].map((x) => DataProfesi.fromJson(x))),
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
    "code": code,
  };
}

class DataProfesi {
  int profesiId;
  String profesiName;
  dynamic profesiIcon;
  dynamic profesiDesc;

  DataProfesi({
    this.profesiId,
    this.profesiName,
    this.profesiIcon,
    this.profesiDesc,
  });

  factory DataProfesi.fromJson(Map<String, dynamic> json) => new DataProfesi(
    profesiId: json["profesi_id"],
    profesiName: json["profesi_name"],
    profesiIcon: json["profesi_icon"],
    profesiDesc: json["profesi_desc"],
  );

  Map<String, dynamic> toJson() => {
    "profesi_id": profesiId,
    "profesi_name": profesiName,
    "profesi_icon": profesiIcon,
    "profesi_desc": profesiDesc,
  };
}

//RESPONSE SCHEDULE DOCTOR
ResponseScheduleDoctor responseScheduleDoctorFromJson(String str) => ResponseScheduleDoctor.fromJson(json.decode(str));

String responseScheduleDoctorToJson(ResponseScheduleDoctor data) => json.encode(data.toJson());

class ResponseScheduleDoctor {
  String message;
  List<DataSchedule> data;
  int code;

  ResponseScheduleDoctor({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseScheduleDoctor.fromJson(Map<String, dynamic> json) => new ResponseScheduleDoctor(
    message: json["message"],
    data: new List<DataSchedule>.from(json["data"].map((x) => DataSchedule.fromJson(x))),
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
    "code": code,
  };
}

class DataSchedule {
  int jadwalId;
  String dokterName;
  String poliName;
  String jadwalStart;
  String jadwalEnd;
  String waktupraktek;
  String hari;

  DataSchedule({
    this.jadwalId,
    this.dokterName,
    this.poliName,
    this.jadwalStart,
    this.jadwalEnd,
    this.waktupraktek,
    this.hari,
  });

  factory DataSchedule.fromJson(Map<String, dynamic> json) => new DataSchedule(
    jadwalId: json["jadwal_id"],
    dokterName: json["dokter_name"],
    poliName: json["poli_name"],
    jadwalStart: json["jadwal_start"],
    jadwalEnd: json["jadwal_end"],
    waktupraktek: json["waktupraktek"],
    hari: json["hari"],
  );

  Map<String, dynamic> toJson() => {
    "jadwal_id": jadwalId,
    "dokter_name": dokterName,
    "poli_name": poliName,
    "jadwal_start": jadwalStart,
    "jadwal_end": jadwalEnd,
    "waktupraktek": waktupraktek,
    "hari": hari,
  };
}

//RESPONSE POLIKLINIK
ResponseListPoliklinik responseListPoliklinikFromJson(String str) => ResponseListPoliklinik.fromJson(json.decode(str));

String responseListPoliklinikToJson(ResponseListPoliklinik data) => json.encode(data.toJson());

class ResponseListPoliklinik {
  String message;
  List<DataPoliklinik> data;
  int code;

  ResponseListPoliklinik({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseListPoliklinik.fromJson(Map<String, dynamic> json) => new ResponseListPoliklinik(
    message: json["message"],
    data: new List<DataPoliklinik>.from(json["data"].map((x) => DataPoliklinik.fromJson(x))),
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
    "code": code,
  };
}

class DataPoliklinik {
  int poliId;
  String poliCode;
  String poliIcon;
  String poliName;
  dynamic poliDescription;

  DataPoliklinik({
    this.poliId,
    this.poliCode,
    this.poliIcon,
    this.poliName,
    this.poliDescription,
  });

  factory DataPoliklinik.fromJson(Map<String, dynamic> json) => new DataPoliklinik(
    poliId: json["poli_id"],
    poliCode: json["poli_code"],
    poliIcon: json["poli_icon"],
    poliName: json["poli_name"],
    poliDescription: json["poli_description"],
  );

  Map<String, dynamic> toJson() => {
    "poli_id": poliId,
    "poli_code": poliCode,
    "poli_icon": poliIcon,
    "poli_name": poliName,
    "poli_description": poliDescription,
  };
}

//RESPONSE POLY SCHEDULE
ResponsePolySchedule responsePolyScheduleFromJson(String str) => ResponsePolySchedule.fromJson(json.decode(str));

String responsePolyScheduleToJson(ResponsePolySchedule data) => json.encode(data.toJson());

class ResponsePolySchedule {
  String message;
  List<DataPolySchedule> data;
  int code;

  ResponsePolySchedule({
    this.message,
    this.data,
    this.code,
  });

  factory ResponsePolySchedule.fromJson(Map<String, dynamic> json) => new ResponsePolySchedule(
    message: json["message"],
    data: new List<DataPolySchedule>.from(json["data"].map((x) => DataPolySchedule.fromJson(x))),
    code: json["code"],
  );

  ResponsePolySchedule.map(dynamic obj) {
    this.code = obj["code"];
    this.message = obj["message"];
    this.data = obj["data"] == null ? new DataPolySchedule() : ResponsePolySchedule.fromJson(obj["data"]);
  }

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
    "code": code,
  };
}

class DataPolySchedule {
  int jadwalId;
  String namaDokter;
  String jadwalStart;
  String jadwalEnd;
  String waktupraktek;
  String hari;
  String quota;
  int statusQuota;

  DataPolySchedule({
    this.jadwalId,
    this.namaDokter,
    this.jadwalStart,
    this.jadwalEnd,
    this.waktupraktek,
    this.hari,
    this.quota,
    this.statusQuota,
  });

  factory DataPolySchedule.fromJson(Map<String, dynamic> json) => new DataPolySchedule(
    jadwalId: json["jadwal_id"],
    namaDokter: json["nama_dokter"],
    jadwalStart: json["jadwal_start"],
    jadwalEnd: json["jadwal_end"],
    waktupraktek: json["waktupraktek"],
    hari: json["hari"],
    quota: json["quota"],
    statusQuota: json["status_quota"],
  );

  Map<String, dynamic> toJson() => {
    "jadwal_id": jadwalId,
    "nama_dokter": namaDokter,
    "jadwal_start": jadwalStart,
    "jadwal_end": jadwalEnd,
    "waktupraktek": waktupraktek,
    "hari": hari,
    "quota": quota,
    "status_quota": statusQuota,
  };
}

//RESPONSE BOOKING
ResponseBooking responseBookingFromJson(String str) => ResponseBooking.fromJson(json.decode(str));

String responseBookingToJson(ResponseBooking data) => json.encode(data.toJson());

class ResponseBooking {
  String message;
  int code;
  Data data;

  ResponseBooking({
    this.message,
    this.code,
    this.data,
  });

  factory ResponseBooking.fromJson(Map<String, dynamic> json) => new ResponseBooking(
    message: json["message"],
    code: json["code"],
    data: Data.fromJson(json["data"]),
  );

  ResponseBooking.map(dynamic obj) {
    this.code = obj["code"];
    this.message = obj["message"];
    this.data = obj["data"] == null ? new Data() : Data.fromJson(obj["data"]);
  }

  Map<String, dynamic> toJson() => {
    "message": message,
    "code": code,
    "data": data.toJson(),
  };
}

class Data {
  int kodebooking;
  int idpoliklinik;
  DateTime tanggalbooking;
  int kodeKeluarga;
  String jampraktekAwal;
  String jampraktekAkhir;
  int iddokter;
  int createdBy;

  Data({
    this.kodebooking,
    this.idpoliklinik,
    this.tanggalbooking,
    this.kodeKeluarga,
    this.jampraktekAwal,
    this.jampraktekAkhir,
    this.iddokter,
    this.createdBy,
  });

  factory Data.fromJson(Map<String, dynamic> json) => new Data(
    kodebooking: json["kodebooking"],
    idpoliklinik: json["idpoliklinik"],
    tanggalbooking: DateTime.parse(json["tanggalbooking"]),
    kodeKeluarga: json["kode_keluarga"],
    jampraktekAwal: json["jampraktek_awal"],
    jampraktekAkhir: json["jampraktek_akhir"],
    iddokter: json["iddokter"],
    createdBy: json["created_by"],
  );

  Map<String, dynamic> toJson() => {
    "kodebooking": kodebooking,
    "idpoliklinik": idpoliklinik,
    "tanggalbooking": "${tanggalbooking.year.toString().padLeft(4, '0')}-${tanggalbooking.month.toString().padLeft(2, '0')}-${tanggalbooking.day.toString().padLeft(2, '0')}",
    "kode_keluarga": kodeKeluarga,
    "jampraktek_awal": jampraktekAwal,
    "jampraktek_akhir": jampraktekAkhir,
    "iddokter": iddokter,
    "created_by": createdBy,
  };
}

//RESPONSE BOOKING CONFIRMATION
ResponseBookingConfirmation responseBookingConfirmationFromJson(String str) => ResponseBookingConfirmation.fromJson(json.decode(str));

String responseBookingConfirmationToJson(ResponseBookingConfirmation data) => json.encode(data.toJson());

class ResponseBookingConfirmation {
  String message;
  int code;
  DataBookingConfirmation data;

  ResponseBookingConfirmation({
    this.message,
    this.code,
    this.data,
  });

  factory ResponseBookingConfirmation.fromJson(Map<String, dynamic> json) => new ResponseBookingConfirmation(
    message: json["message"],
    code: json["code"],
    data: DataBookingConfirmation.fromJson(json["data"]),
  );

  ResponseBookingConfirmation.map(dynamic obj) {
    this.code = obj["code"];
    this.message = obj["message"];
    this.data = obj["data"] == null ? new DataBookingConfirmation() : ResponseBookingConfirmation.fromJson(obj["data"]);
  }

  Map<String, dynamic> toJson() => {
    "message": message,
    "code": code,
    "data": data.toJson(),
  };
}

class DataBookingConfirmation {
  int bookingId;
  int kodebooking;
  String bookingDate;
  String poliklinikName;
  String dokterName;
  String jampraktekAwal;
  String jampraktekAkhir;
  String hari;

  DataBookingConfirmation({
    this.bookingId,
    this.kodebooking,
    this.bookingDate,
    this.poliklinikName,
    this.dokterName,
    this.jampraktekAwal,
    this.jampraktekAkhir,
    this.hari,
  });

  factory DataBookingConfirmation.fromJson(Map<String, dynamic> json) => new DataBookingConfirmation(
    bookingId: json["booking_id"],
    kodebooking: json["kodebooking"],
    bookingDate: json["booking_date"],
    poliklinikName: json["poliklinik_name"],
    dokterName: json["dokter_name"],
    jampraktekAwal: json["jampraktek_awal"],
    jampraktekAkhir: json["jampraktek_akhir"],
    hari: json["hari"],
  );

  Map<String, dynamic> toJson() => {
    "booking_id": bookingId,
    "kodebooking": kodebooking,
    "booking_date": bookingDate,
    "poliklinik_name": poliklinikName,
    "dokter_name": dokterName,
    "jampraktek_awal": jampraktekAwal,
    "jampraktek_akhir": jampraktekAkhir,
    "hari": hari,
  };
}

//RESPONSE DASHBOARD
ResponseDashboard responseDashboardFromJson(String str) => ResponseDashboard.fromJson(json.decode(str));

String responseDashboardToJson(ResponseDashboard data) => json.encode(data.toJson());

class ResponseDashboard {
  String message;
  DataDashboard data;
  int code;

  ResponseDashboard({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseDashboard.fromJson(Map<String, dynamic> json) => new ResponseDashboard(
    message: json["message"],
    data: DataDashboard.fromJson(json["data"]),
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": data.toJson(),
    "code": code,
  };
}

class DataDashboard {
  List<Berita> berita;
  List<Booking> booking;
  String urgentnumber;
  List<Howtopicture> howtopicture;

  DataDashboard({
    this.berita,
    this.booking,
    this.urgentnumber,
    this.howtopicture,
  });

  factory DataDashboard.fromJson(Map<String, dynamic> json) => new DataDashboard(
    berita: new List<Berita>.from(json["berita"].map((x) => Berita.fromJson(x))),
    booking: new List<Booking>.from(json["booking"].map((x) => Booking.fromJson(x))),
    urgentnumber: json["urgentnumber"],
    howtopicture: new List<Howtopicture>.from(json["howtopicture"].map((x) => Howtopicture.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "berita": new List<dynamic>.from(berita.map((x) => x.toJson())),
    "booking": new List<dynamic>.from(booking.map((x) => x.toJson())),
    "urgentnumber": urgentnumber,
    "howtopicture": new List<dynamic>.from(howtopicture.map((x) => x.toJson())),
  };
}

class Berita {
  String judul;
  String isi;
  String gambar;
  String url;

  Berita({
    this.judul,
    this.isi,
    this.gambar,
    this.url,
  });

  factory Berita.fromJson(Map<String, dynamic> json) => new Berita(
    judul: json["judul"],
    isi: json["isi"],
    gambar: json["gambar"],
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "judul": judul,
    "isi": isi,
    "gambar": gambar,
    "url": url,
  };
}

class Booking {
  int kodebooking;
  String poliklinikName;
  String bookingDate;
  String jampraktekAwal;
  String jampraktekAkhir;
  String hari;

  Booking({
    this.kodebooking,
    this.poliklinikName,
    this.bookingDate,
    this.jampraktekAwal,
    this.jampraktekAkhir,
    this.hari,
  });

  factory Booking.fromJson(Map<String, dynamic> json) => new Booking(
    kodebooking: json["kodebooking"],
    poliklinikName: json["poliklinik_name"],
    bookingDate: json["booking_date"],
    jampraktekAwal: json["jampraktek_awal"],
    jampraktekAkhir: json["jampraktek_akhir"],
    hari: json["hari"],
  );

  Map<String, dynamic> toJson() => {
    "kodebooking": kodebooking,
    "poliklinik_name": poliklinikName,
    "booking_date": bookingDate,
    "jampraktek_awal": jampraktekAwal,
    "jampraktek_akhir": jampraktekAkhir,
    "hari": hari,
  };
}

class Howtopicture {
  int id;
  String icon;
  String gambar;

  Howtopicture({
    this.id,
    this.icon,
    this.gambar,
  });

  factory Howtopicture.fromJson(Map<String, dynamic> json) => new Howtopicture(
    id: json["id"],
    icon: json["icon"],
    gambar: json["gambar"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "icon": icon,
    "gambar": gambar,
  };
}

//
ResponseGetProfile responseGetProfileFromJson(String str) => ResponseGetProfile.fromJson(json.decode(str));

String responseGetProfileToJson(ResponseGetProfile data) => json.encode(data.toJson());

class ResponseGetProfile {
  String message;
  DataProfile data;
  int code;

  ResponseGetProfile({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseGetProfile.fromJson(Map<String, dynamic> json) => new ResponseGetProfile(
    message: json["message"],
    data: DataProfile.fromJson(json["data"]),
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": data.toJson(),
    "code": code,
  };
}

class DataProfile {
  int userId;
  String name;
  String address;
  String image;
  String imageFullpath;
  String telephone;
  String email;
  int roleId;
  String deviceToken;
  String roleName;

  DataProfile({
    this.userId,
    this.name,
    this.address,
    this.image,
    this.imageFullpath,
    this.telephone,
    this.email,
    this.roleId,
    this.deviceToken,
    this.roleName,
  });

  factory DataProfile.fromJson(Map<String, dynamic> json) => new DataProfile(
    userId: json["user_id"],
    name: json["name"],
    address: json["address"],
    image: json["image"],
    imageFullpath: json["image_fullpath"],
    telephone: json["telephone"],
    email: json["email"],
    roleId: json["role_id"],
    deviceToken: json["device_token"],
    roleName: json["role_name"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "name": name,
    "address": address,
    "image": image,
    "image_fullpath": imageFullpath,
    "telephone": telephone,
    "email": email,
    "role_id": roleId,
    "device_token": deviceToken,
    "role_name": roleName,
  };
}

ResponseQueue responseQueueFromJson(String str) => ResponseQueue.fromJson(json.decode(str));

String responseQueueToJson(ResponseQueue data) => json.encode(data.toJson());

class ResponseQueue {
  String message;
  DataQueue data;
  int code;

  ResponseQueue({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseQueue.fromJson(Map<String, dynamic> json) => new ResponseQueue(
    message: json["message"],
    data: DataQueue.fromJson(json["data"]),
    code: json["code"],
  );

  ResponseQueue.map(dynamic obj) {
    this.code = obj["code"];
    this.message = obj["message"];
    this.data = obj["data"] == null ? new DataQueue() : ResponseQueue.fromJson(obj["data"]);
  }

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": data.toJson(),
    "code": code,
  };
}

class DataQueue {
  String noAntrian;
  String jamKedatangan;

  DataQueue({
    this.noAntrian,
    this.jamKedatangan,
  });

  factory DataQueue.fromJson(Map<String, dynamic> json) => new DataQueue(
    noAntrian: json["no_antrian"],
    jamKedatangan: json["jam_kedatangan"],
  );

  Map<String, dynamic> toJson() => {
    "no_antrian": noAntrian,
    "jam_kedatangan": jamKedatangan,
  };
}

//
ResponseListConfirm responseListConfirmFromJson(String str) => ResponseListConfirm.fromJson(json.decode(str));

String responseListConfirmToJson(ResponseListConfirm data) => json.encode(data.toJson());

class ResponseListConfirm {
  String message;
  List<DataConfirm> data;
  int code;

  ResponseListConfirm({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseListConfirm.fromJson(Map<String, dynamic> json) => new ResponseListConfirm(
    message: json["message"],
    data: new List<DataConfirm>.from(json["data"].map((x) => DataConfirm.fromJson(x))),
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
    "code": code,
  };
}

class DataConfirm {
  int bookingId;
  int kodebooking;
  String bookingDate;
  String poliklinikName;
  String dokterName;
  String jampraktekAwal;
  String jampraktekAkhir;
  String hari;

  DataConfirm({
    this.bookingId,
    this.kodebooking,
    this.bookingDate,
    this.poliklinikName,
    this.dokterName,
    this.jampraktekAwal,
    this.jampraktekAkhir,
    this.hari,
  });

  factory DataConfirm.fromJson(Map<String, dynamic> json) => new DataConfirm(
    bookingId: json["booking_id"],
    kodebooking: json["kodebooking"],
    bookingDate: json["booking_date"],
    poliklinikName: json["poliklinik_name"],
    dokterName: json["dokter_name"],
    jampraktekAwal: json["jampraktek_awal"],
    jampraktekAkhir: json["jampraktek_akhir"],
    hari: json["hari"],
  );

  Map<String, dynamic> toJson() => {
    "booking_id": bookingId,
    "kodebooking": kodebooking,
    "booking_date": bookingDate,
    "poliklinik_name": poliklinikName,
    "dokter_name": dokterName,
    "jampraktek_awal": jampraktekAwal,
    "jampraktek_akhir": jampraktekAkhir,
    "hari": hari,
  };
}

ResponseConfirmQueue responseConfirmQueueFromJson(String str) => ResponseConfirmQueue.fromJson(json.decode(str));

String responseConfirmQueueToJson(ResponseConfirmQueue data) => json.encode(data.toJson());

class ResponseConfirmQueue {
  String message;
  DataQueueConfirm data;
  int code;

  ResponseConfirmQueue({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseConfirmQueue.fromJson(Map<String, dynamic> json) => new ResponseConfirmQueue(
    message: json["message"],
    data: DataQueueConfirm.fromJson(json["data"]),
    code: json["code"],
  );

  ResponseConfirmQueue.map(dynamic obj) {
    this.code = obj["code"];
    this.message = obj["message"];
    this.data = obj["data"] == null ? new DataQueueConfirm() : DataQueueConfirm.fromJson(obj["data"]);
  }

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": data.toJson(),
    "code": code,
  };
}

class DataQueueConfirm {
  int noAntrian;
  String jamKedatangan;

  DataQueueConfirm({
    this.noAntrian,
    this.jamKedatangan,
  });

  factory DataQueueConfirm.fromJson(Map<String, dynamic> json) => new DataQueueConfirm(
    noAntrian: json["no_antrian"],
    jamKedatangan: json["jam_kedatangan"],
  );

  Map<String, dynamic> toJson() => {
    "no_antrian": noAntrian,
    "jam_kedatangan": jamKedatangan,
  };
}

//New List Doctor by Poli
// To parse this JSON data, do
//
//     final responseListDoctorPoli = responseListDoctorPoliFromJson(jsonString);

ResponseListDoctorPoli responseListDoctorPoliFromJson(String str) => ResponseListDoctorPoli.fromJson(json.decode(str));
String responseListDoctorPoliToJson(ResponseListDoctorPoli data) => json.encode(data.toJson());

class ResponseListDoctorPoli {
  String message;
  List<DataDokter> data;
  int code;

  ResponseListDoctorPoli({
    this.message,
    this.data,
    this.code,
  });

  factory ResponseListDoctorPoli.fromJson(Map<String, dynamic> json) => new ResponseListDoctorPoli(
    message: json["message"],
    data: new List<DataDokter>.from(json["data"].map((x) => DataDokter.fromJson(x))),
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
    "code": code,
  };
}

class DataDokter {
  int dokterId;
  String dokterImage;
  String dokterName;
  String dokterPoli;

  DataDokter({
    this.dokterId,
    this.dokterImage,
    this.dokterName,
    this.dokterPoli,
  });

  factory DataDokter.fromJson(Map<String, dynamic> json) => new DataDokter(
    dokterId: json["dokter_id"],
    dokterImage: json["dokter_image"],
    dokterName: json["dokter_name"],
    dokterPoli: json["dokter_poli"],
  );

  Map<String, dynamic> toJson() => {
    "dokter_id": dokterId,
    "dokter_image": dokterImage,
    "dokter_name": dokterName,
    "dokter_poli": dokterPoli,
  };
}



